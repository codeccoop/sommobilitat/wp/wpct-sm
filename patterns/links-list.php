<?php
/**
 * Title: Llista d'enllaços
 * Slug: sm-pattern/links-list
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"categories":["sm-pattern"],"patternName":"sm-pattern/links-list","name":"Llista d'enllaços"},"align":"full","style":{"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
<section
  class="wp-block-group alignfull"
  style="
    padding-top: var(--wp--preset--spacing--40);
    padding-bottom: var(--wp--preset--spacing--40);
  "
>
  <!-- wp:group {"lock":{"move":false,"remove":false},"align":"wide","className":"sm-banner","style":{"spacing":{"padding":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50","left":"var:preset|spacing|60","right":"var:preset|spacing|50"}},"border":{"radius":"12px"}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
  <div
    class="wp-block-group alignwide sm-banner has-base-background-color has-background"
    style="
      border-radius: 12px;
      padding-top: var(--wp--preset--spacing--50);
      padding-right: var(--wp--preset--spacing--50);
      padding-bottom: var(--wp--preset--spacing--50);
      padding-left: var(--wp--preset--spacing--60);
    "
  >
    <!-- wp:heading {"level":3} -->
    <h3 class="wp-block-heading">Links List</h3>
    <!-- /wp:heading -->

    <!-- wp:columns -->
    <div class="wp-block-columns">
      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|30","padding":{"bottom":"var:preset|spacing|30"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
        <div
          class="wp-block-group"
          style="padding-bottom: var(--wp--preset--spacing--30)"
        >
          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|30","padding":{"bottom":"var:preset|spacing|30"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
        <div
          class="wp-block-group"
          style="padding-bottom: var(--wp--preset--spacing--30)"
        >
          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|30","padding":{"bottom":"var:preset|spacing|30"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
        <div
          class="wp-block-group"
          style="padding-bottom: var(--wp--preset--spacing--30)"
        >
          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|30","padding":{"bottom":"var:preset|spacing|30"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
        <div
          class="wp-block-group"
          style="padding-bottom: var(--wp--preset--spacing--30)"
        >
          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->

          <!-- wp:wpct-block/external-link {"arrow":"internal"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
  </div>
  <!-- /wp:group -->
</section>
<!-- /wp:group -->
