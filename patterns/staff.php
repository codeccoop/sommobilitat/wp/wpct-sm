<?php
/**
 * Title: Consell Rector
 * Slug: sm-pattern/staff
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Consell Rector","categories":["sm-pattern"],"patternName":"sm-pattern/staff"},"className":"sm-staff","style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60"}}},"layout":{"type":"constrained"}} -->
<section class="wp-block-group sm-staff" style="padding-top:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60)">
    <!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
    <h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--10)">Consell Rector</h2>
    <!-- /wp:heading -->

    <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"},"blockGap":"0"},"dimensions":{"minHeight":""}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
    <div class="wp-block-group" style="margin-bottom:var(--wp--preset--spacing--50)">
        <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"150px"}}} -->
        <hr class="wp-block-separator has-alpha-channel-opacity is-style-wide" />
        <!-- /wp:separator -->
    </div>
    <!-- /wp:group -->

    <!-- wp:group {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|50"}}},"layout":{"type":"constrained","contentSize":"80%","justifyContent":"left"}} -->
    <div class="wp-block-group" style="padding-bottom:var(--wp--preset--spacing--50)">
        <!-- wp:paragraph {"lock":{"move":false,"remove":false}} -->
        <p>El Consell Rector, escollit per les persones sòcies, s’encarrega d’implementar les directrius marcades per l’assemblea tot respectant els valors recollits en els estatuts de la cooperativa. Amb càrrecs voluntaris no remunerats, l’actual Consell Rector està format per:</p>
        <!-- /wp:paragraph -->
    </div>
    <!-- /wp:group -->

    <!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|70"}}}} -->
    <div class="wp-block-columns">
        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:image {"id":63652,"sizeSlug":"medium","linkDestination":"none","align":"center","className":"is-style-rounded"} -->
            <figure class="wp-block-image aligncenter size-medium is-style-rounded">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/668d9c71418ba1370f076190de688659-300x300.jpeg" alt="" class="wp-image-63652"/>
            </figure>
            <!-- /wp:image -->

            <!-- wp:heading {"level":3,"style":{"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|10"}}}} -->
            <h3 class="wp-block-heading" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--10)">Nom i cognoms</h3>
            <!-- /wp:heading -->

            <!-- wp:paragraph -->
            <p>L’assemblea agrupa a totes les persones socies i és l’òrgan màxim de decisió de la cooperativa: una persona, un vot.</p>
            <!-- /wp:paragraph -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:image {"id":63652,"sizeSlug":"medium","linkDestination":"none","align":"center","className":"is-style-rounded"} -->
            <figure class="wp-block-image aligncenter size-medium is-style-rounded">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/668d9c71418ba1370f076190de688659-300x300.jpeg" alt="" class="wp-image-63652"/>
            </figure>
            <!-- /wp:image -->

            <!-- wp:heading {"level":3,"style":{"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|10"}}}} -->
            <h3 class="wp-block-heading" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--10)">Nom i cognoms</h3>
            <!-- /wp:heading -->

            <!-- wp:paragraph -->
            <p>L’assemblea agrupa a totes les persones socies i és l’òrgan màxim de decisió de la cooperativa: una persona, un vot.</p>
            <!-- /wp:paragraph -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:image {"id":63652,"sizeSlug":"medium","linkDestination":"none","align":"center","className":"is-style-rounded"} -->
            <figure class="wp-block-image aligncenter size-medium is-style-rounded">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/668d9c71418ba1370f076190de688659-300x300.jpeg" alt="" class="wp-image-63652"/>
            </figure>
            <!-- /wp:image -->

            <!-- wp:heading {"level":3,"style":{"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|10"}}}} -->
            <h3 class="wp-block-heading" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--10)">Nom i cognoms</h3>
            <!-- /wp:heading -->

            <!-- wp:paragraph -->
            <p>L’assemblea agrupa a totes les persones socies i és l’òrgan màxim de decisió de la cooperativa: una persona, un vot.</p>
            <!-- /wp:paragraph -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:image {"id":63652,"sizeSlug":"medium","linkDestination":"none","align":"center","className":"is-style-rounded"} -->
            <figure class="wp-block-image aligncenter size-medium is-style-rounded">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/668d9c71418ba1370f076190de688659-300x300.jpeg" alt="" class="wp-image-63652"/>
            </figure>
            <!-- /wp:image -->

            <!-- wp:heading {"level":3,"style":{"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|10"}}}} -->
            <h3 class="wp-block-heading" style="margin-top:var(--wp--preset--spacing--30);margin-bottom:var(--wp--preset--spacing--10)">Nom i cognoms</h3>
            <!-- /wp:heading -->

            <!-- wp:paragraph -->
            <p>L’assemblea agrupa a totes les persones socies i és l’òrgan màxim de decisió de la cooperativa: una persona, un vot.</p>
            <!-- /wp:paragraph -->
        </div>
        <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
</section>
<!-- /wp:group -->
