<?php
/**
 * Title: Banner amb imatge de fons
 * Slug: sm-pattern/banner-image
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Banner amb imatge de fons"},"align":"full","style":{"background":{"backgroundImage":{"url":"https://sommobilitat.codeccoop.org/wp-content/uploads/2024/08/40cc8029aecbc2a63882c97b790677bb-scaled.jpeg","id":63767,"source":"file","title":"40cc8029aecbc2a63882c97b790677bb"},"backgroundPosition":"50% 0","backgroundSize":"cover"},"spacing":{"padding":{"top":"var:preset|spacing|90","bottom":"var:preset|spacing|90","left":"var:preset|spacing|40","right":"var:preset|spacing|40"}}},"layout":{"type":"constrained","contentSize":"500px"}} -->
<section class="wp-block-group alignfull" style="padding-top:var(--wp--preset--spacing--90);padding-right:var(--wp--preset--spacing--40);padding-bottom:var(--wp--preset--spacing--90);padding-left:var(--wp--preset--spacing--40)">
    <!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60","left":"var:preset|spacing|60","right":"var:preset|spacing|60"}},"layout":{"selfStretch":"fit","flexSize":null},"border":{"radius":"13px"}},"backgroundColor":"brand","layout":{"type":"constrained"}} -->
    <div class="wp-block-group has-brand-background-color has-background" style="border-radius:13px;padding-top:var(--wp--preset--spacing--60);padding-right:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60);padding-left:var(--wp--preset--spacing--60)">
        <!-- wp:heading {"textAlign":"center"} -->
        <h2 class="wp-block-heading has-text-align-center">Trobar cotxes</h2>
        <!-- /wp:heading -->

        <!-- wp:paragraph {"align":"center"} -->
        <p class="has-text-align-center">Aparcaments on hi ha els vehicles elèctrics compartits i informació sobre com accedir-hi</p>
        <!-- /wp:paragraph -->

        <!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
        <div class="wp-block-buttons">
            <!-- wp:button {"className":"is-style-arrow"} -->
            <div class="wp-block-button is-style-arrow"><a class="wp-block-button__link wp-element-button">Veure mapa</a></div>
            <!-- /wp:button -->
        </div>
        <!-- /wp:buttons -->
    </div>
    <!-- /wp:group -->
</section>
<!-- /wp:group -->
