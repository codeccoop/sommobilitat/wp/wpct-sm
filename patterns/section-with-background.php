
<?php

/**
 * Title: Secció amb color de fons
 * Slug: sm-pattern/section-with-background
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Secció amb color de fons","categories":["sm-pattern"],"patternName":"sm-pattern/section-with-background"},"align":"full","style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"backgroundColor":"brand","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull has-brand-background-color has-background" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70)"><!-- wp:heading {"textColor":"typography"} -->
<h2 class="wp-block-heading has-typography-color has-text-color">Secció amb color de fons</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"typography"} -->
<p class="has-typography-color has-text-color">Lorem fistrum te va a hasé pupitaa caballo blanco caballo negroorl quietooor por la gloria de mi madre pupita te voy a borrar el cerito diodeno papaar papaar. Sexuarl caballo blanco caballo negroorl por la gloria de mi madre fistro tiene musho peligro ese pedazo de te voy a borrar el cerito no puedor. Te voy a borrar el cerito condemor ahorarr se calle ustée diodeno. Por la gloria de mi madre amatomaa a gramenawer llevame al sircoo a peich la caidita amatomaa ese que llega.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"className":"is-style-arrow"} -->
<div class="wp-block-button is-style-arrow"><a class="wp-block-button__link wp-element-button">Ves-HI</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></section>
<!-- /wp:group -->
