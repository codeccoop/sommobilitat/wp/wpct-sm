<?php
/**
 * Title: Secció + imatge (esquera)
 * Slug: sm-pattern/section-image-left
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Secció + imatge (esquerra)","categories":["sm-pattern"],"patternName":"sm-pattern/section-image-left"},"className":"sm-section-col-image","style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60"}}},"layout":{"type":"constrained"}} -->
<section class="wp-block-group sm-section-col-image" style="padding-top:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60)">
    <!-- wp:columns {"templateLock":false,"lock":{"move":false,"remove":false},"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|40"}}}} -->
    <div class="wp-block-columns">
        <!-- wp:column {"lock":{"move":false,"remove":false}} -->
        <div class="wp-block-column">
            <!-- wp:image {"id":63571,"width":"auto","height":"300px","aspectRatio":"16/9","scale":"cover","sizeSlug":"full","linkDestination":"none","align":"left","className":"is-style-border-radius"} -->
            <figure class="wp-block-image alignleft size-full is-resized is-style-border-radius">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/06/Bitmap-1.png" alt="" class="wp-image-63571" style="aspect-ratio:16/9;object-fit:cover;width:auto;height:300px"/>
            </figure>
            <!-- /wp:image -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column {"style":{"spacing":{"padding":{"left":"0"}}}} -->
        <div class="wp-block-column" style="padding-left:0">
            <!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
            <h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--10)">Suma't al moviment</h2>
            <!-- /wp:heading -->

              <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
              <div
                class="wp-block-group"
                style="margin-bottom: var(--wp--preset--spacing--50)"
              >
                <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"150px"}}} -->
                <hr class="wp-block-separator has-alpha-channel-opacity is-style-wide" />
                <!-- /wp:separator -->
              </div>
              <!-- /wp:group -->

            <!-- wp:paragraph {"lock":{"move":false,"remove":false}} -->
            <p>Som Mobilitat és una cooperativa sense afany de lucre i ens dirigim a totes aquelles persones i col·lectius que volen canviar el seu model de mobilitat i apostar per un nou model que busca enfortir els béns comuns. Pensem en global i actuem en clau local; volem compartir les experiències d’èxit per replicar-les arreu del territori. Com més siguem, millors seran les solucions que podrem oferir.</p>
            <!-- /wp:paragraph -->

            <!-- wp:buttons {"lock":{"move":false,"remove":false}} -->
            <div class="wp-block-buttons">
                <!-- wp:button {"backgroundColor":"brand","textColor":"typography","className":"is-style-arrow","style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}}} -->
                <div class="wp-block-button is-style-arrow"><a class="wp-block-button__link has-typography-color has-brand-background-color has-text-color has-background has-link-color wp-element-button" href="/"><strong>NoSTRES COMUNITATS</strong></a></div>
                <!-- /wp:button -->
            </div>
            <!-- /wp:buttons -->
        </div>
        <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
</section>
<!-- /wp:group -->
