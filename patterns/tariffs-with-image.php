<?php
/**
 * Title: Tarifes amb imatge
 * Slug: sm-pattern/tariff-image
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","align":"full","style":{"spacing":{"margin":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50"}}},"layout":{"type":"constrained"}} -->
<section
  class="wp-block-group alignfull"
  style="
    margin-top: var(--wp--preset--spacing--50);
    margin-bottom: var(--wp--preset--spacing--50);
  "
>
  <!-- wp:columns {"align":"wide","style":{"spacing":{"blockGap":{"left":"var:preset|spacing|30"}}}} -->
  <div class="wp-block-columns alignwide">
    <!-- wp:column -->
    <div class="wp-block-column">
      <!-- wp:group {"style":{"border":{"radius":"12px"},"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40","left":"var:preset|spacing|40","right":"var:preset|spacing|40"}}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
      <div
        class="wp-block-group has-base-background-color has-background"
        style="
          border-radius: 12px;
          padding-top: var(--wp--preset--spacing--40);
          padding-right: var(--wp--preset--spacing--40);
          padding-bottom: var(--wp--preset--spacing--40);
          padding-left: var(--wp--preset--spacing--40);
        "
      >
        <!-- wp:image {"id":73221,"sizeSlug":"full","linkDestination":"none","align":"center","style":{"border":{"radius":"12px"}}} -->
        <figure class="wp-block-image aligncenter size-full has-custom-border">
          <img
            src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/12/SM_2017_11.jpg"
            alt=""
            class="wp-image-73221"
            style="border-radius: 12px"
          />
        </figure>
        <!-- /wp:image -->

        <!-- wp:group {"metadata":{"name":"Títol de secció","categories":["sm-pattern"],"patternName":"sm-pattern/section-title"},"layout":{"type":"constrained"}} -->
        <div class="wp-block-group">
          <!-- wp:heading {"level":3,"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
          <h3
            class="wp-block-heading"
            style="margin-bottom: var(--wp--preset--spacing--10)"
          >
            Lloguer flexible
          </h3>
          <!-- /wp:heading -->

          <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|20"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
          <div
            class="wp-block-group"
            style="margin-bottom: var(--wp--preset--spacing--20)"
          >
            <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"75px"}},"backgroundColor":"contrast"} -->
            <hr
              class="wp-block-separator has-text-color has-contrast-color has-alpha-channel-opacity has-contrast-background-color has-background is-style-wide"
            />
            <!-- /wp:separator -->
          </div>
          <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:paragraph {"style":{"spacing":{"margin":{"top":"0","bottom":"0"}}}} -->
        <p style="margin-top: 0; margin-bottom: 0">
          <strong
            >Lloga qualsevol dels vehicles 100 % elèctrics les hores o dies que
            necessitis.</strong
          >
        </p>
        <!-- /wp:paragraph -->

        <!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|10","padding":{"top":"var:preset|spacing|20","bottom":"var:preset|spacing|20"}}},"layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"bottom"}} -->
        <div
          class="wp-block-group"
          style="
            padding-top: var(--wp--preset--spacing--20);
            padding-bottom: var(--wp--preset--spacing--20);
          "
        >
          <!-- wp:paragraph {"style":{"layout":{"selfStretch":"fit","flexSize":null},"typography":{"lineHeight":"0.8"}},"fontSize":"xxx-large"} -->
          <p class="has-xxx-large-font-size" style="line-height: 0.8">
            <strong>9€</strong>
          </p>
          <!-- /wp:paragraph -->

          <!-- wp:paragraph -->
          <p><strong>/mes</strong></p>
          <!-- /wp:paragraph -->
        </div>
        <!-- /wp:group -->

        <!-- wp:list {"className":"is-style-checklist","style":{"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}}} -->
        <ul
          style="
            margin-top: var(--wp--preset--spacing--30);
            margin-bottom: var(--wp--preset--spacing--30);
          "
          class="wp-block-list is-style-checklist"
        >
          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Plaça d’aparcament 24 hores</strong> amb punt de recàrrega
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Reserva, obre i tanca el vehicle</strong> a través de l’App
            de Som Mobilitat
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Dona d’alta als usuaris</strong> i gestiona l’ús a través de
            l’APP
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Informes</strong> d’usos i emissionsç
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-uncheck"} -->
          <li class="is-style-checklist-uncheck">
            <strong>Sense estalvi</strong>
          </li>
          <!-- /wp:list-item -->
        </ul>
        <!-- /wp:list -->

        <!-- wp:buttons {"style":{"spacing":{"padding":{"top":"var:preset|spacing|10","bottom":"var:preset|spacing|10"}}}} -->
        <div
          class="wp-block-buttons"
          style="
            padding-top: var(--wp--preset--spacing--10);
            padding-bottom: var(--wp--preset--spacing--10);
          "
        >
          <!-- wp:button {"backgroundColor":"brand","textColor":"contrast","className":"is-style-arrow","style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"}}}}} -->
          <div class="wp-block-button is-style-arrow">
            <a
              class="wp-block-button__link has-contrast-color has-brand-background-color has-text-color has-background has-link-color wp-element-button"
              href="#"
              >Contractar</a
            >
          </div>
          <!-- /wp:button -->
        </div>
        <!-- /wp:buttons -->
      </div>
      <!-- /wp:group -->
    </div>
    <!-- /wp:column -->

    <!-- wp:column -->
    <div class="wp-block-column">
      <!-- wp:group {"style":{"border":{"radius":"12px"},"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40","left":"var:preset|spacing|40","right":"var:preset|spacing|40"}}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
      <div
        class="wp-block-group has-base-background-color has-background"
        style="
          border-radius: 12px;
          padding-top: var(--wp--preset--spacing--40);
          padding-right: var(--wp--preset--spacing--40);
          padding-bottom: var(--wp--preset--spacing--40);
          padding-left: var(--wp--preset--spacing--40);
        "
      >
        <!-- wp:image {"id":73221,"sizeSlug":"full","linkDestination":"none","align":"center","style":{"border":{"radius":"12px"}}} -->
        <figure class="wp-block-image aligncenter size-full has-custom-border">
          <img
            src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/12/SM_2017_11.jpg"
            alt=""
            class="wp-image-73221"
            style="border-radius: 12px"
          />
        </figure>
        <!-- /wp:image -->

        <!-- wp:group {"metadata":{"name":"Títol de secció","categories":["sm-pattern"],"patternName":"sm-pattern/section-title"},"layout":{"type":"constrained"}} -->
        <div class="wp-block-group">
          <!-- wp:heading {"level":3,"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
          <h3
            class="wp-block-heading"
            style="margin-bottom: var(--wp--preset--spacing--10)"
          >
            Lloguer flexible
          </h3>
          <!-- /wp:heading -->

          <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|20"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
          <div
            class="wp-block-group"
            style="margin-bottom: var(--wp--preset--spacing--20)"
          >
            <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"75px"}},"backgroundColor":"contrast"} -->
            <hr
              class="wp-block-separator has-text-color has-contrast-color has-alpha-channel-opacity has-contrast-background-color has-background is-style-wide"
            />
            <!-- /wp:separator -->
          </div>
          <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:paragraph {"style":{"spacing":{"margin":{"top":"0","bottom":"0"}}}} -->
        <p style="margin-top: 0; margin-bottom: 0">
          <strong
            >Lloga qualsevol dels vehicles 100 % elèctrics les hores o dies que
            necessitis.</strong
          >
        </p>
        <!-- /wp:paragraph -->

        <!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|10","padding":{"top":"var:preset|spacing|20","bottom":"var:preset|spacing|20"}}},"layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"bottom"}} -->
        <div
          class="wp-block-group"
          style="
            padding-top: var(--wp--preset--spacing--20);
            padding-bottom: var(--wp--preset--spacing--20);
          "
        >
          <!-- wp:paragraph {"style":{"layout":{"selfStretch":"fit","flexSize":null},"typography":{"lineHeight":"0.8"}},"fontSize":"xxx-large"} -->
          <p class="has-xxx-large-font-size" style="line-height: 0.8">
            <strong>9€</strong>
          </p>
          <!-- /wp:paragraph -->

          <!-- wp:paragraph -->
          <p><strong>/mes</strong></p>
          <!-- /wp:paragraph -->
        </div>
        <!-- /wp:group -->

        <!-- wp:list {"className":"is-style-checklist","style":{"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}}} -->
        <ul
          style="
            margin-top: var(--wp--preset--spacing--30);
            margin-bottom: var(--wp--preset--spacing--30);
          "
          class="wp-block-list is-style-checklist"
        >
          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Plaça d’aparcament 24 hores</strong> amb punt de recàrrega
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Reserva, obre i tanca el vehicle</strong> a través de l’App
            de Som Mobilitat
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Dona d’alta als usuaris</strong> i gestiona l’ús a través de
            l’APP
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Informes</strong> d’usos i emissionsç
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-uncheck"} -->
          <li class="is-style-checklist-uncheck">
            <strong>Sense estalvi</strong>
          </li>
          <!-- /wp:list-item -->
        </ul>
        <!-- /wp:list -->

        <!-- wp:buttons {"style":{"spacing":{"padding":{"top":"var:preset|spacing|10","bottom":"var:preset|spacing|10"}}}} -->
        <div
          class="wp-block-buttons"
          style="
            padding-top: var(--wp--preset--spacing--10);
            padding-bottom: var(--wp--preset--spacing--10);
          "
        >
          <!-- wp:button {"backgroundColor":"brand","textColor":"contrast","className":"is-style-arrow","style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"}}}}} -->
          <div class="wp-block-button is-style-arrow">
            <a
              class="wp-block-button__link has-contrast-color has-brand-background-color has-text-color has-background has-link-color wp-element-button"
              href="#"
              >Contractar</a
            >
          </div>
          <!-- /wp:button -->
        </div>
        <!-- /wp:buttons -->
      </div>
      <!-- /wp:group -->
    </div>
    <!-- /wp:column -->

    <!-- wp:column -->
    <div class="wp-block-column">
      <!-- wp:group {"style":{"border":{"radius":"12px"},"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40","left":"var:preset|spacing|40","right":"var:preset|spacing|40"}}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
      <div
        class="wp-block-group has-base-background-color has-background"
        style="
          border-radius: 12px;
          padding-top: var(--wp--preset--spacing--40);
          padding-right: var(--wp--preset--spacing--40);
          padding-bottom: var(--wp--preset--spacing--40);
          padding-left: var(--wp--preset--spacing--40);
        "
      >
        <!-- wp:image {"id":73221,"sizeSlug":"full","linkDestination":"none","align":"center","style":{"border":{"radius":"12px"}}} -->
        <figure class="wp-block-image aligncenter size-full has-custom-border">
          <img
            src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/12/SM_2017_11.jpg"
            alt=""
            class="wp-image-73221"
            style="border-radius: 12px"
          />
        </figure>
        <!-- /wp:image -->

        <!-- wp:group {"metadata":{"name":"Títol de secció","categories":["sm-pattern"],"patternName":"sm-pattern/section-title"},"layout":{"type":"constrained"}} -->
        <div class="wp-block-group">
          <!-- wp:heading {"level":3,"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
          <h3
            class="wp-block-heading"
            style="margin-bottom: var(--wp--preset--spacing--10)"
          >
            Lloguer flexible
          </h3>
          <!-- /wp:heading -->

          <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|20"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
          <div
            class="wp-block-group"
            style="margin-bottom: var(--wp--preset--spacing--20)"
          >
            <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"75px"}},"backgroundColor":"contrast"} -->
            <hr
              class="wp-block-separator has-text-color has-contrast-color has-alpha-channel-opacity has-contrast-background-color has-background is-style-wide"
            />
            <!-- /wp:separator -->
          </div>
          <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:paragraph {"style":{"spacing":{"margin":{"top":"0","bottom":"0"}}}} -->
        <p style="margin-top: 0; margin-bottom: 0">
          <strong
            >Lloga qualsevol dels vehicles 100 % elèctrics les hores o dies que
            necessitis.</strong
          >
        </p>
        <!-- /wp:paragraph -->

        <!-- wp:group {"style":{"spacing":{"blockGap":"var:preset|spacing|10","padding":{"top":"var:preset|spacing|20","bottom":"var:preset|spacing|20"}}},"layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"bottom"}} -->
        <div
          class="wp-block-group"
          style="
            padding-top: var(--wp--preset--spacing--20);
            padding-bottom: var(--wp--preset--spacing--20);
          "
        >
          <!-- wp:paragraph {"style":{"layout":{"selfStretch":"fit","flexSize":null},"typography":{"lineHeight":"0.8"}},"fontSize":"xxx-large"} -->
          <p class="has-xxx-large-font-size" style="line-height: 0.8">
            <strong>9€</strong>
          </p>
          <!-- /wp:paragraph -->

          <!-- wp:paragraph -->
          <p><strong>/mes</strong></p>
          <!-- /wp:paragraph -->
        </div>
        <!-- /wp:group -->

        <!-- wp:list {"className":"is-style-checklist","style":{"spacing":{"margin":{"top":"var:preset|spacing|30","bottom":"var:preset|spacing|30"}}}} -->
        <ul
          style="
            margin-top: var(--wp--preset--spacing--30);
            margin-bottom: var(--wp--preset--spacing--30);
          "
          class="wp-block-list is-style-checklist"
        >
          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Plaça d’aparcament 24 hores</strong> amb punt de recàrrega
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Reserva, obre i tanca el vehicle</strong> a través de l’App
            de Som Mobilitat
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Dona d’alta als usuaris</strong> i gestiona l’ús a través de
            l’APP
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-check"} -->
          <li class="is-style-checklist-check">
            <strong>Informes</strong> d’usos i emissionsç
          </li>
          <!-- /wp:list-item -->

          <!-- wp:list-item {"className":"is-style-checklist-uncheck"} -->
          <li class="is-style-checklist-uncheck">
            <strong>Sense estalvi</strong>
          </li>
          <!-- /wp:list-item -->
        </ul>
        <!-- /wp:list -->

        <!-- wp:buttons {"style":{"spacing":{"padding":{"top":"var:preset|spacing|10","bottom":"var:preset|spacing|10"}}}} -->
        <div
          class="wp-block-buttons"
          style="
            padding-top: var(--wp--preset--spacing--10);
            padding-bottom: var(--wp--preset--spacing--10);
          "
        >
          <!-- wp:button {"backgroundColor":"brand","textColor":"contrast","className":"is-style-arrow","style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"}}}}} -->
          <div class="wp-block-button is-style-arrow">
            <a
              class="wp-block-button__link has-contrast-color has-brand-background-color has-text-color has-background has-link-color wp-element-button"
              href="#"
              >Contractar</a
            >
          </div>
          <!-- /wp:button -->
        </div>
        <!-- /wp:buttons -->
      </div>
      <!-- /wp:group -->
    </div>
    <!-- /wp:column -->
  </div>
  <!-- /wp:columns -->
</section>
<!-- /wp:group -->
