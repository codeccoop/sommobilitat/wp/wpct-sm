<?php
/**
 * Title: Banner amb ombrejat
 * Slug: sm-pattern/banner-shadow
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"categories":["sm-pattern"],"patternName":"sm-pattern/banner-shadow","name":"Banner ombrejat"},"align":"full","style":{"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull" style="padding-top: var(--wp--preset--spacing--40); padding-bottom: var(--wp--preset--spacing--40)">
  <!-- wp:group {"templateLock":"all","lock":{"move":true,"remove":true},"align":"wide","className":"sm-banner sm-banner-shadow","style":{"spacing":{"padding":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50","left":"var:preset|spacing|60","right":"var:preset|spacing|50"}},"border":{"radius":"12px"}},"backgroundColor":"brand","layout":{"type":"constrained"}} -->
  <div
    class="wp-block-group alignwide sm-banner sm-banner-shadow has-brand-background-color has-background"
    style="
      border-radius: 12px;
      padding-top: var(--wp--preset--spacing--50);
      padding-right: var(--wp--preset--spacing--50);
      padding-bottom: var(--wp--preset--spacing--50);
      padding-left: var(--wp--preset--spacing--60);
    "
  >
    <!-- wp:columns {"align":"wide"} -->
    <div class="wp-block-columns alignwide">
      <!-- wp:column {"width":"55%"} -->
      <div class="wp-block-column" style="flex-basis: 55%">
        <!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"right":"var:preset|spacing|20","left":"var:preset|spacing|20"}}},"layout":{"type":"constrained"}} -->
        <div
          class="wp-block-group alignwide"
          style="
            padding-right: var(--wp--preset--spacing--20);
            padding-left: var(--wp--preset--spacing--20);
          "
        >
          <!-- wp:heading -->
          <h2 class="wp-block-heading">Fes-te'n soci:a!</h2>
          <!-- /wp:heading -->

          <!-- wp:paragraph -->
          <p>
            Ser soci/a et permetrà gaudir de tots els serveis de Som Mobilitat i
            ens ajudarà a fer realitat la cooperativa.
          </p>
          <!-- /wp:paragraph -->

          <!-- wp:buttons -->
          <div class="wp-block-buttons">
            <!-- wp:button {"className":"is-style-arrow"} -->
            <div class="wp-block-button is-style-arrow">
              <a class="wp-block-button__link wp-element-button">SOM-HI</a>
            </div>
            <!-- /wp:button -->
          </div>
          <!-- /wp:buttons -->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column {"verticalAlignment":"stretch","width":"45%"} -->
      <div
        class="wp-block-column is-vertically-aligned-stretch"
        style="flex-basis: 45%"
      >
        <!-- wp:image {"id":63564,"scale":"cover","sizeSlug":"large","linkDestination":"none","align":"center","style":{"border":{"radius":"12px"}}} -->
        <figure class="wp-block-image aligncenter size-large has-custom-border">
          <img
            src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/04/Vehicles_flota_SomMobilitat_007-7-1024x385.jpg"
            alt=""
            class="wp-image-63564"
            style="border-radius: 12px; object-fit: cover"
          />
        </figure>
        <!-- /wp:image -->
      </div>
      <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
  </div>
  <!-- /wp:group -->
</section>
<!-- /wp:group -->
