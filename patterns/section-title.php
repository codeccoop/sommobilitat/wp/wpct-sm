<?php
/**
 * Title: Títol de secció
 * Slug: sm-pattern/section-title
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"metadata":{"name":"Títol de secció","categories":["sm-pattern"],"patternName":"sm-pattern/section-title"},"layout":{"type":"constrained"}} -->
<div class="wp-block-group">
  <!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
  <h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--10)">Títol</h2>
  <!-- /wp:heading -->

  <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
  <div
    class="wp-block-group"
    style="margin-bottom: var(--wp--preset--spacing--50)"
  >
    <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"150px"}}} -->
    <hr class="wp-block-separator has-alpha-channel-opacity is-style-wide" />
    <!-- /wp:separator -->
  </div>
  <!-- /wp:group -->
</div>
<!-- /wp:group -->
