<?php
/**
 * Title: SM Stepper
 * Slug: sm-pattern/stepper
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"metadata":{"name":"SM Stepper"},"style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70)">
    <!-- wp:wpct-block/stepper -->
    <div class="wp-block-wpct-block-stepper wpct-block-stepper wp-block-group">
        <!-- wp:group {"templateLock":false,"metadata":{"name":"SM Step"},"className":"wpct-block-stepper-step wp-block-group"} -->
        <div class="wp-block-group wpct-block-stepper-step">
            <!-- wp:image {"id":63669,"aspectRatio":"1","scale":"cover","sizeSlug":"full","linkDestination":"none","lock":{"remove":false}} -->
            <figure class="wp-block-image size-full">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg" alt="" class="wp-image-63669" style="aspect-ratio:1;object-fit:cover"/>
            </figure>
            <!-- /wp:image -->

            <!-- wp:heading {"level":4,"lock":{"remove":false},"style":{"spacing":{"margin":{"top":"0","bottom":"0"}}}} -->
            <h4 class="wp-block-heading" style="margin-top:0;margin-bottom:0">Step title</h4>
            <!-- /wp:heading -->

            <!-- wp:paragraph -->
            <p>Step description</p>
            <!-- /wp:paragraph -->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"templateLock":false,"metadata":{"name":"SM Step"},"className":"wpct-block-stepper-step wp-block-group"} -->
        <div class="wp-block-group wpct-block-stepper-step">
            <!-- wp:image {"id":63670,"aspectRatio":"1","scale":"cover","sizeSlug":"full","linkDestination":"none","lock":{"remove":false}} -->
            <figure class="wp-block-image size-full">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/31466da02360eecbc77606b8d42da6ab.jpeg" alt="" class="wp-image-63670" style="aspect-ratio:1;object-fit:cover"/>
            </figure>
            <!-- /wp:image -->

            <!-- wp:heading {"level":4,"lock":{"remove":false},"style":{"spacing":{"margin":{"top":"0","bottom":"0"}}}} -->
            <h4 class="wp-block-heading" style="margin-top:0;margin-bottom:0">Step title</h4>
            <!-- /wp:heading -->

            <!-- wp:paragraph -->
            <p>Step description</p>
            <!-- /wp:paragraph -->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"templateLock":false,"metadata":{"name":"SM Step"},"className":"wpct-block-stepper-step wp-block-group"} -->
        <div class="wp-block-group wpct-block-stepper-step">
            <!-- wp:image {"id":63671,"aspectRatio":"1","scale":"cover","sizeSlug":"full","linkDestination":"none","lock":{"remove":false}} -->
            <figure class="wp-block-image size-full">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/ce7b7579debab3acbc8e46eafc136c5b.jpeg" alt="" class="wp-image-63671" style="aspect-ratio:1;object-fit:cover"/>
            </figure>
            <!-- /wp:image -->

            <!-- wp:heading {"level":4,"lock":{"remove":false},"style":{"spacing":{"margin":{"top":"0","bottom":"0"}}}} -->
            <h4 class="wp-block-heading" style="margin-top:0;margin-bottom:0">Step title</h4>
            <!-- /wp:heading -->

            <!-- wp:paragraph -->
            <p>Step description</p>
            <!-- /wp:paragraph -->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"templateLock":false,"metadata":{"name":"SM Step"},"className":"wpct-block-stepper-step wp-block-group"} -->
        <div class="wp-block-group wpct-block-stepper-step">
            <!-- wp:image {"id":63672,"aspectRatio":"1","scale":"cover","sizeSlug":"full","linkDestination":"none","lock":{"remove":false}} -->
            <figure class="wp-block-image size-full">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/f0ad260a799e6de0baafe061a582f995.jpeg" alt="" class="wp-image-63672" style="aspect-ratio:1;object-fit:cover"/>
            </figure>
            <!-- /wp:image -->

            <!-- wp:heading {"level":4,"lock":{"remove":false},"style":{"spacing":{"margin":{"top":"0","bottom":"0"}}}} -->
            <h4 class="wp-block-heading" style="margin-top:0;margin-bottom:0">Step title</h4>
            <!-- /wp:heading -->

            <!-- wp:paragraph -->
            <p>Step description</p>
            <!-- /wp:paragraph -->
        </div>
        <!-- /wp:group -->
    </div>
    <!-- /wp:wpct-block/stepper -->
</div>
<!-- /wp:group -->
