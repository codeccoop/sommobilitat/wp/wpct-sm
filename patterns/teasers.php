<?php
/**
 * Title: Teasers
 * Slug: sm-pattern/teasers
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"metadata":{"name":"Teasers"},"style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70)">
    <!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|40"}}}} -->
    <div class="wp-block-columns">
        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:wpct-block/teaser {"image":"https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg","background":"var(\u002d\u002dwp\u002d\u002dpreset\u002d\u002dcolor\u002d\u002dla-rioja)"} -->
            <figure class="wp-block-wpct-block-teaser" style="background:var(--wp--preset--color--la-rioja)">
                <a href="/"><img class="wpct-teaser-thumbnail" src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg"/></a>
                <figcaption>
                    <!-- wp:group {"templateLock":false,"className":"wpct-teaser-content","style":{"spacing":{"blockGap":"0","padding":{"top":"0","bottom":"0"},"margin":{"top":"0","bottom":"0"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
                    <div class="wp-block-group wpct-teaser-content" style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0">
                        <!-- wp:heading {"level":4,"placeholder":"Teaser title"} -->
                        <h4 class="wp-block-heading">Teaser 1</h4>
                        <!-- /wp:heading -->

                        <!-- wp:paragraph {"placeholder":"Teaser text"} -->
                        <p>Lorem ipsum dolor sit amer</p>
                        <!-- /wp:paragraph -->
                    </div>
                    <!-- /wp:group -->
                    <p class="wpct-teaser-cta"><a class="wpct-teaser-link" href="/">Veure més</a></p>
                </figcaption>
            </figure>
            <!-- /wp:wpct-block/teaser -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:wpct-block/teaser {"image":"https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg","background":"var(\u002d\u002dwp\u002d\u002dpreset\u002d\u002dcolor\u002d\u002dla-rioja)"} -->
            <figure class="wp-block-wpct-block-teaser" style="background:var(--wp--preset--color--la-rioja)">
                <a href="/"><img class="wpct-teaser-thumbnail" src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg"/></a>
                <figcaption>
                    <!-- wp:group {"templateLock":false,"className":"wpct-teaser-content","style":{"spacing":{"blockGap":"0","padding":{"top":"0","bottom":"0"},"margin":{"top":"0","bottom":"0"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
                    <div class="wp-block-group wpct-teaser-content" style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0">
                        <!-- wp:heading {"level":4,"placeholder":"Teaser title"} -->
                        <h4 class="wp-block-heading">Teaser 1</h4>
                        <!-- /wp:heading -->

                        <!-- wp:paragraph {"placeholder":"Teaser text"} -->
                        <p>Lorem ipsum dolor sit amer</p>
                        <!-- /wp:paragraph -->
                    </div>
                    <!-- /wp:group -->
                    <p class="wpct-teaser-cta"><a class="wpct-teaser-link" href="/">Veure més</a></p>
                </figcaption>
            </figure>
            <!-- /wp:wpct-block/teaser -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:wpct-block/teaser {"image":"https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg","background":"var(\u002d\u002dwp\u002d\u002dpreset\u002d\u002dcolor\u002d\u002dla-rioja)"} -->
            <figure class="wp-block-wpct-block-teaser" style="background:var(--wp--preset--color--la-rioja)">
                <a href="/"><img class="wpct-teaser-thumbnail" src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg"/></a>
                <figcaption>
                    <!-- wp:group {"templateLock":false,"className":"wpct-teaser-content","style":{"spacing":{"blockGap":"0","padding":{"top":"0","bottom":"0"},"margin":{"top":"0","bottom":"0"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
                    <div class="wp-block-group wpct-teaser-content" style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0">
                        <!-- wp:heading {"level":4,"placeholder":"Teaser title"} -->
                        <h4 class="wp-block-heading">Teaser 1</h4>
                        <!-- /wp:heading -->

                        <!-- wp:paragraph {"placeholder":"Teaser text"} -->
                        <p>Lorem ipsum dolor sit amer</p>
                        <!-- /wp:paragraph -->
                    </div>
                    <!-- /wp:group -->
                    <p class="wpct-teaser-cta"><a class="wpct-teaser-link" href="/">Veure més</a></p>
                </figcaption>
            </figure>
            <!-- /wp:wpct-block/teaser -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:wpct-block/teaser {"image":"https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg","background":"var(\u002d\u002dwp\u002d\u002dpreset\u002d\u002dcolor\u002d\u002dla-rioja)"} -->
            <figure class="wp-block-wpct-block-teaser" style="background:var(--wp--preset--color--la-rioja)">
                <a href="/"><img class="wpct-teaser-thumbnail" src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg"/></a>
                <figcaption>
                    <!-- wp:group {"templateLock":false,"className":"wpct-teaser-content","style":{"spacing":{"blockGap":"0","padding":{"top":"0","bottom":"0"},"margin":{"top":"0","bottom":"0"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
                    <div class="wp-block-group wpct-teaser-content" style="margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0">
                        <!-- wp:heading {"level":4,"placeholder":"Teaser title"} -->
                        <h4 class="wp-block-heading">Teaser 1</h4>
                        <!-- /wp:heading -->

                        <!-- wp:paragraph {"placeholder":"Teaser text"} -->
                        <p>Lorem ipsum dolor sit amer</p>
                        <!-- /wp:paragraph -->
                    </div>
                    <!-- /wp:group -->
                    <p class="wpct-teaser-cta"><a class="wpct-teaser-link" href="/">Veure més</a></p>
                </figcaption>
            </figure>
            <!-- /wp:wpct-block/teaser -->
        </div>
        <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
</div>
<!-- /wp:group -->
