<?php
/**
 * Title: Accordió de FAQs
 * Slug: sm-pattern/faqs-accordion
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","style":{"spacing":{"margin":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|70"}}},"layout":{"type":"constrained"}} -->
<section
  class="wp-block-group"
  style="
    margin-top: var(--wp--preset--spacing--50);
    margin-bottom: var(--wp--preset--spacing--70);
  "
>
  <!-- wp:group {"metadata":{"name":"Títol de secció","categories":["sm-pattern"],"patternName":"sm-pattern/section-title"},"layout":{"type":"constrained"}} -->
  <div class="wp-block-group">
    <!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
    <h2
      class="wp-block-heading"
      style="margin-bottom: var(--wp--preset--spacing--10)"
    >
      Carsharing
    </h2>
    <!-- /wp:heading -->

    <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
    <div
      class="wp-block-group"
      style="margin-bottom: var(--wp--preset--spacing--50)"
    >
      <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"150px"}}} -->
      <hr class="wp-block-separator has-alpha-channel-opacity is-style-wide" />
      <!-- /wp:separator -->
    </div>
    <!-- /wp:group -->
  </div>
  <!-- /wp:group -->

  <!-- wp:wpct-block/accordion-loop -->
  <div
    class="wp-block-wpct-block-accordion-loop wpct-block-accordion wpct-block-accordion-loop"
    data-active="0"
    data-animate="true"
    data-collapsible="false"
    data-event="click"
    data-height="content"
  >
    <!-- wp:query {"queryId":1,"query":{"postType":"faqs","order":"asc","orderBy":"title","author":"","perPage":9,"pages":0,"offset":0,"exclude":[],"sticky":"","inherit":false,"taxQuery":{"category":[77]},"parents":[],"format":[]},"layout":{"type":"constrained"}} -->
    <div class="wp-block-query">
      <!-- wp:post-template {"layout":{"type":"constrained"}} -->
      <!-- wp:wpct-block/accordion-section {"layout":{"type":"constrained"}} -->
      <div
        class="wp-block-wpct-block-accordion-section wpct-block-accordion-section"
      >
        <!-- wp:group {"lock":{"remove":true,"move":true},"metadata":{"name":"Accordion title"},"className":"wpct-accordion-section-header","style":{"border":{"radius":"12px"},"spacing":{"padding":{"top":"var:preset|spacing|20","bottom":"var:preset|spacing|20","left":"var:preset|spacing|40","right":"var:preset|spacing|40"},"margin":{"bottom":"var:preset|spacing|30"}}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
        <div
          class="wp-block-group wpct-accordion-section-header has-base-background-color has-background"
          style="
            border-radius: 12px;
            margin-bottom: var(--wp--preset--spacing--30);
            padding-top: var(--wp--preset--spacing--20);
            padding-right: var(--wp--preset--spacing--40);
            padding-bottom: var(--wp--preset--spacing--20);
            padding-left: var(--wp--preset--spacing--40);
          "
        >
          <!-- wp:post-title {"level":3,"style":{"spacing":{"margin":{"top":"0","bottom":"0"},"padding":{"top":"0","bottom":"0"}}},"fontSize":"large"} /-->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"lock":{"remove":true,"move":true},"metadata":{"name":"Content"},"className":"wpct-accordion-section-content","style":{"spacing":{"padding":{"top":"0","bottom":"var:preset|spacing|20","left":"var:preset|spacing|40","right":"var:preset|spacing|40"},"margin":{"bottom":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
        <div
          class="wp-block-group wpct-accordion-section-content"
          style="
            margin-bottom: var(--wp--preset--spacing--40);
            padding-top: 0;
            padding-right: var(--wp--preset--spacing--40);
            padding-bottom: var(--wp--preset--spacing--20);
            padding-left: var(--wp--preset--spacing--40);
          "
        >
          <!-- wp:post-content /-->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:wpct-block/accordion-section -->
      <!-- /wp:post-template -->
    </div>
    <!-- /wp:query -->
  </div>
  <!-- /wp:wpct-block/accordion-loop -->
</section>
<!-- /wp:group -->
