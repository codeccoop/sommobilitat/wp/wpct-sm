<?php
/**
 * Title: Capçalera de pàgina
 * Slug: sm-pattern/page-heading
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"metadata":{"name":"Capçelera de pàgina"},"align":"wide","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide"><!-- wp:wpct-block/page-header -->
<!-- wp:post-title {"level":1} /-->

<!-- wp:group {"templateLock":false,"className":"wpct-block-page-header-content"} -->
<div class="wp-block-group wpct-block-page-header-content"><!-- wp:post-excerpt /-->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button"></a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group -->
<!-- /wp:wpct-block/page-header --></div>
<!-- /wp:group -->
