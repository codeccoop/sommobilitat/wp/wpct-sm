<?php
/**
 * Title: Banner Lite
 * Slug: sm-pattern/banner-lite
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Banner Lite","categories":["sm-pattern"],"patternName":"sm-pattern/banner-lite"},"align":"full","className":"sm-banner-wrapper","style":{"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
<section
  class="wp-block-group alignfull sm-banner-wrapper"
  style="
    padding-top: var(--wp--preset--spacing--40);
    padding-bottom: var(--wp--preset--spacing--40);
  "
>
  <!-- wp:group {"templateLock":"all","lock":{"move":true,"remove":true},"align":"wide","className":"sm-banner-lite","style":{"spacing":{"padding":{"top":"0","bottom":"0","left":"var:preset|spacing|50","right":"0"}},"border":{"radius":"12px"}},"backgroundColor":"swans-downs","layout":{"type":"default"}} -->
  <div
    class="wp-block-group alignwide sm-banner-lite has-swans-downs-background-color has-background"
    style="
      border-radius: 12px;
      padding-top: 0;
      padding-right: 0;
      padding-bottom: 0;
      padding-left: var(--wp--preset--spacing--50);
    "
  >
    <!-- wp:columns {"verticalAlignment":"center","align":"wide","style":{"spacing":{"padding":{"right":"0","left":"0","top":"0","bottom":"0"}}}} -->
    <div
      class="wp-block-columns alignwide are-vertically-aligned-center"
      style="
        padding-top: 0;
        padding-right: 0;
        padding-bottom: 0;
        padding-left: 0;
      "
    >
      <!-- wp:column {"verticalAlignment":"center","width":"65%","style":{"spacing":{"padding":{"right":"var:preset|spacing|40","left":"var:preset|spacing|40"}}}} -->
      <div
        class="wp-block-column is-vertically-aligned-center"
        style="
          padding-right: var(--wp--preset--spacing--40);
          padding-left: var(--wp--preset--spacing--40);
          flex-basis: 65%;
        "
      >
        <!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"right":"var:preset|spacing|30","left":"0","top":"var:preset|spacing|40","bottom":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
        <div
          class="wp-block-group alignwide"
          style="
            padding-top: var(--wp--preset--spacing--40);
            padding-right: var(--wp--preset--spacing--30);
            padding-bottom: var(--wp--preset--spacing--40);
            padding-left: 0;
          "
        >
          <!-- wp:heading {"level":3} -->
          <h3 class="wp-block-heading">
            Descobreix la triple solució per a entitats  i empreses amb valors
            mediambientals i socials.
          </h3>
          <!-- /wp:heading -->

          <!-- wp:wpct-block/external-link {"arrow":"internal","fontSize":"medium"} -->
          <p
            class="wp-block-wpct-block-external-link wpct-external-link has-medium-font-size"
            data-arrow="internal"
          >
            <a href="#" target="_self">Link</a>
          </p>
          <!-- /wp:wpct-block/external-link -->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column {"verticalAlignment":"center","width":"35%"} -->
      <div
        class="wp-block-column is-vertically-aligned-center"
        style="flex-basis: 35%"
      >
        <!-- wp:image {"id":74749,"scale":"cover","sizeSlug":"large","linkDestination":"none","align":"right","style":{"border":{"radius":{"topRight":"12px","bottomRight":"12px","topLeft":"0px","bottomLeft":"0px"}},"spacing":{"margin":{"top":"0","bottom":"0","left":"0","right":"0"}}}} -->
        <figure
          class="wp-block-image alignright size-large has-custom-border"
          style="
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
          "
        >
          <img
            src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/12/zoeikangoo_Vic_ServeisMunicipals-6-1-scaled-1-1024x644.jpg"
            alt=""
            class="wp-image-74749"
            style="
              border-top-left-radius: 0px;
              border-top-right-radius: 12px;
              border-bottom-left-radius: 0px;
              border-bottom-right-radius: 12px;
              object-fit: cover;
            "
          />
        </figure>
        <!-- /wp:image -->
      </div>
      <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
  </div>
  <!-- /wp:group -->
</section>
<!-- /wp:group -->
