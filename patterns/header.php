<?php

/**
 * Title: SM Header
 * Slug: sm-pattern/header
 * Categories: wpct-header
 * Viewport Width: 1500
 */

?>

<!-- wp:group {"backgroundColor":"base","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-base-background-color has-background"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|20","bottom":"var:preset|spacing|20"},"margin":{"top":"0px","bottom":"0"}}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
<div class="wp-block-group" style="margin-top:0px;margin-bottom:0;padding-top:var(--wp--preset--spacing--20);padding-bottom:var(--wp--preset--spacing--20)"><!-- wp:site-logo {"width":80,"shouldSyncIcon":true} /-->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"left","verticalAlignment":"top"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical","justifyContent":"right","verticalAlignment":"space-between"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"bottom":"var:preset|spacing|20"}}},"className":"sm-menu-top-navigation is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right","verticalAlignment":"center"}} -->
<div class="wp-block-group sm-menu-top-navigation is-style-show-desktop" style="padding-bottom:var(--wp--preset--spacing--20)"><!-- wp:navigation {"ref":25,"overlayMenu":"never","hasIcon":false} /--></div>
<!-- /wp:group -->

<!-- wp:navigation {"ref":4,"textColor":"black","overlayMenu":"never","className":"is-style-show-desktop","layout":{"type":"flex"},"style":{"spacing":{"blockGap":"var:preset|spacing|20"},"layout":{"selfStretch":"fit","flexSize":null}},"fontSize":"medium"} /-->

<!-- wp:group {"className":"is-style-show-mobile-tablet","layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group is-style-show-mobile-tablet"><!-- wp:paragraph {"style":{"layout":{"selfStretch":"fit","flexSize":null}},"fontSize":"medium"} -->
<p class="has-medium-font-size"><strong>Menú</strong></p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","width":"0.5rem","style":{"layout":{"flexSize":"1rem","selfStretch":"fixed"}}} -->
<div style="height:0px;width:0.5rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:navigation {"ref":521,"textColor":"black","overlayMenu":"always","icon":"menu","layout":{"type":"flex"},"style":{"spacing":{"blockGap":"var:preset|spacing|20"},"layout":{"selfStretch":"fit","flexSize":null}},"fontSize":"medium"} /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"metadata":{"name":""},"style":{"spacing":{"padding":{"left":"var:preset|spacing|20"}}},"className":"is-style-show-desktop","layout":{"type":"flex","orientation":"vertical","justifyContent":"right"}} -->
<div class="wp-block-group is-style-show-desktop" style="padding-left:var(--wp--preset--spacing--20)"><!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"brand","textColor":"typography","style":{"spacing":{"padding":{"left":"var:preset|spacing|20","right":"var:preset|spacing|20","top":"0","bottom":"0"}},"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}},"fontSize":"x-small"} -->
<div class="wp-block-button has-custom-font-size has-x-small-font-size"><a class="wp-block-button__link has-typography-color has-brand-background-color has-text-color has-background has-link-color wp-element-button" style="padding-top:0;padding-right:var(--wp--preset--spacing--20);padding-bottom:0;padding-left:var(--wp--preset--spacing--20)"><strong>SUMA-T'HI</strong></a></div>
<!-- /wp:button -->

<!-- wp:button {"backgroundColor":"brand","textColor":"typography","style":{"spacing":{"padding":{"left":"var:preset|spacing|20","right":"var:preset|spacing|20","top":"0","bottom":"0"}},"elements":{"link":{"color":{"text":"var:preset|color|typography"}}},"typography":{"fontStyle":"normal","fontWeight":"700"}},"fontSize":"x-small"} -->
<div class="wp-block-button has-custom-font-size has-x-small-font-size" style="font-style:normal;font-weight:700"><a class="wp-block-button__link has-typography-color has-brand-background-color has-text-color has-background has-link-color wp-element-button" style="padding-top:0;padding-right:var(--wp--preset--spacing--20);padding-bottom:0;padding-left:var(--wp--preset--spacing--20)"><strong>INVERTEIX</strong></a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
