<?php
/**
 * Title: Feed del blog
 * Slug: sm-pattern/blog-feed
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Feed del blog","categories":["sm-pattern"],"patternName":"sm-pattern/blog-feed"},"align":"full","className":"sm-blog-feed sm-garland-background","style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull sm-blog-feed sm-garland-background has-base-background-color has-background" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70)">
    <!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
    <h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--10)">Ùltimes activitats</h2>
    <!-- /wp:heading -->

    <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
    <div
        class="wp-block-group"
        style="margin-bottom: var(--wp--preset--spacing--50)"
    >
        <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"150px"}}} -->
        <hr class="wp-block-separator has-alpha-channel-opacity is-style-wide" />
        <!-- /wp:separator -->
    </div>
    <!-- /wp:group -->

    <!-- wp:group {"className":"is-style-show-tablet-desktop","align":"wide","layout":{"type":"constrained"}} -->
    <div class="wp-block-group alignwide is-style-show-tablet-desktop">
        <!-- wp:query {"queryId":14,"query":{"perPage":"3","pages":"1","offset":0,"postType":"post","order":"desc","orderBy":"date","author":"","search":"","exclude":[],"sticky":"","inherit":false},"align":"wide"} -->
        <div class="wp-block-query alignwide">
            <!-- wp:post-template {"style":{"spacing":{"blockGap":"var:preset|spacing|50"}},"layout":{"type":"grid","columnCount":3}} -->
                <!-- wp:pattern {"slug":"sm-pattern/blog-entry"} /-->
            <!-- /wp:post-template -->
        </div>
        <!-- /wp:query -->
    </div>
    <!-- /wp:group -->

    <!-- wp:group {"className":"is-style-show-mobile","layout":{"type":"constrained"}} -->
    <div class="wp-block-group is-style-show-mobile">
        <!-- wp:wpct-block/slider-loop {"adaptiveHeight":true,"arrows":false,"centerMode":true} -->
        <div class="wp-block-wpct-block-slider-loop wpct-block-slider wpct-block-slider-loop wp-block-group" data-adaptiveheight="true" data-arrows="false" data-autoplay="false" data-autoplayspeed="5" data-centermode="true" data-custompaging="false" data-dots="false" data-fade="false" data-infinite="true" data-initialslide="0" data-rtl="false" data-slidestoscroll="1" data-slidestoshow="1" data-speed="300" data-swipe="true" data-variablewidth="false">
            <!-- wp:query {"queryId":21,"query":{"postType":"post","order":"desc","orderBy":"date","author":"","perPage":3,"pages":0,"offset":0,"exclude":[],"sticky":"","inherit":false},"layout":{"type":"constrained"}} -->
            <div class="wp-block-query">
                <!-- wp:post-template {"layout":{"type":"constrained"}} -->
                    <!-- wp:group {"templateLock":false,"metadata":{"name":"Wpct Slide"},"className":"wpct-block-slider-slide wp-block-group","layout":{"type":"constrained"}} -->
                    <div class="wp-block-group wpct-block-slider-slide">
                    <!-- wp:pattern {"slug": "sm-pattern/blog-entry"} /-->
                    </div>
                    <!-- /wp:group -->
                <!-- /wp:post-template -->
            </div>
            <!-- /wp:query -->
        </div>
        <!-- /wp:wpct-block/slider-loop -->
    </div>
    <!-- /wp:group -->

    <!-- wp:buttons {"align":"wide"} -->
    <div class="wp-block-buttons alignwide">
        <!-- wp:button {"backgroundColor":"contrast","textColor":"base","className":"is-style-arrow","style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}}} -->
        <div class="wp-block-button is-style-arrow">
            <a class="wp-block-button__link has-base-color has-contrast-background-color has-text-color has-background has-link-color wp-element-button" href="https://sommobilitat.local/activitats/">totes les entrades</a>
        </div>
        <!-- /wp:button -->
    </div>
    <!-- /wp:buttons -->
</section>
<!-- /wp:group -->
