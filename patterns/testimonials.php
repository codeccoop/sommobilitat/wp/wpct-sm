<?php
/**
 * Title: Testimonis
 * Slug: sm-pattern/testimonials
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Testimonis","categories":["sm-pattern"],"patternName":"sm-pattern/testimonials"},"align":"full","className":"sm-testimonials","style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|60","left":"0","right":"0"}}},"backgroundColor":"swans-downs","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull sm-testimonials has-swans-downs-background-color has-background" style="padding-top:var(--wp--preset--spacing--70);padding-right:0;padding-bottom:var(--wp--preset--spacing--60);padding-left:0">
    <!-- wp:wpct-block/slider {"autoplay":true} -->
    <div class="wp-block-wpct-block-slider wpct-block-slider wp-block-group" data-adaptiveheight="false" data-arrows="true" data-autoplay="true" data-autoplayspeed="5" data-centermode="false" data-custompaging="false" data-dots="false" data-fade="false" data-infinite="true" data-initialslide="0" data-rtl="false" data-slidestoscroll="1" data-slidestoshow="1" data-speed="300" data-swipe="true" data-variablewidth="false">
        <!-- wp:group {"templateLock":false,"metadata":{"name":"Wpct Slide"},"className":"wpct-block-slider-slide wp-block-group","layout":{"type":"constrained"}} -->
        <div class="wp-block-group wpct-block-slider-slide">
            <!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
            <div class="wp-block-group">
                <!-- wp:image {"id":63650,"width":"123px","height":"auto","sizeSlug":"full","linkDestination":"none","className":"is-style-rounded","style":{"layout":{"selfStretch":"fixed","flexSize":"123px"}}} -->
                <figure class="wp-block-image size-full is-resized is-style-rounded">
                    <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/9e04a586ca9ab44f16cb9dc2704d7fee-edited.jpeg" alt="" class="wp-image-63650" style="width:123px;height:auto"/>
                </figure>
                <!-- /wp:image -->

                <!-- wp:group {"style":{"spacing":{"padding":{"right":"var:preset|spacing|40","left":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
                <div class="wp-block-group" style="padding-right:var(--wp--preset--spacing--40);padding-left:var(--wp--preset--spacing--40)">
                    <!-- wp:paragraph {"fontSize":"large"} -->
                    <p class="has-large-font-size">
                        <strong><em>“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.”</em></strong>
                    </p>
                    <!-- /wp:paragraph -->

                    <!-- wp:paragraph {"fontSize":"x-small"} -->
                    <p class="has-x-small-font-size">Nombre apellido, usuari del carsharing,</p>
                    <!-- /wp:paragraph -->
                </div>
                <!-- /wp:group -->
            </div>
            <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"templateLock":false,"metadata":{"name":"Wpct Slide"},"className":"wpct-block-slider-slide wp-block-group"} -->
        <div class="wp-block-group wpct-block-slider-slide">
            <!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
            <div class="wp-block-group">
                <!-- wp:image {"id":63650,"width":"123px","height":"auto","sizeSlug":"full","linkDestination":"none","className":"is-style-rounded","style":{"layout":{"selfStretch":"fixed","flexSize":"123px"}}} -->
                <figure class="wp-block-image size-full is-resized is-style-rounded">
                    <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/9e04a586ca9ab44f16cb9dc2704d7fee-edited.jpeg" alt="" class="wp-image-63650" style="width:123px;height:auto"/>
                </figure>
                <!-- /wp:image -->

                <!-- wp:group {"style":{"spacing":{"padding":{"right":"var:preset|spacing|40","left":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
                <div class="wp-block-group" style="padding-right:var(--wp--preset--spacing--40);padding-left:var(--wp--preset--spacing--40)">
                    <!-- wp:paragraph {"fontSize":"large"} -->
                    <p class="has-large-font-size">
                        <strong><em>“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.”</em></strong>
                    </p>
                    <!-- /wp:paragraph -->

                    <!-- wp:paragraph {"fontSize":"x-small"} -->
                    <p class="has-x-small-font-size">Nombre apellido, usuari del carsharing,</p>
                    <!-- /wp:paragraph -->
                </div>
                <!-- /wp:group -->
            </div>
            <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"templateLock":false,"metadata":{"name":"Wpct Slide"},"className":"wpct-block-slider-slide wp-block-group"} -->
        <div class="wp-block-group wpct-block-slider-slide">
            <!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
            <div class="wp-block-group">
                <!-- wp:image {"id":63650,"width":"123px","height":"auto","sizeSlug":"full","linkDestination":"none","className":"is-style-rounded","style":{"layout":{"selfStretch":"fixed","flexSize":"123px"}}} -->
                <figure class="wp-block-image size-full is-resized is-style-rounded">
                    <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/9e04a586ca9ab44f16cb9dc2704d7fee-edited.jpeg" alt="" class="wp-image-63650" style="width:123px;height:auto"/>
                </figure>
                <!-- /wp:image -->

                <!-- wp:group {"style":{"spacing":{"padding":{"right":"var:preset|spacing|40","left":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
                <div class="wp-block-group" style="padding-right:var(--wp--preset--spacing--40);padding-left:var(--wp--preset--spacing--40)">
                    <!-- wp:paragraph {"fontSize":"large"} -->
                    <p class="has-large-font-size">
                        <strong><em>“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.”</em></strong>
                    </p>
                    <!-- /wp:paragraph -->

                    <!-- wp:paragraph {"fontSize":"x-small"} -->
                    <p class="has-x-small-font-size">Nombre apellido, usuari del carsharing,</p>
                    <!-- /wp:paragraph -->
                </div>
                <!-- /wp:group -->
            </div>
            <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"templateLock":false,"metadata":{"name":"Wpct Slide"},"className":"wpct-block-slider-slide wp-block-group"} -->
        <div class="wp-block-group wpct-block-slider-slide">
            <!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
            <div class="wp-block-group">
                <!-- wp:image {"id":63650,"width":"123px","height":"auto","sizeSlug":"full","linkDestination":"none","className":"is-style-rounded","style":{"layout":{"selfStretch":"fixed","flexSize":"123px"}}} -->
                <figure class="wp-block-image size-full is-resized is-style-rounded">
                    <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/9e04a586ca9ab44f16cb9dc2704d7fee-edited.jpeg" alt="" class="wp-image-63650" style="width:123px;height:auto"/>
                </figure>
                <!-- /wp:image -->

                <!-- wp:group {"style":{"spacing":{"padding":{"right":"var:preset|spacing|40","left":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
                <div class="wp-block-group" style="padding-right:var(--wp--preset--spacing--40);padding-left:var(--wp--preset--spacing--40)">
                    <!-- wp:paragraph {"fontSize":"large"} -->
                    <p class="has-large-font-size">
                        <strong><em>“Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.”</em></strong>
                    </p>
                    <!-- /wp:paragraph -->

                    <!-- wp:paragraph {"fontSize":"x-small"} -->
                    <p class="has-x-small-font-size">Nombre apellido, usuari del carsharing,</p>
                    <!-- /wp:paragraph -->
                </div>
                <!-- /wp:group -->
            </div>
            <!-- /wp:group -->
        </div>
        <!-- /wp:group -->
    </div>
    <!-- /wp:wpct-block/slider -->
</section>
<!-- /wp:group -->
