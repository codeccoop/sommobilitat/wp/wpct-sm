<?php
/**
 * Title: Secció de text
 * Slug: sm-pattern/section-text
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Secció de text","categories":["sm-pattern"],"patternName":"sm-pattern/section-text"},"className":"sm-section-text md:w-11/12","style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60"}}},"layout":{"type":"constrained","justifyContent":"left"}} -->
<section class="wp-block-group sm-section-text md:w-11/12" style="padding-top:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60)"><!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
<h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--10)">Treballa amb nosaltres</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group" style="margin-bottom: var(--wp--preset--spacing--50)">
<!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"150px"}}} -->
<hr class="wp-block-separator has-alpha-channel-opacity is-style-wide" />
<!-- /wp:separator -->
</div><!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"margin":{"top":"var:preset|spacing|20","bottom":"var:preset|spacing|50"}}},"layout":{"type":"constrained","contentSize":"90%","justifyContent":"left"}} -->
<div class="wp-block-group" style="margin-top:var(--wp--preset--spacing--20);margin-bottom:var(--wp--preset--spacing--50)"><!-- wp:paragraph {"align":"left","lock":{"move":false,"remove":false}} -->
<p class="has-text-align-left">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:buttons {"lock":{"move":false,"remove":false}} -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"brand","textColor":"typography","className":"is-style-arrow","style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}}} -->
<div class="wp-block-button is-style-arrow"><a class="wp-block-button__link has-typography-color has-brand-background-color has-text-color has-background has-link-color wp-element-button" href="/"><strong>Ofertes de feina</strong></a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></section>
<!-- /wp:group -->
