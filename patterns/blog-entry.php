<?php
/**
 * Title: Entrada del blog
 * Slug: sm-pattern/blog-entry
 * Categories: sm-pattern
 * Viewport Width: 500
 */
?>

<!-- wp:group {"tagName":"article","metadata":{"name":"Entrada del blog"},"align":"wide","className":"sm-blog-entry relative","layout":{"type":"constrained"}} -->
<article class="wp-block-group alignwide sm-blog-entry relative">
    <!-- wp:post-featured-image {"isLink":true,"aspectRatio":"4/3","style":{"border":{"radius":"12px"}}} /-->

    <!-- wp:group {"tagName":"main","style":{"spacing":{"padding":{"right":"var:preset|spacing|40"}}},"className":"translate-y-[2rem] !mb-[2rem]","layout":{"type":"constrained"}} -->
    <main class="wp-block-group translate-y-[2rem] !mb-[2rem]" style="padding-right:var(--wp--preset--spacing--40)">
        <!-- wp:group {"style":{"spacing":{"padding":{"left":"var:preset|spacing|10"}},"border":{"radius":{"topRight":"12px"}}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
        <div
            class="wp-block-group has-base-background-color has-background"
            style="
                border-top-right-radius:12px;
                padding-left: var(--wp--preset--spacing--10);
            "
        >
            <!-- wp:group {"tagName":"header","style":{"spacing":{"padding":{"right":"var:preset|spacing|40"}},"border":{"radius":"0px"}},"layout":{"type":"constrained"}} -->
            <header class="wp-block-group" style="border-radius:0px;padding-right:var(--wp--preset--spacing--40)">
                <!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|20","bottom":"var:preset|spacing|20"},"blockGap":"0"}},"layout":{"type":"constrained"}} -->
                <div class="wp-block-group" style="padding-top:var(--wp--preset--spacing--20);padding-bottom:var(--wp--preset--spacing--20)">
                    <!-- wp:post-title {"isLink":true,"style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}},"spacing":{"margin":{"top":"0","bottom":"0"}}},"textColor":"typography","fontSize":"large"} /-->
                </div>
                <!-- /wp:group -->

                <!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"left"}} -->
                <div class="wp-block-group">
                    <!-- wp:post-date {"style":{"spacing":{"margin":{"right":"0","left":"0"},"padding":{"right":"0"}}}} /-->

                    <!-- wp:post-terms {"term":"category","style":{"spacing":{"margin":{"right":"var:preset|spacing|20","left":"var:preset|spacing|20"}}}} /-->
                </div>
                <!-- /wp:group -->
            </header>
            <!-- /wp:group -->

            <!-- wp:post-excerpt {"moreText":"Llegir més →"} /-->
        </div>
        <!-- /wp:group -->
    </main>
    <!-- /wp:group -->
</article>
<!-- /wp:group -->
