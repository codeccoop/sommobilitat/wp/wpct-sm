<?php
/**
 * Title: Parking Description
 * Slug: sm-pattern/parking-description
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","templateLock":false,"lock":{"move":false,"remove":false},"metadata":{"name":"Parking Description","categories":["sm-pattern"],"patternName":"sm-pattern/parking-description"},"align":"full","style":{"spacing":{"margin":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40"}}},"layout":{"type":"constrained"}} -->
<section
  class="wp-block-group alignfull"
  style="
    margin-top: var(--wp--preset--spacing--40);
    margin-bottom: var(--wp--preset--spacing--40);
  "
>
  <!-- wp:columns {"templateLock":"all","lock":{"move":true,"remove":true},"style":{"border":{"radius":"12px"},"spacing":{"padding":{"top":"0","bottom":"0","left":"0","right":"0"}}},"backgroundColor":"base"} -->
  <div
    class="wp-block-columns has-base-background-color has-background"
    style="
      border-radius: 12px;
      padding-top: 0;
      padding-right: 0;
      padding-bottom: 0;
      padding-left: 0;
    "
  >
    <!-- wp:column {"width":"30%","style":{"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40","left":"var:preset|spacing|50","right":"var:preset|spacing|30"}}}} -->
    <div
      class="wp-block-column"
      style="
        padding-top: var(--wp--preset--spacing--40);
        padding-right: var(--wp--preset--spacing--30);
        padding-bottom: var(--wp--preset--spacing--40);
        padding-left: var(--wp--preset--spacing--50);
        flex-basis: 30%;
      "
    >
      <!-- wp:heading {"level":3} -->
      <h3 class="wp-block-heading">Manuel Valls Engestur - Badalona</h3>
      <!-- /wp:heading -->

      <!-- wp:paragraph {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|30"}}}} -->
      <p style="margin-bottom: var(--wp--preset--spacing--30)">
        <strong>Adreça</strong>
      </p>
      <!-- /wp:paragraph -->

      <!-- wp:group {"templateLock":false,"lock":{"move":false,"remove":false},"style":{"spacing":{"blockGap":"var:preset|spacing|20","margin":{"top":"0","bottom":"var:preset|spacing|30"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
      <div
        class="wp-block-group"
        style="margin-top: 0; margin-bottom: var(--wp--preset--spacing--30)"
      >
        <!-- wp:paragraph -->
        <p>Carrer del Mestre Antoni Nicolau, cantonada Juli Garreta</p>
        <!-- /wp:paragraph -->

        <!-- wp:paragraph -->
        <p>08911 Barcelona</p>
        <!-- /wp:paragraph -->

        <!-- wp:paragraph -->
        <p>
          <a href="https://maps.google.com">Veure direcció a google maps</a>
        </p>
        <!-- /wp:paragraph -->
      </div>
      <!-- /wp:group -->

      <!-- wp:paragraph {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|30"}}}} -->
      <p style="margin-bottom: var(--wp--preset--spacing--30)">
        <strong>Horari</strong>
      </p>
      <!-- /wp:paragraph -->

      <!-- wp:group {"templateLock":false,"lock":{"move":false,"remove":false},"style":{"spacing":{"blockGap":"var:preset|spacing|20","margin":{"bottom":"var:preset|spacing|30"}}},"layout":{"type":"flex","orientation":"vertical"}} -->
      <div
        class="wp-block-group"
        style="margin-bottom: var(--wp--preset--spacing--30)"
      >
        <!-- wp:paragraph -->
        <p>obert 24 h</p>
        <!-- /wp:paragraph -->
      </div>
      <!-- /wp:group -->

      <!-- wp:paragraph {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|30"}}}} -->
      <p style="margin-bottom: var(--wp--preset--spacing--30)">
        <strong>Telèfon de l’aparcament</strong>
      </p>
      <!-- /wp:paragraph -->

      <!-- wp:group {"templateLock":false,"lock":{"move":false,"remove":false},"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|30"},"blockGap":"var:preset|spacing|20"}},"layout":{"type":"flex","orientation":"vertical"}} -->
      <div
        class="wp-block-group"
        style="margin-bottom: var(--wp--preset--spacing--30)"
      >
        <!-- wp:paragraph -->
        <p><a href="tel:+34600000000">93 384 46 79</a></p>
        <!-- /wp:paragraph -->
      </div>
      <!-- /wp:group -->
    </div>
    <!-- /wp:column -->

    <!-- wp:column {"verticalAlignment":"stretch","width":"70%","style":{"spacing":{"blockGap":"0"}},"layout":{"type":"default"}} -->
    <div
      class="wp-block-column is-vertically-aligned-stretch"
      style="flex-basis: 70%"
    >
      <!-- wp:wpct-block/leaflet-location {"className":"h-full"} -->
      <div
        class="wp-block-wpct-block-leaflet-location wpct-block-leaflet h-full"
        data-latitude="41.4"
        data-longitude="2.16"
        data-zoom="13"
      ></div>
      <!-- /wp:wpct-block/leaflet-location -->
    </div>
    <!-- /wp:column -->
  </div>
  <!-- /wp:columns -->
</section>
<!-- /wp:group -->
