<?php
/**
 * Title: Taula simple
 * Slug: sm-pattern/table-simple
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"metadata":{"categories":["sm-pattern"],"patternName":"sm-pattern/table-simple","name":"Taula simple"},"style":{"border":{"radius":"12px"},"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40","left":"0","right":"0"},"margin":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40"}}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
<div
  class="wp-block-group has-base-background-color has-background"
  style="
    border-radius: 12px;
    margin-top: var(--wp--preset--spacing--40);
    margin-bottom: var(--wp--preset--spacing--40);
    padding-top: var(--wp--preset--spacing--40);
    padding-right: 0;
    padding-bottom: var(--wp--preset--spacing--40);
    padding-left: 0;
  "
>
  <!-- wp:heading {"level":3,"style":{"spacing":{"padding":{"right":"var:preset|spacing|50","left":"var:preset|spacing|50"}}}} -->
  <h3
    class="wp-block-heading"
    style="
      padding-right: var(--wp--preset--spacing--50);
      padding-left: var(--wp--preset--spacing--50);
    "
  >
    List title
  </h3>
  <!-- /wp:heading -->

  <!-- wp:group {"style":{"spacing":{"padding":{"right":"var:preset|spacing|50","left":"var:preset|spacing|50","top":"var:preset|spacing|10","bottom":"0"}},"border":{"top":{"width":"1px"}}},"layout":{"type":"constrained"}} -->
  <div
    class="wp-block-group"
    style="
      border-top-width: 1px;
      padding-top: var(--wp--preset--spacing--10);
      padding-right: var(--wp--preset--spacing--50);
      padding-bottom: 0;
      padding-left: var(--wp--preset--spacing--50);
    "
  >
    <!-- wp:table {"className":"is-style-regular","fontSize":"medium"} -->
    <figure class="wp-block-table is-style-regular has-medium-font-size">
      <table class="has-fixed-layout">
        <thead>
          <tr>
            <th class="has-text-align-left" data-align="left">
              <strong>Indicador</strong>
            </th>
            <th class="has-text-align-left" data-align="left">
              <strong>Indicador</strong>
            </th>
            <th class="has-text-align-left" data-align="left">
              <strong>Indicador</strong>
            </th>
            <th class="has-text-align-left" data-align="left">
              <strong>Indicador</strong>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="has-text-align-left" data-align="left">
              <strong>Name</strong>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
            <td class="has-text-align-left" data-align="left">
              <a href="#">Link possible</a>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
          </tr>
          <tr>
            <td class="has-text-align-left" data-align="left">
              <strong>Name</strong>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
            <td class="has-text-align-left" data-align="left">
              <a href="#">Link possible</a>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
          </tr>
          <tr>
            <td class="has-text-align-left" data-align="left">
              <strong>Name</strong>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
            <td class="has-text-align-left" data-align="left">
              <a href="#">Link possible</a>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
          </tr>
          <tr>
            <td class="has-text-align-left" data-align="left">
              <strong>Name</strong>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
            <td class="has-text-align-left" data-align="left">
              <a href="#">Link possible</a>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
          </tr>
          <tr>
            <td class="has-text-align-left" data-align="left">
              <strong>Name</strong>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
            <td class="has-text-align-left" data-align="left">
              <a href="#">Link possible</a>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
          </tr>
          <tr>
            <td class="has-text-align-left" data-align="left">
              <strong>Name</strong>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
            <td class="has-text-align-left" data-align="left">
              <a href="#">Link possible</a>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
          </tr>
          <tr>
            <td class="has-text-align-left" data-align="left">
              <strong>Name</strong>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
            <td class="has-text-align-left" data-align="left">
              <a href="#">Link possible</a>
            </td>
            <td class="has-text-align-left" data-align="left">xxxx</td>
          </tr>
        </tbody>
      </table>
    </figure>
    <!-- /wp:table -->
  </div>
  <!-- /wp:group -->
</div>
<!-- /wp:group -->
