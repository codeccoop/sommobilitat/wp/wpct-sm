<?php
/**
 * Title: Secció + imatge (dreta)
 * Slug: sm-pattern/section-image-right
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Secció + imatge (dreta)","categories":["sm-pattern"],"patternName":"sm-pattern/section-image-right"},"className":"sm-section-col-image","style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60"}}},"layout":{"type":"constrained"}} -->
<section class="wp-block-group sm-section-col-image" style="padding-top:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60)"><!-- wp:columns {"templateLock":false,"lock":{"move":false,"remove":false},"className":"is-style-responsive-reverse is-style-reverse-pile","style":{"spacing":{"blockGap":{"left":"var:preset|spacing|40"}}}} -->
<div class="wp-block-columns is-style-responsive-reverse is-style-reverse-pile"><!-- wp:column {"width":"50%"} -->
<div class="wp-block-column" style="flex-basis:50%"><!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
<h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--10)">Serveis i tarifes</h2>
<!-- /wp:heading -->

  <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
  <div
    class="wp-block-group"
    style="margin-bottom: var(--wp--preset--spacing--50)"
  >
    <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"150px"}}} -->
    <hr class="wp-block-separator has-alpha-channel-opacity is-style-wide" />
    <!-- /wp:separator -->
  </div>
  <!-- /wp:group -->

<!-- wp:paragraph {"lock":{"move":false,"remove":false}} -->
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"lock":{"move":false,"remove":false}} -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"brand","textColor":"typography","className":"is-style-arrow","style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}}} -->
<div class="wp-block-button is-style-arrow"><a class="wp-block-button__link has-typography-color has-brand-background-color has-text-color has-background has-link-color wp-element-button" href="/"><strong>sobre nosaltres</strong></a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"50%"} -->
<div class="wp-block-column" style="flex-basis:50%"><!-- wp:image {"id":63524,"width":"auto","height":"300px","aspectRatio":"16/9","scale":"cover","sizeSlug":"large","linkDestination":"none","className":"is-style-default","style":{"border":{"radius":"13px"}}} -->
<figure class="wp-block-image size-large is-resized has-custom-border is-style-default"><img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/06/d39c41866aa6a6a5267eff3d2e800087-1024x576.jpeg" alt="" class="wp-image-63524" style="border-radius:13px;aspect-ratio:16/9;object-fit:cover;width:auto;height:300px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></section>
<!-- /wp:group -->
