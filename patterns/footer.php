<?php
/**
 * Title: SM Footer.
 * Slug: sm-pattern/footer
 * Categories: wpct-footer
 * Viewport Width: 1350
 */
?>
<!-- wp:group {"className":"sm-footer-bg","style":{"spacing":{"margin":{"top":"0","bottom":"0"}}},"backgroundColor":"la-rioja","textColor":"base","layout":{"type":"constrained"}} -->
<div
  id="contact"
  class="wp-block-group sm-footer-bg has-base-color has-la-rioja-background-color has-text-color has-background"
  style="margin-top: 0; margin-bottom: 0"
>
  <!-- wp:spacer {"height":"var:preset|spacing|70"} -->
  <div
    style="height: var(--wp--preset--spacing--70)"
    aria-hidden="true"
    class="wp-block-spacer"
  ></div>
  <!-- /wp:spacer -->

  <!-- wp:columns -->
  <div class="wp-block-columns">
    <!-- wp:column {"width":"130px","style":{"spacing":{"padding":{"right":"0"}}}} -->
    <div class="wp-block-column" style="padding-right: 0; flex-basis: 130px">
      <!-- wp:group {"style":{"spacing":{"padding":{"bottom":"0"},"margin":{"top":"var:preset|spacing|20","bottom":"var:preset|spacing|20"}}},"layout":{"type":"flex","orientation":"vertical","justifyContent":"center"}} -->
      <div
        class="wp-block-group"
        style="
          margin-top: var(--wp--preset--spacing--20);
          margin-bottom: var(--wp--preset--spacing--20);
          padding-bottom: 0;
        "
      >
        <!-- wp:site-logo /-->
      </div>
      <!-- /wp:group -->
    </div>
    <!-- /wp:column -->

    <!-- wp:column {"verticalAlignment":"center","width":"40%","style":{"spacing":{"padding":{"left":"var:preset|spacing|60"}}}} -->
    <div
      class="wp-block-column is-vertically-aligned-center"
      style="padding-left: var(--wp--preset--spacing--60); flex-basis: 40%"
    >
      <!-- wp:paragraph {"style":{"layout":{"selfStretch":"fixed","flexSize":"500px"},"typography":{"lineHeight":"1.1"}},"textColor":"contrast","fontSize":"x-large"} -->
      <p
        class="has-contrast-color has-text-color has-x-large-font-size"
        style="line-height: 1.1"
      >
        <strong>La cooperativa per una mobilitat +sostenible</strong>
      </p>
      <!-- /wp:paragraph -->
    </div>
    <!-- /wp:column -->

    <!-- wp:column {"verticalAlignment":"center","width":"50%"} -->
    <div
      class="wp-block-column is-vertically-aligned-center"
      style="flex-basis: 50%"
    >
      <!-- wp:social-links {"iconColor":"contrast","iconColorValue":"#1E303F","align":"right","className":"is-style-logos-only","style":{"spacing":{"margin":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40","left":"var:preset|spacing|20","right":"var:preset|spacing|20"},"blockGap":{"top":"0","left":"var:preset|spacing|20"}},"layout":{"selfStretch":"fill","flexSize":null}},"layout":{"type":"flex","justifyContent":"left"}} -->
      <ul
        class="wp-block-social-links alignright has-icon-color is-style-logos-only"
        style="
          margin-top: var(--wp--preset--spacing--40);
          margin-right: var(--wp--preset--spacing--20);
          margin-bottom: var(--wp--preset--spacing--40);
          margin-left: var(--wp--preset--spacing--20);
        "
      >
        <!-- wp:social-link {"url":"#","service":"facebook"} /-->

        <!-- wp:social-link {"url":"#","service":"instagram"} /-->

        <!-- wp:social-link {"url":"#","service":"youtube"} /-->

        <!-- wp:social-link {"url":"#","service":"x"} /-->
      </ul>
      <!-- /wp:social-links -->
    </div>
    <!-- /wp:column -->
  </div>
  <!-- /wp:columns -->

  <!-- wp:spacer {"height":"var:preset|spacing|60"} -->
  <div
    style="height: var(--wp--preset--spacing--60)"
    aria-hidden="true"
    class="wp-block-spacer"
  ></div>
  <!-- /wp:spacer -->

  <!-- wp:group {"style":{"spacing":{"padding":{"right":"var:preset|spacing|60","left":"var:preset|spacing|60","top":"var:preset|spacing|60","bottom":"var:preset|spacing|60"},"blockGap":"var:preset|spacing|60"},"border":{"radius":"13px"}},"backgroundColor":"wasabi","layout":{"type":"constrained"}} -->
  <div
    class="wp-block-group has-wasabi-background-color has-background"
    style="
      border-radius: 13px;
      padding-top: var(--wp--preset--spacing--60);
      padding-right: var(--wp--preset--spacing--60);
      padding-bottom: var(--wp--preset--spacing--60);
      padding-left: var(--wp--preset--spacing--60);
    "
  >
    <!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|40"}}}} -->
    <div class="wp-block-columns">
      <!-- wp:column {"width":"36%"} -->
      <div class="wp-block-column" style="flex-basis: 36%">
        <!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
        <div class="wp-block-group">
          <!-- wp:heading {"level":3,"textColor":"base"} -->
          <h3 class="wp-block-heading has-base-color has-text-color">
            Subscriu-te al butlletí
          </h3>
          <!-- /wp:heading -->

          <!-- wp:paragraph -->
          <p>Queda’t al dia sobre les nostres activitats, fires i xerrades!</p>
          <!-- /wp:paragraph -->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column {"width":"67%"} -->
      <div class="wp-block-column" style="flex-basis: 67%">
        <!-- wp:gravityforms/form {"formId":"21","title":false,"description":false,"ajax":true,"inputSize":"lg","inputBorderRadius":"6","inputBorderColor":"#849824","inputBackgroundColor":"#FFFFFF","inputColor":"#1E303F","inputPrimaryColor":"#FBDC07","labelColor":"#FFFFFF","buttonPrimaryBackgroundColor":"#1E303F"} /-->
      </div>
      <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
  </div>
  <!-- /wp:group -->

  <!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60"}}},"layout":{"type":"constrained"}} -->
  <div
    class="wp-block-group"
    style="
      padding-top: var(--wp--preset--spacing--60);
      padding-bottom: var(--wp--preset--spacing--60);
    "
  >
    <!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|20"}}}} -->
    <div class="wp-block-columns">
      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:heading {"level":5} -->
        <h5 class="wp-block-heading">Contacte</h5>
        <!-- /wp:heading -->

        <!-- wp:paragraph {"style":{"spacing":{"margin":{"top":"var:preset|spacing|40"}}},"textColor":"contrast","fontSize":"x-small"} -->
        <p
          class="has-contrast-color has-text-color has-x-small-font-size"
          style="margin-top: var(--wp--preset--spacing--40)"
        >
          De moment no disposem de punts d’atenció directa al públic: la nostra
          atenció preferent és via correu electrònic.
        </p>
        <!-- /wp:paragraph -->

        <!-- wp:buttons -->
        <div class="wp-block-buttons">
          <!-- wp:button {"className":"is-style-arrow"} -->
          <div class="wp-block-button is-style-arrow">
            <a class="wp-block-button__link wp-element-button" href="#"
              >Contacte</a
            >
          </div>
          <!-- /wp:button -->
        </div>
        <!-- /wp:buttons -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:heading {"level":5} -->
        <h5 class="wp-block-heading">Vehicles</h5>
        <!-- /wp:heading -->

        <!-- wp:list {"className":"is-style-no-disc","style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"},":hover":{"color":{"text":"var:preset|color|hover"}}}},"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"contrast"} -->
        <ul
          style="font-style: normal; font-weight: 500"
          class="wp-block-list is-style-no-disc has-contrast-color has-text-color has-link-color"
        >
          <!-- wp:list-item -->
          <li><a href="#">Com funciona?</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Flota</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Ubicacions</a></li>
          <!-- /wp:list-item -->
        </ul>
        <!-- /wp:list -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:heading {"level":5} -->
        <h5 class="wp-block-heading">Serveis i tarifes</h5>
        <!-- /wp:heading -->

        <!-- wp:list {"className":"is-style-no-disc","style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"},":hover":{"color":{"text":"var:preset|color|hover"}}}},"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"contrast"} -->
        <ul
          style="font-style: normal; font-weight: 500"
          class="wp-block-list is-style-no-disc has-contrast-color has-text-color has-link-color"
        >
          <!-- wp:list-item -->
          <li><a href="#">Ciutadans</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Empreses</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Ajuntaments</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Comunitats</a></li>
          <!-- /wp:list-item -->
        </ul>
        <!-- /wp:list -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:heading {"level":5} -->
        <h5 class="wp-block-heading">La cooperativa</h5>
        <!-- /wp:heading -->

        <!-- wp:list {"className":"is-style-no-disc","style":{"typography":{"fontStyle":"normal","fontWeight":"500"},"elements":{"link":{"color":{"text":"var:preset|color|contrast"},":hover":{"color":{"text":"var:preset|color|hover"}}}}},"textColor":"contrast"} -->
        <ul
          style="font-style: normal; font-weight: 500"
          class="wp-block-list is-style-no-disc has-contrast-color has-text-color has-link-color"
        >
          <!-- wp:list-item -->
          <li><a href="#">Qui som?</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Manifest</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Equip</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Blog</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Premsa</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Treballa amb nosaltres</a></li>
          <!-- /wp:list-item -->
        </ul>
        <!-- /wp:list -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column -->
      <div class="wp-block-column">
        <!-- wp:heading {"level":5} -->
        <h5 class="wp-block-heading">Fem Xarxa!</h5>
        <!-- /wp:heading -->

        <!-- wp:list {"className":"is-style-no-disc","style":{"typography":{"fontStyle":"normal","fontWeight":"500"},"elements":{"link":{"color":{"text":"var:preset|color|contrast"},":hover":{"color":{"text":"var:preset|color|hover"}}}}},"textColor":"contrast"} -->
        <ul
          style="font-style: normal; font-weight: 500"
          class="wp-block-list is-style-no-disc has-contrast-color has-text-color has-link-color"
        >
          <!-- wp:list-item -->
          <li><a href="#">Grups locals</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Cuidadors i referents</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Inverteix</a></li>
          <!-- /wp:list-item -->

          <!-- wp:list-item -->
          <li><a href="#">Projectes</a></li>
          <!-- /wp:list-item -->
        </ul>
        <!-- /wp:list -->
      </div>
      <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
  </div>
  <!-- /wp:group -->

  <!-- wp:spacer {"height":"var:preset|spacing|80"} -->
  <div
    style="height: var(--wp--preset--spacing--80)"
    aria-hidden="true"
    class="wp-block-spacer"
  ></div>
  <!-- /wp:spacer -->

  <!-- wp:group {"style":{"spacing":{"margin":{"top":"0","bottom":"0"},"padding":{"top":"0","bottom":"0"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
  <div
    class="wp-block-group"
    style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0"
  >
    <!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|contrast"}}}},"textColor":"contrast"} -->
    <p class="has-contrast-color has-text-color has-link-color">
      @ Som Mobilitat 2024
    </p>
    <!-- /wp:paragraph -->

    <!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"2em","selfStretch":"fixed"}}} -->
    <div
      style="height: 100px; width: 0px"
      aria-hidden="true"
      class="wp-block-spacer"
    ></div>
    <!-- /wp:spacer -->

    <!-- wp:navigation {"ref":63505,"overlayMenu":"never","style":{"spacing":{"blockGap":"var:preset|spacing|40"},"typography":{"fontStyle":"normal","fontWeight":"400"}},"fontSize":"small"} /-->
  </div>
  <!-- /wp:group -->
</div>
<!-- /wp:group -->
