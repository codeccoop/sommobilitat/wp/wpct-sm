<?php
/**
 * Title: Secció amb garnalda
 * Slug: sm-pattern/garland-section
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"metadata":{"name":"Secció amb garnalda"},"tagName":"section","align":"full","className":"sm-garland-background","style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"background":"base","layout":{"type":"constrained"}} -->
<section
    class="wp-block-group alignfull sm-garland-background has-base-background-color has-background"
    style="
        padding-top:var(--wp--preset--spacing--70);
        padding-bottom:var(--wp--preset--spacing--70);
    "
></section>
<!-- /wp:group -->
