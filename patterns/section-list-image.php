<?php
/**
 * Title: Llista + imatge
 * Slug: sm-pattern/section-list-image
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","metadata":{"name":"Llista + imatge"},"style":{"spacing":{"padding":{"top":"var:preset|spacing|60","bottom":"var:preset|spacing|60"}}},"className":"sm-section-col-image","layout":{"type":"constrained"}} -->
<section class="wp-block-group sm-section-col-image" style="padding-top:var(--wp--preset--spacing--60);padding-bottom:var(--wp--preset--spacing--60)">
    <!-- wp:heading {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|10"}}}} -->
    <h2 class="wp-block-heading" style="margin-bottom:var(--wp--preset--spacing--10)">Com funciona?</h2>
    <!-- /wp:heading -->

      <!-- wp:group {"style":{"spacing":{"margin":{"bottom":"var:preset|spacing|50"}}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
      <div
        class="wp-block-group"
        style="margin-bottom: var(--wp--preset--spacing--50)"
      >
        <!-- wp:separator {"className":"is-style-wide","style":{"layout":{"selfStretch":"fixed","flexSize":"150px"}}} -->
        <hr class="wp-block-separator has-alpha-channel-opacity is-style-wide" />
        <!-- /wp:separator -->
      </div>
      <!-- /wp:group -->

    <!-- wp:columns {"templateLock":false,"lock":{"move":false,"remove":false},"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|40"}}},"className":"is-style-reverse-pile"} -->
    <div class="wp-block-columns is-style-reverse-pile">
        <!-- wp:column {"lock":{"move":false,"remove":false}} -->
        <div class="wp-block-column">
            <!-- wp:paragraph {"lock":{"move":false,"remove":false}} -->
            <p>Compartim vehicles elèctrics a través d'una app. Reserva coches y accedeix a tots els vehicles de Som Mobilitat.</p>
            <!-- /wp:paragraph -->

            <!-- wp:list {"className":"is-style-numeric"} -->
            <ul class="is-style-numeric">
                <!-- wp:list-item {"fontSize":"medium"} -->
                <li class="has-medium-font-size"><strong>Fes-te'n soci:a</strong></li>
                <!-- /wp:list-item -->

                <!-- wp:list-item {"fontSize":"medium"} -->
                <li class="has-medium-font-size"><strong>Descarrega't l'aplicació</strong></li>
                <!-- /wp:list-item -->

                <!-- wp:list-item {"fontSize":"medium"} -->
                <li class="has-medium-font-size"><strong>Reserva el teu cotxe</strong></li>
                <!-- /wp:list-item -->

                <!-- wp:list-item {"fontSize":"medium"} -->
                <li class="has-medium-font-size"><strong>Som-hi!</strong></li>
                <!-- /wp:list-item -->
            </ul>
            <!-- /wp:list -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:image {"id":63572,"width":"auto","height":"300px","sizeSlug":"full","linkDestination":"none","align":"right","className":"is-style-border-radius","style":{"color":[]}} -->
            <figure class="wp-block-image alignright size-full is-resized is-style-border-radius">
                <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/06/Frame-36-1.png" alt="" class="wp-image-63572" style="width:auto;height:300px"/>
            </figure>
            <!-- /wp:image -->
        </div>
        <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
</section>
<!-- /wp:group -->
