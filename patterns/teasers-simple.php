<?php
/**
 * Title: Teasers simples
 * Slug: sm-pattern/teasers-simple
 * Categories: sm-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"metadata":{"name":"Teasers simples","categories":["sm-pattern"],"patternName":"sm-pattern/teasers"},"style":{"spacing":{"padding":{"top":"var:preset|spacing|70","bottom":"var:preset|spacing|70"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:var(--wp--preset--spacing--70);padding-bottom:var(--wp--preset--spacing--70)">
    <!-- wp:columns {"style":{"spacing":{"blockGap":{"left":"var:preset|spacing|40"}}}} -->
    <div class="wp-block-columns">
        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:group {"metadata":{"name":"Teaser simple"},"layout":{"type":"constrained"}} -->
            <div class="wp-block-group">
                <!-- wp:image {"id":63669,"aspectRatio":"1","scale":"cover","sizeSlug":"full","linkDestination":"none","lock":{"remove":false}} -->
                <figure class="wp-block-image size-full">
                    <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/82a35347d0f470c9c3de6992678c4da1.jpeg" alt="" class="wp-image-63669" style="aspect-ratio:1;object-fit:cover"/>
                </figure>
                <!-- /wp:image -->

                <!-- wp:heading {"level":4} -->
                <h4 class="wp-block-heading">Lorem ipsum dolor sit amer</h4>
                <!-- /wp:heading -->

                <!-- wp:paragraph {"className":"is-style-arrow"} -->
                <p class="is-style-arrow"><a href="/">Veure més</a></p>
                <!-- /wp:paragraph -->
            </div>
            <!-- /wp:group -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:group {"metadata":{"name":"Teaser simple"},"layout":{"type":"constrained"}} -->
            <div class="wp-block-group">
                <!-- wp:image {"id":63670,"aspectRatio":"1","scale":"cover","sizeSlug":"full","linkDestination":"none","lock":{"remove":false}} -->
                <figure class="wp-block-image size-full">
                    <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/31466da02360eecbc77606b8d42da6ab.jpeg" alt="" class="wp-image-63670" style="aspect-ratio:1;object-fit:cover"/>
                </figure>
                <!-- /wp:image -->

                <!-- wp:heading {"level":4} -->
                <h4 class="wp-block-heading">Lorem ipsum dolor sit amer</h4>
                <!-- /wp:heading -->

                <!-- wp:paragraph {"className":"is-style-arrow"} -->
                <p class="is-style-arrow"><a href="/">Veure més</a></p>
                <!-- /wp:paragraph -->
            </div>
            <!-- /wp:group -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:group {"metadata":{"name":"Teaser simple"},"layout":{"type":"constrained"}} -->
            <div class="wp-block-group">
                <!-- wp:image {"id":63671,"aspectRatio":"1","scale":"cover","sizeSlug":"full","linkDestination":"none","lock":{"remove":false}} -->
                <figure class="wp-block-image size-full">
                    <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/ce7b7579debab3acbc8e46eafc136c5b.jpeg" alt="" class="wp-image-63671" style="aspect-ratio:1;object-fit:cover"/>
                </figure>
                <!-- /wp:image -->

                <!-- wp:heading {"level":4} -->
                <h4 class="wp-block-heading">Lorem ipsum dolor sit amer</h4>
                <!-- /wp:heading -->

                <!-- wp:paragraph {"className":"is-style-arrow"} -->
                <p class="is-style-arrow"><a href="/">Veure més</a></p>
                <!-- /wp:paragraph -->
            </div>
            <!-- /wp:group -->
        </div>
        <!-- /wp:column -->

        <!-- wp:column -->
        <div class="wp-block-column">
            <!-- wp:group {"metadata":{"name":"Teaser simple"},"layout":{"type":"constrained"}} -->
            <div class="wp-block-group">
                <!-- wp:image {"id":63672,"aspectRatio":"1","scale":"cover","sizeSlug":"full","linkDestination":"none","lock":{"remove":false}} -->
                <figure class="wp-block-image size-full">
                    <img src="https://sommobilitat.codeccoop.org/wp-content/uploads/2024/07/f0ad260a799e6de0baafe061a582f995.jpeg" alt="" class="wp-image-63672" style="aspect-ratio:1;object-fit:cover"/>
                </figure>
                <!-- /wp:image -->

                <!-- wp:heading {"level":4} -->
                <h4 class="wp-block-heading">Lorem ipsum dolor sit amer</h4>
                <!-- /wp:heading -->

                <!-- wp:paragraph {"className":"is-style-arrow"} -->
                <p class="is-style-arrow"><a href="/">Veure més</a></p>
                <!-- /wp:paragraph -->
            </div>
            <!-- /wp:group -->
        </div>
        <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
</div>
<!-- /wp:group -->
