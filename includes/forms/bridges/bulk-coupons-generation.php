<?php

add_filter(
    'forms_bridge_skip_submission',
    function ($skip, $bridge, $payload) {
        if ($bridge->name === 'coupons') {
            $quantity = (int) $payload['quantity'];
            $generated = 0;
            while ($generated < $quantity) {
                sm_generate_coupon($payload, 9);
                $generated++;
            }
            return true;
        }

        return $skip;
    },
    90,
    3
);
