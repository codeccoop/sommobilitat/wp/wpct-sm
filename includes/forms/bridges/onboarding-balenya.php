<?php

add_filter(
    'forms_bridge_attachments',
    function ($attachments, $bridge) {
        if ($bridge->name === 'onboarding-balenya') {
            return [];
        }

        return $attachments;
    },
    10,
    2
);

add_filter(
    'forms_bridge_payload',
    function ($payload, $bridge) {
        if ($bridge->name !== 'onboarding-balenya') {
            return $payload;
        }

        $payload['name'] =
            $payload['source_xml_id'] .
            ' submission: ' .
            $payload['submission_id'];
        $payload['metadata'] = [];
        foreach ($payload as $key => $val) {
            if (
                in_array($key, [
                    'metadata',
                    'name',
                    'email_from',
                    'source_xml_id',
                ])
            ) {
                continue;
            }

            $payload['metadata'][] = [
                'key' => $key,
                'value' => (string) $val,
            ];
            unset($payload[$key]);
        }

        $uploads = sm_form_uploads_urls();
        foreach ($uploads as $upload => $url) {
            $payload['metadata'][] = [
                'key' => $upload,
                'value' => $url,
            ];
        }

        return $payload;
    },
    10,
    2
);

add_action(
    'forms_bridge_after_submission',
    function ($bridge, $payload) {
        if ($bridge->name !== 'onboarding-balenya') {
            return;
        }

        $newsletter_subscription = array_filter($payload['metadata'], function (
            $meta
        ) {
            return $meta['key'] === 'newsletter';
        });
        $newsletter_subscription = array_pop($newsletter_subscription);

        if ($newsletter_subscription['value']) {
            $email = $payload['email_from'];
            $name = array_reduce(
                $payload['metadata'],
                function ($carry, $meta) {
                    if (
                        $meta['key'] === 'contact_firstname' ||
                        $meta['key'] === 'contact_lastname'
                    ) {
                        $carry[$meta['key']] = $meta['value'];
                    }

                    return $carry;
                },
                []
            );
            $name =
                $name['contact_firstname'] . ' ' . $name['contact_lastname'];

            $zip = array_filter($payload['metadata'], function ($meta) {
                return $meta['key'] === 'zip';
            });
            $zip = array_pop($zip)['value'];

            sm_sync_mail_chimp([
                'name' => $name,
                'email' => $email,
                'zip' => $zip,
            ]);
        }
    },
    10,
    2
);
