<?php

add_filter(
    'forms_bridge_skip_submission',
    function ($skip, $bridge, $payload) {
        if ($bridge->name === 'newsletter') {
            sm_sync_mail_chimp([
                'name' => $payload['name'],
                'email' => $payload['email'],
            ]);
            return true;
        }

        return $skip;
    },
    90,
    3
);
