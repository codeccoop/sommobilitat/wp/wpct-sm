<?php

add_filter(
    'forms_bridge_payload',
    function ($payload, $bridge) {
        if ($bridge->name === 'promo-codes') {
            $uploads = sm_form_uploads_urls();
            foreach ($uploads as $upload => $url) {
                $payload[$upload] = $url;
            }

            $coupon = sm_get_coupon_by_name($payload['promo_code']);
            $data = sm_parse_reward($payload, $coupon);

            if (
                $data['data_partner_creation_type'] === 'new' ||
                $data['data_partner_creation_type'] === 'subscription'
            ) {
                $partner_data = sm_parse_reward_partner_data($payload);
                $data = array_merge($data, $partner_data);
            }

            $payload = $data;
        }

        return $payload;
    },
    10,
    2
);

add_action(
    'forms_bridge_after_submission',
    function ($bridge, $payload) {
        if ($bridge->name === 'promo-codes') {
            $coupon = sm_get_coupon_by_name($payload['promo_code']);

            if (!$coupon->coupon_allow_promo) {
                update_post_meta($coupon->ID, 'coupon_used', true);
            }

            if ($payload['data_partner_creation_type'] !== 'existing') {
                $submission = apply_filters('forms_bridge_submission', []);
                $uploads = sm_form_uploads_urls();
                foreach ($uploads as $upload => $url) {
                    $submission[$upload] = $url;
                }

                $member_data = sm_get_member_data($submission);
                $member_data['must_register_in_cs'] = true;
                $backend = apply_filters('http_bridge_backend', null, 'Odoo');
                $res = $backend->post(
                    '/api/subscription-request',
                    $member_data
                );

                if (!is_wp_error($res)) {
                    sm_register_member($member_data, $coupon);
                }
            }
        }
    },
    10,
    2
);

add_filter(
    'forms_bridge_attachments',
    function ($attachments, $bridge) {
        if ($bridge->name === 'promo-codes') {
            return [];
        }

        return $attachments;
    },
    10,
    2
);
