<?php

add_filter(
    'forms_bridge_payload',
    function ($payload, $bridge) {
        if ($bridge->name !== 'rewards') {
            return $payload;
        }

        if (isset($payload['CarID'])) {
            $payload['cs_feedback_car_id'] = $payload['CarID'];
            unset($payload['CarID']);
        }

        if (isset($payload['Home'])) {
            $payload['cs_feedback_home'] = $payload['Home'];
            unset($payload['Home']);
        }

        if (isset($payload['MemberID'])) {
            $payload['cs_feedback_member_id'] = $payload['MemberID'];
            unset($payload['MemberID']);
        }

        if (isset($payload['ReservationStart'])) {
            $payload['cs_feedback_reservation_start'] =
                $payload['ReservationStart'];
            unset($payload['ReservationStart']);
        }

        $external_obj_id = (int) $payload['submission_id'];
        unset($payload['submission_id']);

        $reward_name = 'Maintenance: ' . $external_obj_id;
        $payload = array_merge($payload, [
            'reward_type' => 'fleet_maintenance',
            'data_partner_creation_type' => 'existing',
            'external_obj_id' => $external_obj_id,
            'name' => $reward_name,
            'reward_info' => $reward_name,
        ]);

        $payload['data_partner_vat'] = sm_sanitize_vat(
            $payload['data_partner_vat']
        );
        $payload['maintenance_car_plate'] = sm_sanitize_vat(
            $payload['maintenance_car_plate']
        );
        $payload['reward_date'] = date(
            'Y-m-d',
            strtotime(sm_format_postdata_date($payload['reward_data']))
        );

        switch ($payload['maintenance_reservation_type']) {
            case 'maintenance':
                $payload['maintenance_forgive_reservation'] = true;
                break;
            case 'reservation_and_maintenance':
                $payload['maintenance_discount_reservation'] = true;
                break;
        }

        switch ($payload['maintenance_type']) {
            case 'clean_inside':
                $payload['maintenance_create_car_service'] = true;
                $payload['maintenance_car_service'] = 'Neteja - Interior';
                $payload['related_analytic_account'] = 'Recompenses Neteja';
                break;
            case 'clean_outside':
                $payload['maintenance_create_car_service'] = true;
                $payload['maintenance_car_service'] = 'Neteja - Exterior';
                $payload['related_analytic_account'] = 'Recompenses Neteja';
                break;
            case 'clean_inside_outside':
                $payload['maintenance_create_car_service'] = true;
                $payload['maintenance_car_service'] =
                    'Neteja - Interior / Exterior';
                $payload['related_analytic_account'] = 'Recompenses Neteja';
                break;
            case 'car_to_mechanic':
                $payload['maintenance_create_car_service'] = false;
                $payload['related_analytic_account'] =
                    'Recompenses logística cotxes';
                break;
            case 'swap_car':
                $payload['maintenance_create_car_service'] = false;
                $payload['related_analytic_account'] =
                    'Recompenses logística cotxes';
                break;
            case 'wheel_pressure':
                $payload['maintenance_create_car_service'] = true;
                $payload['maintenance_car_service'] =
                    'Manteniment - Ajustar pressió rodes';
                $payload['related_analytic_account'] =
                    'Recompenses manteniment';
                break;
            case 'charge_car':
                $payload['maintenance_create_car_service'] = false;
                $payload['related_analytic_account'] =
                    'Recompenses logística cotxes';
                break;
            case 'member_tutorial':
                $payload['maintenance_create_car_service'] = false;
                $payload['related_analytic_account'] =
                    'Feines tècniques i consultoria';
                break;
            case 'representation_act':
                $payload['maintenance_create_car_service'] = false;
                $payload['related_analytic_account'] =
                    'Feines tècniques i consultoria';
                break;
            case 'replace_hydroalcoholic_gel':
                $payload['maintenance_create_car_service'] = true;
                $payload['maintenance_car_service'] =
                    'Manteniment - Reposar gel hidroalcohòlic';
                $payload['related_analytic_account'] =
                    'Recompenses manteniment';
                break;
            case 'move_car_to_parking':
                $payload['maintenance_create_car_service'] = true;
                $payload['maintenance_car_service'] =
                    "Logística - Moure cotxe a l'aparcament";
                $payload['related_analytic_account'] =
                    'Recompenses logística cotxes';
                break;
            case 'other':
                $payload['maintenance_create_car_service'] = false;
                break;
        }

        if ($payload['maintenance_duration'] !== 'other') {
            $payload['reward_addtime'] = (int) $payload['maintenance_duration'];
            $payload['reward_addmoney'] =
                ((float) $payload['maintenance_duration']) * 0.25;
        }

        if (isset($payload['cs_feedback_car_id'])) {
            $payload['maintenance_carconfig_index'] =
                $payload['cs_feedback_car_id'];
        }
        if (isset($payload['cs_feedback_home'])) {
            $payload['maintenance_carconfig_home'] =
                $payload['cs_feedback_home'];
        }
        if (isset($payload['cs_feedback_member_id'])) {
            $payload['maintenance_cs_person_index'] =
                $payload['cs_feedback_member_id'];
        }
        if (isset($payload['cs_feedback_reservation_start'])) {
            $rs_date = date_create($payload['cs_feedback_reservation_start']);
            $payload['maintenance_reservation_start'] = date_format(
                $rs_date,
                'Y-m-d H:i:s'
            );
        }

        return $payload;
    },
    10,
    2
);

add_filter(
    'forms_bridge_attachments',
    function ($attachments, $bridge) {
        if ($bridge->name === 'rewards') {
            return [];
        }

        return $attachments;
    },
    10,
    2
);
