<?php

add_filter(
    'forms_bridge_payload',
    function ($payload, $bridge) {
        if ($bridge->name !== 'subscription-request') {
            return $payload;
        }

        $uploads = sm_form_uploads_urls();
        foreach ($uploads as $upload => $url) {
            $payload[$upload] = $url;
        }

        return sm_get_member_data($payload);
    },
    10,
    2
);

add_action(
    'forms_bridge_after_submission',
    function ($bridge, $payload) {
        if ($bridge->name === 'subscription-request') {
            sm_register_member($payload);
        }
    },
    10,
    2
);

add_filter(
    'forms_bridge_attachments',
    function ($attachments, $bridge) {
        if ($bridge->name === 'subscription-request') {
            return [];
        }

        return $attachments;
    },
    10,
    2
);
