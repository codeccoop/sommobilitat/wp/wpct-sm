<?php

add_filter(
    'gform_field_validation',
    function ($result, $value, $form, $field) {
        if (
            !sm_is_form_bridged('subscription-request', $form['id']) ||
            $field->type !== 'address'
        ) {
            return $result;
        }

        $is_valid = $result['is_valid'];
        foreach ($field->inputs as $input) {
            switch ($input['name']) {
                case 'zip_code':
                    $is_valid =
                        $is_valid &&
                        sm_check_valid_postal_code($value[$input['id']]);
                    break;
                case 'state':
                    $is_valid =
                        $is_valid &&
                        sm_check_valid_address_state($value[$input['id']]);
                    break;
            }
        }

        $result['is_valid'] = $is_valid;
        if (!$result['is_valid']) {
            $result['message'] = __('L\'adreça no és vàlida', 'wpct-sm');
        }

        return $result;
    },
    10,
    4
);

add_filter(
    'gform_field_validation',
    function ($result, $value, $form, $field) {
        if (
            !sm_is_form_bridged('subscription-request', $form['id']) ||
            $field->type !== 'date'
        ) {
            return $result;
        }

        if ($field->inputName === 'birthdate') {
            $date = sm_format_member_date($value);
            $result['is_valid'] =
                $result['is_valid'] && sm_check_is_adult($date);
            if (!$result['is_valid']) {
                $result['message'] = __(
                    'Per fer-te soci de Som Mobilitat has de ser major d\'edat',
                    'wpct-sm'
                );
            }
        }

        return $result;
    },
    10,
    4
);

add_action(
    'gform_enqueue_scripts',
    function ($form) {
        if (sm_is_form_bridged('subscription-request', $form['id'])) {
            add_action('wp_footer', function () use ($form) {
                sm_state_input_autofill($form);
            });
        }
    },
    10
);
