<?php

add_filter(
    'gform_field_validation',
    function ($result, $value, $form, $field) {
        if (
            !sm_is_form_bridged('onboarding-balenya', $form['id']) ||
            $field->type !== 'address'
        ) {
            return $result;
        }

        $is_valid = $result['is_valid'];
        foreach ($field->inputs as $input) {
            switch ($input['name']) {
                case 'zip':
                    $is_valid =
                        $is_valid &&
                        sm_check_valid_postal_code($value[$input['id']]);
                    break;
            }
        }

        $result['is_valid'] = $is_valid;
        if (!$result['is_valid']) {
            $result['message'] = __('L\'adreça no és vàlida', 'wpct-sm');
        }

        return $result;
    },
    10,
    4
);

add_filter(
    'gform_field_validation',
    function ($result, $value, $form, $field) {
        if (
            !sm_is_form_bridged('onboarding-balenya', $form['id']) ||
            $field->type !== 'date'
        ) {
            return $result;
        }

        $is_valid = $result['is_valid'];
        if ($field->inputName === 'birthdate') {
            $date = sm_format_member_date($value);
            $is_valid = $is_valid && sm_check_is_adult($date);
            $error_msg = __(
                'Per registrar-te al servei de carsharing has de ser major de 22 anys.',
                'wpct-sm'
            );
            update_option('_sm_coupon_registration_bdate', $date);
        } elseif ($field->inputName === 'driving_license_date') {
            $date = sm_format_member_date($value);
            $bdate = get_option('_sm_coupon_registration_bdate');
            $is_valid = $is_valid && sm_check_valid_expiration_date($date);
            $is_valid =
                $is_valid &&
                sm_check_not_enough_experience_for_register($date, $bdate);
            delete_option('_sm_coupon_registration_bdate');
            $error_msg = __(
                'Per registrar-te al servei de carsharing has de tenir un carnet de conduir amb data vàlida i en cas de tenir menys de 25 anys, ha de tenir un mínim de 2 anys d\'antiguitat.',
                'wpct-sm'
            );
        }

        $result['is_valid'] = $is_valid;
        if (!$result['is_valid']) {
            $result['message'] = $error_msg;
        }

        return $result;
    },
    10,
    4
);

add_filter(
    'gform_date_max_year',
    function ($max_date, $form, $field) {
        if (
            sm_is_form_bridged('onboarding-balenya', $form['id']) &&
            $field->inputName === 'driving_license_date'
        ) {
            return date('Y') + 15;
        }
        return $max_date;
    },
    90,
    3
);

add_filter(
    'gform_date_min_year',
    function ($min_date, $form, $field) {
        if (
            sm_is_form_bridged('onboarding-balenya', $form['id']) &&
            $field->inputName === 'driving_license_date'
        ) {
            return date('Y');
        }
        return $min_date;
    },
    90,
    3
);
