<?php

add_filter(
    'gform_field_validation',
    function ($result, $value, $form, $field) {
        if (
            !sm_is_form_bridged('promo-codes', $form['id']) ||
            $field->type !== 'address'
        ) {
            return $result;
        }

        $is_valid = $result['is_valid'];
        foreach ($field->inputs as $input) {
            switch ($input['name']) {
                case 'zip_code':
                    $is_valid =
                        $is_valid &&
                        sm_check_valid_postal_code($value[$input['id']]);
                    break;
                case 'state':
                    $is_valid =
                        $is_valid &&
                        sm_check_valid_address_state($value[$input['id']]);
                    break;
            }
        }

        $result['is_valid'] = $is_valid;
        if (!$result['is_valid']) {
            $result['message'] = __('L\'adreça no és vàlida', 'wpct-sm');
        }

        return $result;
    },
    10,
    4
);

add_filter(
    'gform_field_validation',
    function ($result, $value, $form, $field) {
        if (
            !sm_is_form_bridged('promo-codes', $form['id']) ||
            $field->type !== 'date'
        ) {
            return $result;
        }

        $is_valid = $result['is_valid'];
        if ($field->inputName === 'birthdate') {
            $date = sm_format_member_date($value);
            $is_valid = $is_valid && sm_check_is_adult($date);
            $error_msg = __(
                'Per registrar-te al servei de carsharing has de ser major de 22 anys.',
                'wpct-sm'
            );
            update_option('_sm_coupon_registration_bdate', $date);
        } elseif ($field->inputName === 'driving_license_expiration_date') {
            $date = sm_format_member_date($value);
            $bdate = get_option('_sm_coupon_registration_bdate');
            $is_valid = $is_valid && sm_check_valid_expiration_date($date);
            $is_valid =
                $is_valid &&
                sm_check_not_enough_experience_for_register($date, $bdate);
            delete_option('_sm_coupon_registration_bdate');
            $error_msg = __(
                'Per registrar-te al servei de carsharing has de tenir un carnet de conduir amb data vàlida i en cas de tenir menys de 25 anys, ha de tenir un mínim de 2 anys d\'antiguitat.',
                'wpct-sm'
            );
        }

        $result['is_valid'] = $is_valid;
        if (!$result['is_valid']) {
            $result['message'] = $error_msg;
        }

        return $result;
    },
    10,
    4
);

add_filter('gform_validation', function ($validation) {
    $form = $validation['form'];
    if (!sm_is_form_bridged('promo-codes', $form['id'])) {
        return $validation;
    }

    $field = array_filter($form['fields'], function ($field) {
        return $field->inputName === 'promo_code';
    });
    $field = array_pop($field);
    $promo_code = $_POST['input_' . $field->id];
    $coupon = sm_get_coupon_by_name($promo_code);

    try {
        if (empty($coupon)) {
            throw new Exception(
                __(
                    'El codi no coincideix amb els del nostre registre. Et recordem que per fer-te soci sense un codi promocional pots visitar el següent <a href="https://www.sommobilitat.coop/formulari-fes-te-soci/">enllaç</a>. Per qualsevol dubte o incidència possat en contacte amb nosaltres a <a href="mailto:info@sommobilitat.coop">info@sommobilitat.coop</a>',
                    'wpct-sm'
                )
            );
        } else {
            if (get_post_meta($coupon->ID, 'coupon_used', true)) {
                throw new Exception(
                    __(
                        'El codi ja ha estat fet servir. Per qualsevol dubte o incidència possat en contacte amb nosaltres a <a href="mailto:info@sommobilitat.coop">info@sommobilitat.coop</a>',
                        'wpct-sm'
                    )
                );
            } elseif (
                get_post_meta(
                    $coupon->ID,
                    'coupon_related_company_cif',
                    true
                ) &&
                !get_post_meta($coupon->ID, 'coupon_allow_promo', true)
            ) {
                throw new Exception(
                    __(
                        'El cupó fet servir correspon a un cupó de registre d\'organització. El pots fer servir al <a href="https://www.sommobilitat.coop/registre/">formulari de registre</a>',
                        'wpct-sm'
                    )
                );
            }
        }
    } catch (Exception $e) {
        $validation['is_valid'] = false;
        $field = array_values(
            array_filter($form['fields'], function ($field) {
                return $field->inputName === 'promo_code';
            })
        )[0];

        $field->failed_validation = true;
        $field->validation_message = $e->getMessage();
    } catch (Error $e) {
        $validation['is_valid'] = false;
    }

    $validation['form'] = $form;
    return $validation;
});

add_filter(
    'gform_date_max_year',
    function ($max_date, $form, $field) {
        if (
            sm_is_form_bridged('promo-codes', $form['id']) &&
            $field->inputName === 'driving_license_expiration_date'
        ) {
            return date('Y') + 15;
        }
        return $max_date;
    },
    90,
    3
);

add_filter(
    'gform_date_min_year',
    function ($min_date, $form, $field) {
        if (
            sm_is_form_bridged('promo-codes', $form['id']) &&
            $field->inputName === 'driving_license_expiration_date'
        ) {
            return date('Y');
        }
        return $min_date;
    },
    90,
    3
);

add_action(
    'gform_enqueue_scripts',
    function ($form) {
        if (sm_is_form_bridged('promo-codes', $form['id'])) {
            add_action('wp_footer', function () use ($form) {
                sm_state_input_autofill($form);
            });
        }
    },
    10
);
