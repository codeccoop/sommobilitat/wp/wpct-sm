<?php

if (function_exists('acf_add_local_field_group')) {
    acf_add_local_field_group(array(
      'key' => 'group_5a1d8290a46e1',
      'title' => __('Cupó', 'wpct-sm'),
      'fields' => array(
        array(
          'key' => 'field_5a1d842185cdb',
          'label' => 'Completed',
          'name' => 'coupon_used',
          'type' => 'true_false',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'message' => '',
          'default_value' => 0,
          'ui' => 0,
          'ui_on_text' => '',
          'ui_off_text' => '',
        ),
        array(
          'key' => 'field_5a1d842185123',
          'label' => 'Allow to be used as promo',
          'name' => 'coupon_allow_promo',
          'type' => 'true_false',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'message' => '',
          'default_value' => 0,
          'ui' => 0,
          'ui_on_text' => '',
          'ui_off_text' => '',
        ),
        array(
          'key' => 'field_5a1d829904529',
          'label' => 'Membership',
          'name' => 'coupon_membership',
          'type' => 'select',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'choices' => array(
            'create_coop_member_completed_cs_user' =>
            'Create cs user + Coop member completed',
            'create_coop_member_completed_cs_user_dedicated_ba' =>
            'Create cs user + Coop member completed + dedicated billingAccount',
            'create_coop_member_completed_cs_user_force_register' =>
            'Create cs user (forced) + Coop member completed',
            'create_coop_member_completed_cs_user_force_register_dedicated_ba' =>
            'Create cs user (forced) + Coop member completed + dedicated billingAccount',
            'create_cs_user_not_member' =>
            'Create cs user but not coop member',
            'create_cs_user_not_member_dedicated_ba' =>
            'Create cs user but not coop member + dedicated billingAccount',
            'create_cs_user_not_member_force_register' =>
            'Create cs user (forced) but not coop member',
            'create_cs_user_not_member_force_register_dedicated_ba' =>
            'Create cs user (forced) but not coop member + dedicated billingAccount',
            'create_coop_member_cs_user' =>
            'Create cs user + Coop member not completed',
            'create_coop_member_cs_user_dedicated_ba' =>
            'Create cs user + dedicated billingAccount + Coop member not completed',
            'create_coop_member_cs_user_force_register' =>
            'Create cs user (forced) + Coop member not completed',
            'create_coop_member_cs_user_force_register_dedicated_ba' =>
            'Create cs user (forced) + dedicated billingAccount + Coop member not completed',
            'create_coop_member_completed' =>
            'Create coop member completed, no reward',
            'add_cs_user_to_company' =>
            'Add cs user to company',
            'create_client' =>
            'Create client'
          ),
          'default_value' => array(
          ),
          'allow_null' => 0,
          'multiple' => 0,
          'ui' => 0,
          'ajax' => 0,
          'return_format' => 'value',
          'placeholder' => '',
        ),
        array(
          'key' => 'field_5a1d837a0452a',
          'label' => 'Reward (minutes)',
          'name' => 'coupon_reward',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5b680027ce012',
          'label' => 'Reward (money)',
          'name' => 'coupon_reward_money',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5a1fce1b60340',
          'label' => 'Info',
          'name' => 'coupon_reward_info',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5c77cb8ee6b24',
          'label' => 'Tariff (Name)',
          'name' => 'coupon_tariff_name',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5c77cba7e6b25',
          'label' => 'Tariff (Related Model)',
          'name' => 'coupon_tariff_related_model',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5c77cbbee6b26',
          'label' => 'Tariff (Type)',
          'name' => 'member_coupon_tariff_type',
          'type' => 'select',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'choices' => array(
            'pocketbook' => 'pocketbook',
            'time_frame' => 'time_frame',
          ),
          'default_value' => array(
          ),
          'allow_null' => 0,
          'multiple' => 0,
          'ui' => 0,
          'return_format' => 'value',
          'ajax' => 0,
          'placeholder' => '',
        ),
        array(
          'key' => 'field_5c77cbd0e6b27',
          'label' => 'Tariff (Quantity)',
          'name' => 'coupon_tariff_quantity',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5c77c28263b24',
          'label' => 'Group',
          'name' => 'coupon_group',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5c77c28263abc',
          'label' => 'Secondary Group',
          'name' => 'coupon_secondary_group',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5b6800646c298',
          'label' => 'Coupon CS Config',
          'name' => 'coupon_reward_config',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5a28fcfd2e2c7',
          'label' => 'Related Email',
          'name' => 'coupon_related_email',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5c77c2bf63b25',
          'label' => 'Related company CIF',
          'name' => 'coupon_related_company_cif',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array(
          'key' => 'field_5c77c2bf63123',
          'label' => 'Related analytic account',
          'name' => 'coupon_related_analytic_account',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        )
      ),
      'location' => array(
        array(
          array(
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'sm_coupon',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
      'active' => 1,
      'description' => '',
    ));
}
