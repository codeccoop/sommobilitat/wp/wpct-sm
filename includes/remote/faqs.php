<?php

add_filter(
    'posts_bridge_skip_synchronization',
    function ($skip, $rcpt, $data) {
        if ($rcpt->post_type === 'faqs') {
            if (empty($data['project_id'])) {
                return true;
            }

            if ($data['project_id'][1] !== 'FAQS') {
                return true;
            }
        }

        return $skip;
    },
    10,
    3
);

add_filter(
    'posts_bridge_remote_data',
    function ($data, $rcpt) {
        if ($rcpt->post_type === 'faqs') {
            $data['content'] = preg_replace('/\\n/', '', $data['content']);
        }

        return $data;
    },
    10,
    2
);

add_filter(
    'pre_get_posts',
    function ($query) {
        if ($query->is_main_query() && $query->is_search() && !is_admin()) {
            $query->set('post_type', 'faqs');
        }

        return $query;
    },
    10,
    1
);
