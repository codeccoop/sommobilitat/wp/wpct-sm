<?php

function sm_generate_random_string($length = 8)
{
    $characters = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
    $len = strlen($characters);
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[rand(0, $len - 1)];
    }
    return $string;
}

function sm_get_post_by_name($name, $post_type = 'post')
{
    $args = [
        'posts_per_page' => 1,
        'offset' => 0,
        'name' => $name,
        'post_type' => $post_type,
        'post_status' => 'publish',
    ];

    $posts = get_posts($args);
    if (!empty($posts)) {
        return $posts[0];
    }

    return false;
}

function sm_sync_mail_chimp($data)
{
    $email = str_replace(' ', '', strtolower($data['email']));

    $backend = apply_filters('http_bridge_backend', null, 'MailChimp');
    if (!$backend) {
        sm_send_mailchimp_error_email($data, 'No mailchimp backend found');
        return;
    }

    [
        'API-KEY' => $api_key,
        'LIST-ID' => $list_id,
    ] = $backend->headers;

    add_filter(
        'http_bridge_backend_headers',
        function ($headers, $backend) {
            if ($backend->name === 'MailChimp') {
                unset($headers['API-KEY']);
                unset($headers['LIST-ID']);
            }
            return $headers;
        },
        10,
        2
    );

    $res = $backend->post(
        "/lists/{$list_id}/members?skip_merge_validation=true",
        [
            'email_address' => $email,
            'status' => 'subscribed',
            'merge_fields' => [
                'EMAIL' => $email,
                'FNAME' => $data['name'],
                'ZIP' => !empty($data['zip']) ? $data['zip'] : '00000',
            ],
        ],
        [
            'Authorization' => 'Basic ' . base64_encode('user:' . $api_key),
        ]
    );

    if (is_wp_error($res)) {
        sm_send_mailchimp_error_email($data, $res->get_error_message());
    }
}

function sm_is_form_bridged($bridge_name, $form_id = null)
{
    $bridges = array_map(
        fn($bridge) => $bridge->name,
        apply_filters('forms_bridge_bridges', [], $form_id)
    );

    return in_array($bridge_name, $bridges, true);
}

function sm_form_uploads_urls()
{
    $uploads = apply_filters('forms_bridge_uploads', []);
    if ($uploads === null) {
        return [];
    }

    $urls = [];
    foreach ($uploads as $field => $file) {
        if ($file['is_multi']) {
            for ($i = 0; $i < count($file['path']); $i++) {
                $unique_name = $field . '_' . ($i + 1);
                $urls[$unique_name] = forms_bridge_attachment_url(
                    $file['path'][$i]
                );
            }
        } else {
            $urls[$field] = forms_bridge_attachment_url($file['path']);
        }
    }

    return $urls;
}

function sm_form_uploads_base64()
{
    $uploads = apply_filters('forms_bridge_uploads', []);
    if ($uploads === null) {
        return [];
    }

    $urls = [];
    foreach ($uploads as $field => $file) {
        if ($file['is_multi']) {
            for ($i = 0; $i < count($file['path']); $i++) {
                $unique_name = $field . '_' . ($i + 1);
                $content = file_get_contents($file['path'][$i]);
                $urls[$unique_name] = base64_encode($content);
            }
        } else {
            $content = file_get_contents($file['path']);
            $urls[$field] = base64_encode($content);
        }
    }

    return $urls;
}
