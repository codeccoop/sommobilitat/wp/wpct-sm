<?php

function sm_get_coupon_by_name($name)
{
    return sm_get_post_by_name($name, 'sm_coupon');
}

function sm_get_coupon_type($coupon)
{
    if ($coupon) {
        $member_nr = false;
        switch ($coupon->coupon_membership) {
            case 'create_coop_member_completed_cs_user':
                $membership_type = 'create_coop_member_completed_cs_user';
                $complete_member = true;
                $create_cs_user = true;
                $force_register = false;
                $dedicated_ba = false;
                $create_subscription = true;
                break;
            case 'create_coop_member_completed_cs_user_dedicated_ba':
                $membership_type =
                    'create_coop_member_completed_cs_user_dedicated_ba';
                $complete_member = true;
                $create_cs_user = true;
                $force_register = false;
                $dedicated_ba = true;
                $create_subscription = true;
                break;
            case 'create_coop_member_completed_cs_user_force_register':
                $membership_type =
                    'create_coop_member_completed_cs_user_force_register';
                $complete_member = true;
                $create_cs_user = true;
                $force_register = true;
                $dedicated_ba = false;
                $create_subscription = true;
                break;
            case 'create_coop_member_completed_cs_user_force_register_dedicated_ba':
                $membership_type =
                    'create_coop_member_completed_cs_user_force_register_dedicated_ba';
                $complete_member = true;
                $create_cs_user = true;
                $force_register = true;
                $dedicated_ba = true;
                $create_subscription = true;
                break;
            case 'create_cs_user_not_member':
                $membership_type = 'create_cs_user_not_member';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = false;
                $dedicated_ba = false;
                $create_subscription = false;
                $member_nr = $coupon->coupon_related_company_cif ? -3 : -1;
                break;
            case 'create_cs_user_not_member_dedicated_ba':
                $membership_type = 'create_cs_user_not_member_dedicated_ba';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = false;
                $dedicated_ba = true;
                $create_subscription = false;
                $member_nr = $coupon->coupon_related_company_cif ? -3 : -1;
                break;
            case 'create_cs_user_not_member_force_register':
                $membership_type = 'create_cs_user_not_member_force_register';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = true;
                $dedicated_ba = false;
                $create_subscription = false;
                $member_nr = $coupon->coupon_related_company_cif ? -3 : -1;
                break;
            case 'create_cs_user_not_member_force_register_dedicated_ba':
                $membership_type =
                    'create_cs_user_not_member_force_register_dedicated_ba';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = true;
                $dedicated_ba = true;
                $create_subscription = false;
                $member_nr = $coupon->coupon_related_company_cif ? -3 : -1;
                break;
            case 'create_coop_member_cs_user':
                $membership_type = 'create_coop_member_cs_user';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = false;
                $dedicated_ba = false;
                $create_subscription = true;
                break;
            case 'create_coop_member_cs_user_dedicated_ba':
                $membership_type = 'create_coop_member_cs_user_dedicated_ba';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = false;
                $dedicated_ba = true;
                $create_subscription = true;
                break;
            case 'create_coop_member_cs_user_force_register':
                $membership_type = 'create_coop_member_cs_user_force_register';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = true;
                $dedicated_ba = false;
                $create_subscription = true;
                break;
            case 'create_coop_member_cs_user_force_register_dedicated_ba':
                $membership_type =
                    'create_coop_member_cs_user_force_register_dedicated_ba';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = true;
                $dedicated_ba = true;
                $create_subscription = true;
                break;
            case 'create_coop_member':
                $membership_type = 'create_coop_member';
                $complete_member = false;
                $create_cs_user = false;
                $force_register = false;
                $dedicated_ba = false;
                $create_subscription = true;
                break;
            case 'create_coop_member_completed':
                $membership_type = 'create_coop_member_completed';
                $complete_member = true;
                $create_cs_user = false;
                $force_register = false;
                $dedicated_ba = false;
                $create_subscription = true;
                break;
            case 'add_cs_user_to_company':
                $membership_type = 'add_cs_user_to_company';
                $complete_member = false;
                $create_cs_user = true;
                $force_register = false;
                $dedicated_ba = false;
                $create_subscription = false;
                $member_nr = -3;
                break;
            case 'create_client':
                $membership_type = 'create_client';
                $complete_member = false;
                $create_cs_user = false;
                $force_register = false;
                $create_subscription = false;
                $dedicated_ba = false;
                break;
        }

        return [
            'membership_type' => $membership_type,
            'member_nr' => $member_nr,
            'complete_member' => $complete_member,
            'create_cs_user' => $create_cs_user,
            'force_register' => $force_register,
            'dedicated_ba' => $dedicated_ba,
            'create_subscription' => $create_subscription,
        ];
    }

    return false;
}

function get_next_sequence_member_coupon_id()
{
    $posts_array = get_posts([
        'posts_per_page' => 1,
        'offset' => 0,
        'orderby' => 'date',
        'order' => 'DESC',
        'post_type' => 'sm_member_coupon',
        'post_status' => 'publish',
        'suppress_filters' => true,
    ]);

    if (count($posts_array) > 0) {
        return (int) $posts_array[0]->ID + 1;
    } else {
        return 1;
    }
}

function sm_generate_coupon($data, $length = 8)
{
    $name = sm_generate_random_string($length);
    $coupon = sm_get_coupon_by_name($name);

    while ($coupon) {
        $name = sm_generate_random_string($length);
        $coupon = sm_get_coupon_by_name($name);
    }

    $meta = [
        'coupon_used' => null,
        'coupon_related_email' => null,
        'coupon_membership' => null,
        'coupon_reward' => null,
        'coupon_reward_money' => null,
        'coupon_reward_info' => null,
        'coupon_tariff_name' => null,
        'coupon_tariff_related_model' => null,
        'member_coupon_tariff_type' => null,
        'coupon_tariff_quantity' => null,
        'coupon_group' => null,
        'coupon_reward_config' => null,
        'coupon_related_analytic_account' => null,
        'coupon_related_company_cif' => null,
    ];

    foreach (array_keys($meta) as $key) {
        if (isset($data[$key])) {
            $meta[$key] = $data[$key];

            if ($key === 'coupon_used') {
                $meta[$key] = (bool) $meta[$key];
            } elseif ($key === 'coupon_related_email') {
                $meta[$key] = sm_sanitize_email($meta[$key]);
            }
        } else {
            unset($meta[$key]);
        }
    }

    $coupon_id = wp_insert_post([
        'post_title' => $name,
        'post_type' => 'sm_coupon',
        'post_status' => 'publish',
        'post_date' => date('Y-m-d H:i:s'),
        'meta_input' => $meta,
    ]);

    if (!$coupon_id || is_wp_error($coupon_id)) {
        return null;
    }

    return get_post($coupon_id);
}
