<?php

function sm_check_valid_date($data)
{
    $dt = DateTime::createFromFormat("d/m/Y", $data);
    return $dt !== false && empty($dt::getLastErrors());
}

function sm_check_is_adult($data)
{
    if (sm_check_valid_date($data)) {
        $format = "d/m/Y";
        $dateobj = DateTime::createFromFormat($format, $data);
        $now = new DateTime();
        if ($now <= $dateobj) {
            return false;
        }

        $diff = $now->diff($dateobj);
        $diff_months = (($diff->format('%y') * 12) + $diff->format('%m'));
        return $diff_months >= 264;
    }

    return false;
}

function sm_check_not_enough_experience_for_register($birthdate, $dl_expiration_date)
{
    if (sm_check_valid_date($birthdate) && sm_check_valid_date($dl_expiration_date)) {
        $format = "d/m/Y";
        $bd_dateobj = DateTime::createFromFormat($format, $birthdate);
        $now = new DateTime();
        $diff = $now->diff($bd_dateobj);
        $diff_months = (($diff->format('%y') * 12) + $diff->format('%m'));

        if ($diff_months < 300) {
            $exp_dateobj = DateTime::createFromFormat($format, $dl_expiration_date);
            $exp_minusten_dateobj = $exp_dateobj->modify('-10 year');
            $diff = $now->diff($exp_minusten_dateobj);
            $diff_months = (($diff->format('%y') * 12) + $diff->format('%m'));
            return $diff_months >= 24;
        }

        return true;
    }

    return false;
}

function sm_check_valid_expiration_date($data)
{
    if (sm_check_valid_date($data)) {
        $format = "d/m/Y";
        $dateobj = DateTime::createFromFormat($format, $data);
        return new DateTime() < $dateobj;
    }

    return false;
}


function sm_check_valid_postal_code($data)
{
    return preg_match("/^[0-9]{5}$/", $data);
}

$sm_address_states = [
    'Alacant' => 'A',
    'Albacete' => 'AB',
    'Almería' => 'AL',
    'Ávila' => 'AV',
    'Barcelona' => 'B',
    'Badajoz' => 'BA',
    'Bizkaia' => 'BI',
    'Burgos' => 'BU',
    'A Coruña' => 'C',
    'Cádiz' => 'CA',
    'Cáceres' => 'CC',
    'Ceuta' => 'CE',
    'Córdoba' => 'CO',
    'Ciudad Real' => 'CR',
    'Castelló' => 'CS',
    'Cuenca' => 'CU',
    'Las Palmas' => 'GC',
    'Girona' => 'GI',
    'Granada' => 'GR',
    'Guadalajara' => 'GU',
    'Huelva' => 'H',
    'Huesca' => 'HU',
    'Jaén' => 'J',
    'Lleida' => 'L',
    'León' => 'LE',
    'La Rioja' => 'LO',
    'Lugo' => 'LU',
    'Madrid' => 'M',
    'Málaga' => 'MA',
    'Melilla' => 'ME',
    'Murcia' => 'MU',
    'Navarra' => 'NA',
    'Asturias' => 'O',
    'Ourense' => 'OR',
    'Palencia' => 'P',
    'Illes Balears' => 'PM',
    'Pontevedra' => 'PO',
    'Cantabria' => 'S',
    'Salamanca' => 'SA',
    'Sevilla' => 'SE',
    'Segovia' => 'SG',
    'Soria' => 'SO',
    'Gipuzkoa' => 'SS',
    'Tarragona' => 'T',
    'Teruel' => 'TE',
    'Santa Cruz de Tenerife' => 'TF',
    'Toledo' => 'TO',
    'València' => 'V',
    'Valladolid' => 'VA',
    'Álava' => 'VI',
    'Zaragoza' => 'Z',
    'Zamora' => 'ZA',
];

function sm_check_valid_address_state($value)
{
    global $sm_address_states;
    return isset($sm_address_states[$value]);
}

function sm_get_address_state_code($value)
{
    global $sm_address_states;
    if (!isset($sm_address_states[$value])) {
        return null;
    }

    return $sm_address_states[$value];
}

function sm_get_address_state_label($code)
{
    global $sm_address_states;
    foreach ($sm_address_states as $l => $c) {
        if ($code === $c) {
            return $l;
            break;
        }
    }
}
