<?php

function sm_send_member_email($data, $template)
{
    $to = sm_sanitize_email($data['email']);
    $subject = 'Et donem la benvinguda a Som Mobilitat!';
    $body = '';
    $cs_body = false;

    if ($template === 'general') {
        $body = render_member_email_body($data);
        $cs_body = render_member_cs_email_body($data);
        $cs_subject =
            'Informació important sobre el servei de mobilitat elèctrica compartida';
    } elseif ($template === 'montepio') {
        $body = render_member_email_body_montepio($data);
    }

    $headers = [
        'Content-Type: text/html; charset=UTF-8',
        'From: Som Mobilitat <socis@sommobilitat.coop>',
    ];

    wp_mail($to, $subject, $body, $headers);

    if ($cs_body) {
        wp_mail($to, $cs_subject, $cs_body, $headers);
    }
}

function sm_send_admin_email($data)
{
    $to = 'info@sommobilitat.coop';
    $subject = 'Notificació nou soci ' . $data['firstname'];

    $body = '<h2>Tenim un nou soci a Som Mobilitat</h2>';
    $body .= '<p>Les seves dades de registre són:</p>';

    $body .= render_email_member_data($data);

    if ($data['must_register_in_cs']) {
        $body .=
            '<p><strong>Hem d&#39;afegir al soci al carsharing</strong></p>';
    }

    $body .= '<p>Em tornaré a posar en contacte quan tinguem un nou soci.</p>';
    $body .=
        '<p><img src="http://www.sommobilitat.org/wp-content/uploads/2016/03/sm_logo_menu_03.png"/></p>';

    $headers = [
        'Content-Type: text/html; charset=UTF-8',
        'From: Som Mobilitat <info@sommobilitat.org>',
    ];

    wp_mail($to, $subject, $body, $headers);
}

function sm_send_mailchimp_error_email($data, $result)
{
    $to = 'admin@sommobilitat.coop';
    $subject = 'Error subscribing to mailchimp';

    $body = "<ul><li>{$data['name']}</li>";
    $body .= "<li>{$data['email']}</li>";
    $body .= "<li>{$result}</li>";

    $headers = [
        'Content-Type: text/html; charset=UTF-8',
        'From: Som Mobilitat <info@sommobilitat.org>',
    ];

    wp_mail($to, $subject, $body, $headers);
}

function sm_send_add_more_drivers_email($coupon, $data)
{
    $coupon_code = $coupon->post_title;

    $to = sm_sanitize_email($data['email']);
    $subject = 'Registre Empresa Som Mobilitat';

    $body = '<p>Hola</p>';
    $body .=
        '<p>El codi per registrar a les persones usuàries de l’empresa és el: </p>';
    $body .= '<p><strong>' . $coupon_code . '</strong></p>';
    $body .=
        '<p><strong>MOLT IMPORTANT:</strong> Aquest és un codi únic que serveix per a totes les persones usuàries de l’empresa. En el següent enllaç, us podeu registrar individualment cada treballador/a emplenant el codi a la casella "Cupó empresa".</p>';
    $body .=
        '<p><a href="https://www.sommobilitat.coop/registre">www.sommobilitat.coop/registre</a></p>';
    $body .=
        '<p>Si tens qualsevol dubte, ens pots escriure un correu a <a href="mailto:carsharing@sommobilitat.coop">carsharing@sommobilitat.coop</a></p>';
    $body .= '<p>Atentament,</p>';

    $body .=
        '<p>L&#39;equip de Som Mobilitat </p><p><br>&nbsp;</p><p></p><p></p><p></p><table style="font-family: helvetica;font-size: 13px;font-weight: lighter;line-height: 130%;"><tbody><tr><td><a href="http://www.sommobilitat.coop"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/logo.png" style="padding-right:7px;"></a></td><td style="color:#546062; border-left: 1px solid #546062;"><span style="display:block;"><span style="padding-left: 12px;font-weight:normal;">Equip Som Mobilitat </span><br><a href="mailto:info@sommobilitat.coop" style="color:#546062;"><span style="display: block;float:left;padding: 0 0 0 12px;"> info@sommobilitat.coop </span></a></span><span style="display:block;margin-top:26px;"><a href="http://www.sommobilitat.coop" style="display: block;color:#546062;padding-bottom:3px;"><span style="display: block;padding: 0 0 0 12px;"> www.sommobilitat.coop </span></a><a href="https://www.facebook.com/sommobilitat/" style="display:block;color:#546062;"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/facebook.png" style="display: block;float:left;padding:0 7px 0 10px;"><span style="display:block;padding-top:2px;line-height: 80%;">/sommobilitat</span></a><a href="https://twitter.com/sommobilitat" style="display:block;color:#546062;margin-top: 6px;"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/twiter.png" style="display: block;float:left;padding:0 7px 4px 10px;"><span style="display: block;padding-top:3px;line-height: 80%;">@sommobilitat</span></a></span></td></tr></tbody></table>';

    $headers = [
        'Content-Type: text/html; charset=UTF-8',
        'From: Som Mobilitat <carsharing@sommobilitat.org>',
    ];

    wp_mail($to, $subject, $body, $headers);
}

function render_email_member_data($data)
{
    $body = '<ul>';
    $body .=
        '<li><strong>Data d\'alta:</strong> ' .
        date('d-m-Y H:i:s', time()) .
        '</li>';

    if ($data['is_company']) {
        $body .= '<li>Persona jurídica</li>';
    } else {
        $body .= '<li>Persona física</li>';
    }

    $fields = [
        'Nom' => 'firstname',
        'Cognoms' => 'lastname',
        'Raó social' => 'company_name',
        'Persona representant' => 'representative',
        'DNI/NIF' => 'vat',
        'CIF' => 'vat',
        'Correu electrònic' => 'email',
        'Mòbil' => 'mobile',
        'Telèfon' => 'phone',
        'Data de naixement' => 'birthdate',
        'Adreça' => 'address.street',
        'Municipi' => 'address.city',
        'Codi postal' => 'address.zip_code',
        'Província' => 'address.state',
        'IBAN' => 'iban',
    ];

    foreach ($fields as $label => $field) {
        $levels = explode('.', $field);
        $level_data = $data;
        for ($i = 0; $i < count($levels) - 1; $i++) {
            $level_data = $data[$levels[$i]];
        }

        $field = array_pop($levels);
        if (isset($level_data[$field])) {
            $body .= "<li><strong>{$label}:</strong> {$level_data[$field]}</li>";
        }
    }

    if (!empty($data['newsletter'])) {
        $body .= '<li><strong>Butlletí noticies:</strong> Sí</li>';
    }

    $body .= '</ul>';

    return $body;
}

function render_member_email_body($data)
{
    $name = empty($data['company_name'])
        ? $data['firstname']
        : $data['company_name'];

    $body = '<p>Benvingut/da ' . $name . '!</p><p></p>';
    $body .=
        '<p>Hem rebut les teves dades per formalitzar l’alta de soci/a de la cooperativa. </p>';
    $body .=
        '<p>Properament les validarem i efectuarem el cobrament de l’aportació al capital social de la cooperativa (10 €, aportació única). Quan s’hagi completat tot el procés rebràs un correu electrònic amb el teu número de soci/a.</p>';
    $body .= '<p>Les dades que ens has facilitat són:</p>';

    $body .= render_email_member_data($data);

    $body .=
        '<p>Si vols fer algun canvi en les dades proporcionades, ens pots escriure a <a href="mailto:socis@sommobilitat.coop">socis@sommobilitat.coop</a>.</p>';

    $body .=
        '<p><span style="display:inline-block;padding-bottom: 20px;padding-right: 10px;vertical-align: middle;">Ajuda’ns a fer créixer el projecte. Comparteix-ho a les teves xarxes socials!</span>';
    $body .=
        '<a class="social-share-link facebook-share-button" style="display:inline-block;" href="https://www.facebook.com/dialog/share?app_id=1747751018690664&display=popup&href=https%3A//www.sommobilitat.coop/formulari-fes-te-soci/&quote=M%27acabo%20de%20fer%20soci%20de%20Som%20Mobilitat%20https%3A//www.sommobilitat.coop/formulari-fes-te-soci/%20Entre%20tots%20farem%20una%20mobilitat%20%2Bsostenible" data-size="large"><img src="https://www.sommobilitat.coop/wp-content/uploads/2018/12/if_facebook_circle.png"/></a>';
    $body .=
        '<a class="social-share-link twitter-share-button" style="display:inline-block;margin-left: 10px;" href=https://twitter.com/intent/tweet?text=M%27acabo%20de%20fer%20soci%20de%20Som%20Mobilitat%20https%3A//www.sommobilitat.coop/formulari-fes-te-soci/%20Entre%20tots%20farem%20una%20mobilitat%20%2Bsostenible" data-size="large"><img src="https://www.sommobilitat.coop/wp-content/uploads/2018/12/if_twitter_circle.png"/></a>';
    $body .= '</p>';
    $body .= '<p>Moltes gràcies pel teu suport, </p>';

    $body .=
        '<p>L&#39;equip de Som Mobilitat </p><p><br>&nbsp;</p><p></p><p></p><p></p><table style="font-family: helvetica;font-size: 13px;font-weight: lighter;line-height: 130%;"><tbody><tr><td><a href="http://www.sommobilitat.coop"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/logo.png" style="padding-right:7px;"></a></td><td style="color:#546062; border-left: 1px solid #546062;"><span style="display:block;"><span style="padding-left: 12px;font-weight:normal;">Equip Som Mobilitat </span><br><a href="mailto:info@sommobilitat.coop" style="color:#546062;"><span style="display: block;float:left;padding: 0 0 0 12px;"> info@sommobilitat.coop </span></a></span><span style="display:block;margin-top:26px;"><a href="http://www.sommobilitat.coop" style="display: block;color:#546062;padding-bottom:3px;"><span style="display: block;padding: 0 0 0 12px;"> www.sommobilitat.coop </span></a><a href="https://www.facebook.com/sommobilitat/" style="display:block;color:#546062;"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/facebook.png" style="display: block;float:left;padding:0 7px 0 10px;"><span style="display:block;padding-top:2px;line-height: 80%;">/sommobilitat</span></a><a href="https://twitter.com/sommobilitat" style="display:block;color:#546062;margin-top: 6px;"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/twiter.png" style="display: block;float:left;padding:0 7px 4px 10px;"><span style="display: block;padding-top:3px;line-height: 80%;">@sommobilitat</span></a></span></td></tr></tbody></table>';
    return $body;
}

function render_member_cs_email_body($data)
{
    $name = empty($data['company_name'])
        ? $data['firstname']
        : $data['company_name'];
    $body = "<p>Hola {$name}!</p><p></p>";
    $body .=
        '<p>Si has indicat que vols registrar-te al servei de carsharing i totes les dades són correctes, en el termini màxim de 48 hores ja podràs activar el teu usuari a l’app i començar a llogar els vehicles.</p>';

    $body .=
        '<p>Descarrega’t l’app a: <a href="https://play.google.com/store/apps/details?id=coop.sommobilitat.carsharing&hl=ca&gl=US">Google Play</a> (Android) o <a href="https://apps.apple.com/us/app/som-mobilitat-carsharing/id1332988058">App Store</a> (iPhone).</p>';
    $body .=
        '<p>A Som Mobilitat oferim el primer servei cooperatiu de mobilitat elèctrica compartida a Catalunya, gestionat per tota la comunitat de sòcies i usuàries. Entre totes ens cuidem del bon funcionament del servei i fem possible una mobilitat +sostenible.</p>';
    $body .=
        '<p><strong>Vols saber més sobre Som Mobilitat i els nostres serveis?</strong></p>';

    $body .= '<p><ul>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/faqs-preguntes-mes-frequents/#qui-som">Qui som i què volem</a> <a href="https://www.sommobilitat.coop/faqs-preguntes-mes-frequents/#qui-som">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/faqs-preguntes-mes-frequents/#organitzacio">Com ens organitzem</a> <a href="https://www.sommobilitat.coop/faqs-preguntes-mes-frequents/#organitzacio">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/funcionament-servei-carsharing/">Com funciona el servei de mobilitat compartida</a> <a href="https://www.sommobilitat.coop/funcionament-servei-carsharing/">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/wp-content/uploads/2018/04/Condicions_ACTUALS_MAPA.pdf">Condicions d’ús del servei</a> <a href="https://www.sommobilitat.coop/wp-content/uploads/2018/04/Condicions_ACTUALS_MAPA.pdf">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/inverteix/">Com financem la compra dels vehicles</a> <a href="https://www.sommobilitat.coop/inverteix/">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/activitat/">Activitat de la cooperativa</a> <a href="https://www.sommobilitat.coop/activitat/">(+)</a></li>';
    $body .= '</ul></p>';

    $body .=
        '<p><span style="display:inline-block;padding-bottom: 20px;padding-right: 10px;vertical-align: middle;">Ajuda’ns a fer créixer el projecte. Comparteix-ho a les teves xarxes socials!</span>';
    $body .=
        '<a class="social-share-link facebook-share-button" style="display:inline-block;" href="https://www.facebook.com/dialog/share?app_id=1747751018690664&display=popup&href=https%3A//www.sommobilitat.coop/formulari-fes-te-soci/&quote=M%27acabo%20de%20fer%20soci%20de%20Som%20Mobilitat%20https%3A//www.sommobilitat.coop/formulari-fes-te-soci/%20Entre%20tots%20farem%20una%20mobilitat%20%2Bsostenible" data-size="large"><img src="https://www.sommobilitat.coop/wp-content/uploads/2018/12/if_facebook_circle.png"/></a>';
    $body .=
        '<a class="social-share-link twitter-share-button" style="display:inline-block;margin-left: 10px;" href=https://twitter.com/intent/tweet?text=M%27acabo%20de%20fer%20soci%20de%20Som%20Mobilitat%20https%3A//www.sommobilitat.coop/formulari-fes-te-soci/%20Entre%20tots%20farem%20una%20mobilitat%20%2Bsostenible" data-size="large"><img src="https://www.sommobilitat.coop/wp-content/uploads/2018/12/if_twitter_circle.png"/></a>';
    $body .= '</p>';

    $body .= '<p>Moltes gràcies pel teu suport, </p>';

    $body .=
        '<p>L&#39;equip de Som Mobilitat </p><p><br>&nbsp;</p><p></p><p></p><p></p><table style="font-family: helvetica;font-size: 13px;font-weight: lighter;line-height: 130%;"><tbody><tr><td><a href="http://www.sommobilitat.coop"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/logo.png" style="padding-right:7px;"></a></td><td style="color:#546062; border-left: 1px solid #546062;"><span style="display:block;"><span style="padding-left: 12px;font-weight:normal;">Equip Som Mobilitat </span><br><a href="mailto:info@sommobilitat.coop" style="color:#546062;"><span style="display: block;float:left;padding: 0 0 0 12px;"> info@sommobilitat.coop </span></a></span><span style="display:block;margin-top:26px;"><a href="http://www.sommobilitat.coop" style="display: block;color:#546062;padding-bottom:3px;"><span style="display: block;padding: 0 0 0 12px;"> www.sommobilitat.coop </span></a><a href="https://www.facebook.com/sommobilitat/" style="display:block;color:#546062;"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/facebook.png" style="display: block;float:left;padding:0 7px 0 10px;"><span style="display:block;padding-top:2px;line-height: 80%;">/sommobilitat</span></a><a href="https://twitter.com/sommobilitat" style="display:block;color:#546062;margin-top: 6px;"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/twiter.png" style="display: block;float:left;padding:0 7px 4px 10px;"><span style="display: block;padding-top:3px;line-height: 80%;">@sommobilitat</span></a></span></td></tr></tbody></table>';

    return $body;
}

function render_member_email_body_montepio($data)
{
    $name = $data['firstname'];

    $body = '<p>Benvingut/da ' . $name . '!</p><p></p>';
    $body .=
        '<p>En el marc del projecte e-Montepio et donem la benvinguda a Som Mobilitat, la cooperativa que ofereix el primer servei de vehicles elèctrics compartits de Catalunya.</p>';
    $body .=
        '<p>Per poder gaudir de les hores de mobilitat que tens contractades amb Montepio:</p><ol>';

    $body .=
        '<li>Activarem la teva alta de soci/a a Som Mobilitat. Per fer-ho, properament girarem al teu compte corrent l’import de 10 € corresponent a la quota única obligatòria (es retornen en cas de donar-se de baixa de la cooperativa).</li>';
    $body .=
        '<li>Rebràs un correu amb les instruccions i la contrasenya per accedir a l’aplicació que et permetrà reservar + obrir + conduir els vehicles que hi trobaràs.</li>';
    $body .=
        '<li>Un cop tinguis l’app instal·lada veuràs que el teu usuari té dos grups de vehicles disponibles dins de l’app:<ol><li>Grup general de Som Mobilitat: on podràs veure i reservar tots els vehicles de la flota de Som Mobilitat (inclòs el de Manresa).</li><li>Grup tancat de Montepio: on podràs veure i reservar exclusivament el vehicle de Montepio i gaudir de les hores 12 hores de mobilitat elèctrica gratuïta que tens a la teva disposició.</li></ol></li>';
    $body .= '<p></p>';
    $body .= '<p><strong>Resum de la teva alta de soci/a</strong></p>';

    $body .= '<p>Les teves dades de registre són:</p>';

    $body .= render_email_member_data($data);

    $body .=
        '<p>Si vols fer algun canvi en les dades proporcionades, ens pots escriure a <a href="mailto:socis@sommobilitat.coop">socis@sommobilitat.coop</a>.</p>';
    $body .= '<p></p>';

    $body .=
        '<p><strong>Vols saber més sobre Som Mobilitat i els nostres serveis?</strong></p>';

    $body .= '<p><ul>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/faqs-preguntes-mes-frequents/#qui-som">Qui som i què volem</a> <a href="https://www.sommobilitat.coop/faqs-preguntes-mes-frequents/#qui-som">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/faqs-preguntes-mes-frequents/#organitzacio">Com ens organitzem</a> <a href="https://www.sommobilitat.coop/faqs-preguntes-mes-frequents/#organitzacio">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/funcionament-servei-carsharing/">Com funciona el servei de mobilitat compartida</a> <a href="https://www.sommobilitat.coop/funcionament-servei-carsharing/">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/wp-content/uploads/2018/04/Condicions_ACTUALS_MAPA.pdf">Condicions d’ús del servei</a> <a href="https://www.sommobilitat.coop/wp-content/uploads/2018/04/Condicions_ACTUALS_MAPA.pdf">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/inverteix/">Com financem la compra dels vehicles</a> <a href="https://www.sommobilitat.coop/inverteix/">(+)</a></li>';
    $body .=
        '<li><a href="https://www.sommobilitat.coop/activitat/">Activitat de la cooperativa</a> <a href="https://www.sommobilitat.coop/activitat/">(+)</a></li>';
    $body .= '</ul></p>';

    $body .=
        '<p><span style="display:inline-block;padding-bottom: 20px;padding-right: 10px;vertical-align: middle;">Ajuda’ns a fer créixer el projecte. Comparteix-ho a les teves xarxes socials!</span>';
    $body .=
        '<a class="social-share-link facebook-share-button" style="display:inline-block;" href="https://www.facebook.com/dialog/share?app_id=1747751018690664&display=popup&href=https%3A//www.sommobilitat.coop/formulari-fes-te-soci/&quote=M%27acabo%20de%20fer%20soci%20de%20Som%20Mobilitat%20https%3A//www.sommobilitat.coop/formulari-fes-te-soci/%20Entre%20tots%20farem%20una%20mobilitat%20%2Bsostenible" data-size="large"><img src="https://www.sommobilitat.coop/wp-content/uploads/2018/12/if_facebook_circle.png"/></a>';
    $body .=
        '<a class="social-share-link twitter-share-button" style="display:inline-block;margin-left: 10px;" href=https://twitter.com/intent/tweet?text=M%27acabo%20de%20fer%20soci%20de%20Som%20Mobilitat%20https%3A//www.sommobilitat.coop/formulari-fes-te-soci/%20Entre%20tots%20farem%20una%20mobilitat%20%2Bsostenible" data-size="large"><img src="https://www.sommobilitat.coop/wp-content/uploads/2018/12/if_twitter_circle.png"/></a>';
    $body .= '</p>';
    $body .= '<p>Moltes gràcies pel teu suport, </p>';

    $body .=
        '<p>L&#39;equip de Som Mobilitat </p><p><br>&nbsp;</p><p></p><p></p><p></p><table style="font-family: helvetica;font-size: 13px;font-weight: lighter;line-height: 130%;"><tbody><tr><td><a href="http://www.sommobilitat.coop"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/logo.png" style="padding-right:7px;"></a></td><td style="color:#546062; border-left: 1px solid #546062;"><span style="display:block;"><span style="padding-left: 12px;font-weight:normal;">Equip Som Mobilitat </span><br><a href="mailto:info@sommobilitat.coop" style="color:#546062;"><span style="display: block;float:left;padding: 0 0 0 12px;"> info@sommobilitat.coop </span></a></span><span style="display:block;margin-top:26px;"><a href="http://www.sommobilitat.coop" style="display: block;color:#546062;padding-bottom:3px;"><span style="display: block;padding: 0 0 0 12px;"> www.sommobilitat.coop </span></a><a href="https://www.facebook.com/sommobilitat/" style="display:block;color:#546062;"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/facebook.png" style="display: block;float:left;padding:0 7px 0 10px;"><span style="display:block;padding-top:2px;line-height: 80%;">/sommobilitat</span></a><a href="https://twitter.com/sommobilitat" style="display:block;color:#546062;margin-top: 6px;"><img src="http://www.sommobilitat.coop/wp-content/uploads/2016/10/twiter.png" style="display: block;float:left;padding:0 7px 4px 10px;"><span style="display: block;padding-top:3px;line-height: 80%;">@sommobilitat</span></a></span></td></tr></tbody></table>';

    return $body;
}
