<?php

function sm_sanitize_email($email)
{
    return str_replace(' ', '', strtolower($email));
}

function sm_format_member_date($date)
{
    if (is_array($date)) {
        return call_user_func_array('sm_format_member_datearr', $date);
    } else {
        return sm_format_member_datestr($date);
    }
}

function sm_format_member_datearr($day, $month, $year)
{
    $day = strlen($day) === 1 ? '0'.$day : $day;
    $month = strlen($month) === 1 ? '0'.$month : $month;
    return "{$day}/{$month}/{$year}";
}

function sm_format_member_datestr($date)
{
    if (preg_match('/(\d{4})-(\d{2})-(\d{2})/', $date, $matches)) {
        return implode('/', array_reverse(array_slice($matches, 1)));
    } elseif (preg_match('/(\d{2})-(\d{2})-(\d{4})/', $date, $matches)) {
        return implode('/', array_slice($matches, 1));
    }
}

function sm_format_postdata_date($date)
{
    if (is_array($date)) {
        return call_user_func_array('sm_format_postdata_datearr', $date);
    } else {
        return sm_format_postdata_datestr($date);
    }
}

function sm_format_postdata_datearr($day, $month, $year)
{
    $month = strlen($month) === 1 ? '0'.$month : $month;
    $day = strlen($day) === 1 ? '0'.$day : $day;
    return "{$year}-{$month}-{$day}";
}

function sm_format_postdata_datestr($date)
{
    if (preg_match('/(\d{4})-(\d{2})-(\d{2})/', $date, $matches)) {
        return implode('-', array_slice($matches, 1));
    } elseif (preg_match('/(\d{2})-(\d{2})-(\d{4})/', $date, $matches)) {
        return implode('-', array_reverse(array_slice($matches, 1)));
    }
}

function sm_sanitize_vat($vat)
{
    return strtoupper(preg_replace("/\s/", "", $vat));
}
