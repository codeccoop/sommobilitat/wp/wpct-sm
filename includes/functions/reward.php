<?php

function sm_parse_reward_partner_data($data)
{
    $postdata = [];

    $meta_table = [
        'data_partner_firstname' => 'firstname',
        'data_partner_lastname' => 'lastname',
        'data_partner_mobile' => 'mobile',
        'data_partner_phone' => 'phone',
        'data_partner_gender' => 'gender',
        'data_partner_birthdate_date' => 'birthdate',
        'data_partner_iban' => 'iban',
        'data_partner_image_dni' => 'image_dni',
    ];

    $meta_address_table = [
        'data_partner_street' => 'street',
        'data_partner_zip' => 'zip_code',
        'data_partner_city' => 'city',
        'data_partner_state' => 'state',
    ];

    $member_meta = sm_get_member_data($data);
    foreach ($meta_table as $reward_key => $member_key) {
        if (isset($member_meta[$member_key])) {
            $postdata[$reward_key] = $member_meta[$member_key];
        }
    }

    foreach ($meta_address_table as $reward_key => $member_key) {
        if (isset($member_meta['address'][$member_key])) {
            $postdata[$reward_key] = $member_meta['address'][$member_key];
        }
    }

    if (isset($data['driving_license_expiration_date'])) {
        $postdata['data_partner_driving_license_expiration_date'] =
            $data['driving_license_expiration_date'];
    }

    if (isset($data['image_driving_license'])) {
        $postdata['data_partner_image_driving_license'] =
            $data['image_driving_license'];
    }

    return $postdata;
}

function sm_parse_reward($data, $coupon)
{
    $register_type_data = sm_get_coupon_type($coupon);

    $member_email = sm_sanitize_email($data['email']);
    $member_dni = sm_sanitize_vat($data['vat']);

    $next_mc_id = get_next_sequence_member_coupon_id();
    return [
        'force_register_cs' => (bool) $register_type_data['force_register'],
        'force_dedicated_ba' => (bool) $register_type_data['dedicated_ba'],
        'reward_type' => 'promocode',
        'reward_date' => date('Y-m-d', strtotime('now')),
        'reward_addtime' => (int) $coupon->coupon_reward,
        'reward_addmoney' => (float) $coupon->coupon_reward_money,
        'promo_code' => $coupon->post_title,
        'tariff_name' => $coupon->coupon_tariff_name,
        'tariff_related_model' => $coupon->coupon_tariff_related_model,
        'tariff_type' => $coupon->member_coupon_tariff_type,
        'tariff_quantity' => $coupon->coupon_tariff_quantity,
        'coupon_group' => $coupon->coupon_group,
        'coupon_group_secondary' => $coupon->coupon_group_secondary,
        'reward_config' => $coupon->coupon_reward_config,
        'reward_info' => $coupon->coupon_reward_info,
        'external_promo_obj_id' => $coupon->ID,
        'related_analytic_account' => $coupon->coupon_related_analytic_account,
        'data_partner_email' => $member_email,
        'data_partner_vat' => $member_dni,
        'external_obj_id' => $next_mc_id,
        'name' => "Promo: {$next_mc_id} - {$coupon->post_title}",
        'data_partner_creation_type' =>
            $data['member_or_user'] === 'yes'
                ? 'existing'
                : ($register_type_data['create_subscription']
                    ? 'subscription'
                    : 'new'),
        'data_partner_cs_user_type' =>
            $register_type_data['force_register'] &&
            $coupon->coupon_group_secondary
                ? 'user'
                : ($coupon->coupon_related_company_cif
                    ? 'user'
                    : 'promo'),
    ];
}
