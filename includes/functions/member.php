<?php

function sm_register_member($data, $coupon = null)
{
    $register_type_data = sm_get_coupon_type($coupon) ?: [
        'membership_type' => 'create_coop_member',
        'member_nr' => 0,
        'create_cs_user' => false,
        'complete_member' => false,
        'force_register' => false,
        'dedicated_ba' => false,
    ];

    $notification_template = 'general';

    if ($data['is_company']) {
        $post_cupon = sm_generate_coupon([
            'coupon_used' => false,
            'coupon_membership' => 'add_cs_user_to_company',
            'coupon_related_email' => sm_sanitize_email($data['company_email']),
            'coupon_related_company_cif' => $data['vat'],
            'coupon_reward_info' => 'Register in company: ' . $data['vat'],
        ]);

        sm_send_add_more_drivers_email($post_cupon, $data);
    }

    sm_send_admin_email($data);

    if (
        $register_type_data['membership_type'] !== 'create_cs_user_not_member'
    ) {
        sm_send_member_email($data, $notification_template);
    }

    if ($data['newsletter']) {
        sm_sync_mail_chimp([
            'name' => $data['name'],
            'email' => sm_sanitize_email($data['email']),
            'zip' => $data['address']['zip_code'],
        ]);
    }
}

function sm_get_member_name($data)
{
    $member_type = isset($data['user_type']) ? $data['user_type'] : 'persona';

    switch ($member_type) {
        case 'persona':
            return $data['lastname'] . ', ' . $data['firstname'];
            break;
        case 'juridic':
            return $data['company_name'];
            break;
    }
}

function sm_get_member_data($data)
{
    $name = sm_get_member_name($data);

    $member_data = [
        // 'external_obj_id' => $data['submission_id'],
        'ordered_parts' => 1,
        'share_product' => (int) ($data['share_product'] ?? 1),
        'is_company' =>
            isset($data['user_type']) && $data['user_type'] === 'juridic',
        'name' => $name,
        'firstname' => $data['firstname'],
        'lastname' => $data['lastname'],
        'email' => sm_sanitize_email($data['email']),
        'mobile' => $data['mobile'],
        'vat' => $data['vat'],
        'address' => [
            'country' => 'ES',
            'street' => $data['street'],
            'zip_code' => $data['zip_code'],
            'state' => sm_get_address_state_code($data['state']),
            'city' => $data['city'],
        ],
        'iban' => str_replace(' ', '', $data['iban']),
        'lang' => 'ca_ES',
        'newsletter' =>
            isset($data['newsletter'][0]) && $data['newsletter'][0] === '1',
        'must_register_in_cs' =>
            isset($data['user_type']) && $data['user_type'] === 'juridic',
    ];

    if ($member_data['is_company']) {
        $member_data['company_name'] = $data['company_name'];
        $member_data['representative_vat'] = $data['representative_vat'];
        $member_data['company_email'] = sm_sanitize_email($data['email']);
    } else {
        $member_data['gender'] = $data['gender'];
        $member_data['birthdate'] = sm_format_postdata_date($data['birthdate']);
    }

    // conditionals
    if (isset($data['phone'])) {
        $member_data['phone'] = $data['phone'];
    }
    if (isset($data['image_dni'])) {
        $member_data['image_dni'] = $data['image_dni'];
    }

    return $member_data;
}
