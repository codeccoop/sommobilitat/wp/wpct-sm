<?php

add_action('init', function () {
    register_post_type(
        'sm_coupon',
        [
            'labels' => [
                'name' => __('Cupons', 'wpct-sm'),
                'singular_name' => __('Cupons', 'wpct-sm')
            ],
            // Frontend
            'has_archive' => false,
            'public' => true,
            'publicly_queryable' => true,
            // Admin
            // 'capability_type' => 'sm_coupon',
            'menu_icon' => 'dashicons-businessman',
            'menu_position' => 28,
            'query_var' => true,
            'show_in_menu' => true,
            'show_ui' => true,
            'supports' => [
                'title',
                'author',
                'custom-fields',
            ],
            'rewrite' => ['slug' => 'cupo'],
            // 'map_meta_cap' => true,
            // 'capabilities' => [
            //     'read_post' => 'read_post',
            //     'delete_post' => 'delete_post',
            //     'edit_post' => 'edit_post',
            //     'edit_others_posts' => 'edit_other_posts',
            //     'delete_others_posts' => 'delete_other_posts',
            //     'publish_posts' => 'publish_posts',
            //     'read_private_posts' => 'read_private_posts',
            //     'create_posts' => 'create_posts',
            // ],
        ],
    );
});

// COLUMN IN LIST VIEW
add_filter('manage_sm_coupon_posts_columns', 'sm_coupon_columns_head');
function sm_coupon_columns_head($defaults)
{
    $defaults['coupon_used'] = 'Used';
    $defaults['coupon_reward_info'] = 'Info';
    return $defaults;
}

add_action('manage_sm_coupon_posts_custom_column', 'sm_coupon_columns_content', 10, 2);
function sm_coupon_columns_content($column_name, $post_id)
{
    if ($column_name === 'coupon_used') {
        $coupon_used = get_post_meta($post_id, 'coupon_used', true);
        if ($coupon_used) {
            echo 'Yes';
        } else {
            echo 'No';
        }
    }
    if ($column_name == 'coupon_reward_info') {
        echo get_post_meta($post_id, 'coupon_reward_info', true);
    }
}

add_filter('manage_edit-sm_coupon_sortable_columns', 'sm_coupon_manage_sortable_columns');
function sm_coupon_manage_sortable_columns($sortable_columns)
{
    $sortable_columns['coupon_used'] = 'Used';
    $sortable_columns['coupon_reward_info'] = 'Info';
    return $sortable_columns;
}
