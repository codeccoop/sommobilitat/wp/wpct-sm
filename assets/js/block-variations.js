wp.blocks.registerBlockVariation("core/table", {
  name: "dynamic-table",
  title: wp.i18n.__("Taula dinàmica", "wpct-sm"),
  description: wp.i18n.__("Taula amb filtre i ordenació", "wpct-sm"),
  scope: ["inserter"],
  isDefault: false,
  attributes: {
    head: [
      {
        cells: Array.apply(null, Array(4)).map(() => ({
          content: wp.i18n.__("Indicador", "wpct-sm"),
          tag: "th",
          align: "left",
        })),
      },
    ],
    body: Array.apply(null, Array(9)).map(() => ({
      cells: Array.apply(null, Array(3))
        .map(() => ({
          content: "xxxx",
          tag: "td",
        }))
        .concat([{ content: "<a href='#'>Link</a>", tag: "td" }]),
    })),
    className: "is-variation-dynamic",
  },
  isActive: (blockAttributes) => {
    return (
      /is-variation-dynamic/.test(blockAttributes.className) &&
      (Object.prototype.hasOwnProperty.call(
        blockAttributes,
        "hasFixedLayout"
      ) === false ||
        blockAttributes.hasFixedLayout === true) &&
      blockAttributes.head.length >= 1
    );
  },
});

wp.blocks.registerBlockVariation("core/table", {
  name: "horizontal-table",
  title: wp.i18n.__("Taula horitzontal", "wpct-sm"),
  description: wp.i18n.__("Taula amb scroll horitzontal", "wpct-sm"),
  scope: ["inserter"],
  isDefault: false,
  attributes: {
    hasFixedLayout: false,
    className: "is-variation-horizontal",
    head: [
      {
        cells: Array.apply(null, Array(6)).map(() => ({
          content: wp.i18n.__("Indicator", "wpct-sm"),
          tag: "th",
          align: "left",
        })),
      },
    ],
    body: Array.apply(null, Array(5)).map(() => ({
      cells: Array.apply(null, Array(5))
        .map(() => ({
          content: "xxxx",
          tag: "td",
        }))
        .concat([{ content: "<a href='#'>Link</a>", tag: "td" }]),
    })),
  },
  isActive: (blockAttributes) => {
    return (
      /is-variation-horizontal/.test(blockAttributes.className) &&
      blockAttributes.hasFixedLayout === false
    );
  },
});
