# Wpct Som Mobilitat

Child theme boilerplate to be used with WP Coop Theme

## Downloads
Visit [releases](../../releases) page or get the [latest](../../releases/permalink/latest/downloads/themes/wpct-sm.zip) zip.

> Releases are generated automatically by CI/CD pipeline every semver compatible git tag. In order to trigger the pipeline,
[tag](../../tags/new) the commit you want to release from with the [proper next version](https://semver.org/).

## General

Please visit [WP Coop Theme](https://git.coopdevs.org/codeccoop/wp/wp-coop-theme) for more detailed documentation

## Development

* Run `npm install`
* Run `npm run dev` to start developing

# Build

* Run `npm run build`

## Contributors

* [Coopdevs](https://coopdevs.org)
* [Som Energia](https://somenergia.coop)
* [Talaios](https://talaios.coop)
* [Còdec](https://www.codeccoop.org)

## License

MIT. Please see the [License File](https://git.coopdevs.org/codeccoop/wp/wp-coop-theme/-/blob/main/LICENSE) for more information.
