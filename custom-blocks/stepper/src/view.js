import "./style.css";

let styles = getComputedStyle(document.documentElement);
const md = +styles.getPropertyValue("--breakpoint-md").replace(/px/, "");

(function () {
  const debounce = function (fn, ms) {
    let last;
    return function () {
      clearTimeout(last);
      last = setTimeout(fn, ms);
    };
  };

  document.addEventListener("DOMContentLoaded", function () {
    document.querySelectorAll(".wpct-block-stepper").forEach(function (el) {
      window.addEventListener(
        "resize",
        debounce(() => responsiveBootstrap(el), 400)
      );

      responsiveBootstrap(el);
    });
  });

  function addIndexes(el) {
    const steps = el.querySelectorAll(".wpct-block-stepper-step");
    steps.forEach((step, i) => {
      step.querySelector("figure").setAttribute("data-index", i + 1);
    });
  }

  function responsiveBootstrap(el) {
    addIndexes(el);
    if (window.innerWidth > md) {
      try {
        unmountSlick(el);
      } catch (err) {
        // do nothin
      }
    } else {
      mountSlick(el);
    }
  }

  function unmountSlick(el) {
    jQuery(el).find(".slick-slider").slick("unslick");
    const steps = el.querySelectorAll(".wpct-block-stepper-step");
    steps.forEach((step) => {
      step.parentElement.removeChild(step);
      el.appendChild(step);
    });
    el.removeChild(el.querySelector(".wpct-block-stepper-wrapper"));
    el.removeChild(el.querySelector(".wpct-block-stepper-arrows"));
  }

  function mountSlick(el) {
    el.classList.add("ready");

    const wrapper = document.createElement("div");
    wrapper.classList.add("wpct-block-stepper-wrapper");

    const slides = el.querySelectorAll(".wpct-block-stepper-step");
    slides.forEach((slide) => {
      el.removeChild(slide);
      wrapper.appendChild(slide);
    });

    el.appendChild(wrapper);

    const arrowsWrapper = document.createElement("div");
    arrowsWrapper.classList.add("wpct-block-stepper-arrows");
    el.appendChild(arrowsWrapper);

    jQuery(wrapper).slick({
      dots: false,
      arrows: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      infinite: false,
      centerMode: true,
      autoplay: false,
      initialSlide: 0,
      appendArrows: jQuery(arrowsWrapper),
      adaptiveHeight: true,
    });
  }
})();
