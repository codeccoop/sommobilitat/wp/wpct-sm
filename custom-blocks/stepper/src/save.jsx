const { useBlockProps, useInnerBlocksProps } = wp.blockEditor;

export default function save() {
  const blockProps = useBlockProps.save({
    className: "wpct-block-stepper wp-block-group",
  });
  const innerBlocksProps = useInnerBlocksProps.save(blockProps);

  return <div {...innerBlocksProps} />;
}
