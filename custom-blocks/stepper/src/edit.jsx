import "./editor.css";

const { __ } = wp.i18n;
const {
  useBlockProps,
  useInnerBlocksProps,
  InspectorControls,
  store: blockEditorStore,
} = wp.blockEditor;
const { useDispatch, useSelect } = wp.data;
const { createBlock, createBlocksFromInnerBlocksTemplate } = wp.blocks;
const { PanelBody, RangeControl } = wp.components;

function StepperInspectorControls({ clientId, attributes, setAttributes }) {
  const { steps = 4 } = attributes;
  const { getBlocks } = useSelect(blockEditorStore);
  const { replaceInnerBlocks } = useDispatch(blockEditorStore);

  function updateSteps(to, from) {
    let innerBlocks = getBlocks(clientId);

    if (to > from) {
      innerBlocks = innerBlocks.concat(
        Array.from(Array(to - from)).map(() =>
          createBlock(
            stepBlockTemplate[0],
            stepBlockTemplate[1],
            createBlocksFromInnerBlocksTemplate(stepBlockTemplate[2])
          )
        )
      );
    } else if (from > to) {
      innerBlocks = innerBlocks.slice(0, -(from - to));
    }

    replaceInnerBlocks(clientId, innerBlocks);
    setAttributes({ steps: to });
  }

  return (
    <InspectorControls>
      <PanelBody title={__("Stepper settings", "wpct-sm")}>
        <RangeControl
          label={__("Steps", "wpct-sm")}
          value={steps}
          onChange={(value) => updateSteps(value, steps)}
          min={2}
          max={8}
        />
      </PanelBody>
    </InspectorControls>
  );
}

export default function Edit({ clientId, attributes, setAttributes }) {
  const blockProps = useBlockProps({
    className: `wpct-block-stepper`,
  });
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    defaultBlock: stepBlockTemplate,
    directInsert: true,
    orientation: "horizontal",
    renderAppender: false,
    templateLock: "insert",
    template: Array.from(Array(4)).map(() => stepBlockTemplate),
  });

  return (
    <>
      <StepperInspectorControls
        attributes={attributes}
        setAttributes={setAttributes}
        clientId={clientId}
      />
      <div {...innerBlocksProps} />
    </>
  );
}

const stepBlockTemplate = [
  "core/group",
  {
    className: "wpct-block-stepper-step wp-block-group",
    layout: "default",
    templateLock: false,
    metadata: { name: "SM Step" },
  },
  [
    [
      "core/image",
      {
        aspectRatio: "1",
        scale: "cover",
        sizeSlug: "full",
        lock: {
          remove: false,
        },
      },
    ],
    [
      "core/heading",
      {
        level: 4,
        content: "Step title",
        lock: {
          remove: false,
        },
      },
    ],
    [
      "core/paragraph",
      {
        content: "Step description",
      },
    ],
  ],
];
