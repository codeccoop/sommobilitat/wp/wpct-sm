<?php

add_action('init', 'wpct_block_stepper_init');
function wpct_block_stepper_init()
{
    register_block_type(__DIR__ . '/build');
}

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script(
        'slick-js',
        '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',
        ['jquery'],
        '1.8.1'
    );
});

add_action('after_setup_theme', function () {
    wp_enqueue_block_style('wpct-block/stepper', [
        'handle' => 'slick-css',
        'src' => '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css',
        'deps' => [],
        'ver' => '1.8.1',
    ]);
});

add_filter(
    'render_block',
    function ($block_content, $block) {
        if ('wpct-block/stepper' === $block['blockName']) {
            wp_enqueue_script('slick-js');
        }
        return $block_content;
    },
    10,
    2
);
