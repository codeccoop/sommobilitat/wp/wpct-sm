const { __ } = wp.i18n;
const { CustomSelectControl } = wp.components;

export const OPTIONS = [
  {
    key: "top",
    name: __("Corner Top", "wpct-sm"),
    className: "top",
  },
  {
    key: "full",
    name: __("Full Height", "wpct-sm"),
    className: "full",
  },
  {
    key: "over",
    name: __("Overflow", "wpct-sm"),
    className: "over",
  },
  {
    key: "bottom",
    name: __("Corner Bottom", "wpct-sm"),
    className: "bottom",
  },
];

export default function PaletteControl({ value, setValue }) {
  const option = OPTIONS.find((opt) => opt.key === value) || OPTIONS[0];

  return (
    <CustomSelectControl
      label={__("Disposition", "wpct-sm")}
      options={OPTIONS}
      value={option}
      onChange={({ selectedItem: { key } }) => setValue(key)}
    />
  );
}
