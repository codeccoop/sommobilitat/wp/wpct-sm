const { __ } = wp.i18n;
const { BlockControls, MediaPlaceholder, MediaReplaceFlow } = wp.blockEditor;
const { useSelect } = wp.data;
const { useEntityProp, store: coreStore } = wp.coreData;

export default function CoverImage({ postId, postType }) {
  const onSelect = (value) => {
    setFeaturedImage(value.id);
  };

  const [featuredImage, setFeaturedImage] = useEntityProp(
    "postType",
    postType,
    "featured_media",
    postId
  );

  const { media } = useSelect(
    (select) => {
      const { getMedia } = select(coreStore);
      return {
        media:
          featuredImage &&
          getMedia(featuredImage, {
            context: "view",
          }),
      };
    },
    [featuredImage]
  );

  if (media) {
    return (
      <figure className="wp-block-post-featured-image wpct-page-header-featured-image">
        <BlockControls group="other">
          <MediaReplaceFlow
            mediaURL={media.source_url}
            allowedTypes={["image"]}
            accept="image/*"
            onSelect={onSelect}
          />
        </BlockControls>
        <img clasName="wpct-teaser-thumbnail" src={media.source_url} />
      </figure>
    );
  }

  return (
    <MediaPlaceholder
      onSelect={(el) => onSelect(el)}
      allowedTypes={["image"]}
      multiple={false}
      labels={{ title: __("Teaser image", "wpct-sm") }}
    />
  );
}

CoverImage.Save = function ({ postId, postType }) {
  const [featuredImage] = useEntityProp(
    "postType",
    postType,
    "featured_media",
    postId
  );

  const { media } = useSelect(
    (select) => {
      const { getMedia } = select(coreStore);
      return {
        media:
          featuredImage &&
          getMedia(featuredImage, {
            context: "view",
          }),
      };
    },
    [featuredImage]
  );

  return (
    <figure className="wpct-page-header-featured-image">
      <img src={media?.source_url} />
    </figure>
  );
};
