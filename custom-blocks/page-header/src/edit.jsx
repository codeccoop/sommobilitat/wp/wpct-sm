import PaletteControl from "./PaletteControl.jsx";
import CoverImage from "./CoverImage.jsx";
import DispositionControl from "./DispositionControl.jsx";

import "./editor.css";

const { __ } = wp.i18n;
const { useBlockProps, InnerBlocks, InspectorControls } = wp.blockEditor;
const { PanelBody } = wp.components;

export default function Edit({
  attributes,
  setAttributes,
  context: { postType, postId },
}) {
  const {
    background = "var(--wp--preset--color--brand)",
    disposition = "bottom",
  } = attributes;
  const setAttribute = (attr, value) => {
    setAttributes({ [attr]: value });
  };
  return (
    <>
      <InspectorControls>
        <PanelBody title={__("Page header settings", "wpct-sm")}>
          <PaletteControl
            value={background}
            setValue={(value) => setAttribute("background", value)}
          />
          <DispositionControl
            value={disposition}
            setValue={(value) => setAttribute("disposition", value)}
          />
        </PanelBody>
      </InspectorControls>
      <header
        {...useBlockProps({ className: "wpct-block-page-header alignwide" })}
        data-disposition={disposition}
      >
        <div
          class="wpct-block-page-header-content-wrapper alignwide has-background"
          style={{ backgroundColor: background }}
        >
          <InnerBlocks template={TEMPLATE} templateLock="all" />
        </div>
        <CoverImage postId={postId} postType={postType} />
      </header>
    </>
  );
}

const TEMPLATE = [
  ["core/post-title", { level: 1 }],
  [
    "core/group",
    {
      display: "default",
      className: "wpct-block-page-header-content",
      templateLock: false,
    },
    [["core/post-excerpt"], ["core/buttons", {}, [["core/button"]]]],
  ],
];
