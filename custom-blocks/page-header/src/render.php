<?php
/**
 * Dynamic Block Template.
 * @param   array $attributes - A clean associative array of block attributes.
 * @param   array $block - All the block settings and attributes.
 * @param   string $content - The block inner HTML (usually empty unless using inner blocks).
 */

$post_id = $block->context['postId'];
$featured_image = get_the_post_thumbnail($post_id);
$disposition = $attributes['disposition'];
?>
<header class="wp-block-group wp-block-wpct-block-page-header wpct-block-page-header alignwide" data-disposition="<?= $disposition ?>">
    <div class="wpct-block-page-header-content-wrapper has-background">
		<?= $content ?>
	</div>
	<figure class="wp-block-post-featured-image wpct-block-page-header-featured-image">
		<?= $featured_image ?>
	</figure>
</header>
