<?php
/**
 * Plugin Name:       Page Header
 * Description:       Page header block
 * Requires at least: 6.1
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            Còdec
 * License:           GPL-3.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain:       wpct-sm
 *
 * @package CreateBlock
 */

if (! defined('ABSPATH')) {
    exit;
}

add_action('init', 'wpct_block_page_header_block_init');
function wpct_block_page_header_block_init()
{
    register_block_type(__DIR__ . '/build');
}
