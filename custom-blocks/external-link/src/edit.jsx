import "./editor.css";

const { __ } = wp.i18n;
const { useBlockProps, InspectorControls } = wp.blockEditor;
const { PanelBody, TextControl, SelectControl } = wp.components;

const ARROW_OPTIONS = [
  {
    label: __("Internal", "wpct-sm"),
    value: "internal",
  },
  {
    label: __("External", "wpct-sm"),
    value: "external",
  },
  {
    label: __("Download", "wpct-sm"),
    value: "download",
  },
  {
    label: __("Back", "wpct-sm"),
    value: "back",
  },
];

export default function Edit({ attributes, setAttributes }) {
  const { link, arrow, text } = attributes;
  const blockProps = useBlockProps({
    className: "wpct-external-link",
  });

  const formatLink = (value) => {
    if (!value) return "";
    else value = String(value);

    const schemas = ["https://".split(""), "http://".split("")];
    const isURL = schemas.reduce(
      (isURL, schema) =>
        isURL ||
        schema.reduce((isURL, char, i) => {
          if (i >= value.length) return isURL;
          return isURL && value[i] === char;
        }, true),
      false
    );

    if (!isURL && value.length >= 8) {
      const host = window.location.host;
      return `https://${host}${value.replace(/^\/+/, "")}`;
    }

    return value;
  };

  return (
    <>
      <InspectorControls>
        <PanelBody title={__("External link settings", "wpct-sm")}>
          <TextControl
            label={__("Link text", "wpct-sm")}
            value={text}
            onChange={(text) => setAttributes({ text })}
            __next40pxDefaultSize
            __nextHasNoMarginBottom
          />
          <TextControl
            label={__("Link URL", "wpct-sm")}
            value={link}
            onChange={(link) => setAttributes({ link: formatLink(link) })}
            placeholder="https://"
            __next40pxDefaultSize
            __nextHasNoMarginBottom
          />
          <SelectControl
            label={__("Link type", "wpct-sm")}
            value={arrow}
            onChange={(arrow) => setAttributes({ arrow })}
            options={ARROW_OPTIONS}
            __next40pxDefaultSize
            __nextHasNoMarginBottom
          />
        </PanelBody>
      </InspectorControls>

      <p {...blockProps} data-arrow={arrow}>
        <a href="#">{text}</a>
      </p>
    </>
  );
}
