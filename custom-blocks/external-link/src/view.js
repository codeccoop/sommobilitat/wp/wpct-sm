import "./style.css";

const links = document.querySelectorAll(".wpct-external-link");
for (let link of links) {
  if (link.dataset.arrow === "back") {
    const anchor = link.querySelector("a");
    anchor.removeAttribute("href");
    anchor.addEventListener("click", (ev) => {
      ev.preventDefault();
      history.back();
    });
  }
}
