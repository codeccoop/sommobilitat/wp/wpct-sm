const { useBlockProps } = wp.blockEditor;

export default function save({ attributes }) {
  const { link, text, arrow } = attributes;
  const blockProps = useBlockProps.save({
    className: "wpct-external-link",
  });

  const target = arrow !== "internal" && arrow !== "back" ? "_blank" : "_self";

  return (
    <p {...blockProps} data-arrow={arrow}>
      <a href={link} target={target}>
        {text}
      </a>
    </p>
  );
}
