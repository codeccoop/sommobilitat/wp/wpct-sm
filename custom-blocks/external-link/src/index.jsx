import Edit from "./edit";
import save from "./save";
import metadata from "./block.json";

import "./style.css";

wp.blocks.registerBlockType(metadata.name, {
  edit: Edit,
  save,
});
