const { useBlockProps, InnerBlocks } = wp.blockEditor;

export default function save({ attributes }) {
  const { image, cta, href, background } = attributes;

  const Img = () => {
    if (cta && href) {
      return (
        <a href={href}>
          <img className="wpct-teaser-thumbnail" src={image} />
        </a>
      );
    }

    return <img className="wpct-teaser-thumbnail" src={image} />;
  };

  return (
    <figure {...useBlockProps.save()} style={{ background }}>
      <Img />
      <figcaption>
        <InnerBlocks.Content />
        {href && cta && (
          <p className="wpct-teaser-cta">
            <a className="wpct-teaser-link" href={href}>
              {cta}
            </a>
          </p>
        )}
      </figcaption>
    </figure>
  );
}
