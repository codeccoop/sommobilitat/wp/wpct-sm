const { __ } = wp.i18n;
const { ColorPalette } = wp.components;

const COLORS = [
  {
    color: "var(--wp--preset--color--la-rioja)",
    label: "Wasabi",
  },
  {
    color: "var(--wp--preset--color--swans-downs)",
    label: "Swans Downs",
  },
  {
    color: "var(--wp--preset--color--brand)",
    label: "School Bus",
  },
  {
    color: "var(--wp--preset--color--base)",
    label: "White",
  },
];

export default function PaletteControl({ value, setValue }) {
  return (
    <ColorPalette
      label={__("Teaser color", "wpct-sm")}
      colors={COLORS}
      disableCustomColors={true}
      value={value}
      onChange={setValue}
    />
  );
}
