const { __ } = wp.i18n;
const { TextControl } = wp.components;

export default function LinkControl({ cta, href, setAttributes }) {
  const setCta = (value) => {
    setAttributes({ cta: value });
  };

  const setHref = (value) => {
    setAttributes({ href: formatHref(value) });
  };

  const formatHref = (value) => {
    if (!value) return "";
    else value = String(value);

    const schemas = ["https://".split(""), "http://".split("")];
    const isURL = schemas.reduce(
      (isURL, schema) =>
        isURL ||
        schema.reduce((isURL, char, i) => {
          if (i >= value.length) return isURL;
          return isURL && value[i] === char;
        }, true),
      false
    );

    if (!isURL && value.length >= 8) {
      return "https://" + value;
    }

    return value;
  };

  return (
    <>
      <p>Teaser Link (optional)</p>
      <TextControl
        label={__("Label", "wpct-sm")}
        value={cta}
        onChange={setCta}
      />
      <TextControl
        label={__("URL", "wpct-sm")}
        value={href}
        onChange={setHref}
        placeholder="https://"
      />
    </>
  );
}
