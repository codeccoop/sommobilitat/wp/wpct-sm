import ScrollController from "./ScrollController.js";
import StickyHeader from "./StickyHeader.js";
import { ResponsiveMenus } from "./Navigation.js";
import "./DataTables.js";

document.addEventListener("DOMContentLoaded", () => {
  const scrollController = ScrollController.init();
  const stickyHeader = StickyHeader.init();
  scrollController.onScroll(stickyHeader.onScroll.bind(stickyHeader));

  ResponsiveMenus(stickyHeader.header);
});
