function StickyHeader(el) {
  this.header = el || document.querySelector(".site-header");

  let scrollDelta = 0;
  Object.defineProperty(this, "scrollDelta", {
    get() {
      return scrollDelta;
    },
    set: (v) => {
      scrollDelta = v;
      if (this.header.classList.contains("hidden")) return;
      if (scrollDelta > 100) {
        this.header.classList.add("hidden");
      }
    },
  });

  let direction = null;
  Object.defineProperty(this, "scrollDirection", {
    get() {
      return direction;
    },
    set: (v) => {
      if (v !== direction) {
        this.scrollDelta = 0;
        if (v === "up" && this.header.classList.contains("hidden")) {
          this.header.classList.remove("hidden");
        }
      }
      direction = v;
    },
  });

  if (document.body.classList.contains("admin-bar")) {
    const offsetTop = this.header.offsetTop + this.header.clientHeight;
    const stylesheet = document.createElement("style");
    stylesheet.textContent = `:root {
      --wpct-sm-header-offset: ${offsetTop}px;
    }`;
    document.head.appendChild(stylesheet);
  }
}

StickyHeader.prototype.onScroll = function (ev) {
  if (ev.position.y > 50) {
    this.header.classList.add("sticky");
    document.body.classList.add("sticky-header");
  } else {
    this.header.classList.remove("sticky");
    document.body.classList.remove("sticky-header");
  }

  this.scrollDelta = this.scrollDelta + ev.deltas.y;
  this.scrollDirection = ev.direction.y;
};

StickyHeader.init = function (el) {
  return new StickyHeader(el);
};

export default StickyHeader;
