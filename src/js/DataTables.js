document.addEventListener("DOMContentLoaded", function () {
  const dataTables = document.querySelectorAll(
    ".wp-block-table.is-variation-dynamic"
  );

  for (let dataTable of dataTables) {
    new window.DataTable(dataTable.querySelector("table"), {
      paging: false,
      language: {
        url: "https://cdn.datatables.net/plug-ins/2.2.2/i18n/ca.json",
      },
    });
  }
});
