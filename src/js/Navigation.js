export function ResponsiveMenus() {
  const responsiveMenus = document.querySelectorAll(
    ".wp-block-navigation .wp-block-navigation__responsive-container .wp-block-navigation-item.wp-block-navigation-submenu",
  );

  for (const menu of responsiveMenus) {
    let expanded = false;
    const onClickOut = (ev) => {
      if (!(menu.contains(ev.target) && menu !== ev.target)) {
        ev.preventDefault();
        ev.stopPropagation();
        document.body.removeEventListener("click", onClickOut);
      }
    };

    menu.addEventListener("click", () => {
      expanded = !expanded;
      if (expanded) {
        menu.classList.add("is-open");
        document.body.addEventListener("click", onClickOut);
      } else {
        menu.classList.remove("is-open");
        document.body.removeEventListener("click", onClickOut);
      }
    });
  }
}
