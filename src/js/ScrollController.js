import { throttle } from "./helpers/utils.js";

const DIRS = {
  D: "down",
  U: "up",
  L: "left",
  R: "right",
};

function ScrollController(throttleDelta = 200) {
  this.throttleDelta = throttleDelta;
  this.listeners = new Set();
  this.deltas = {
    x: 0,
    y: 0,
  };
  this.position = {
    x: 0,
    y: 0,
  };
  this.speed = {
    x: 0,
    y: 0,
  };

  const _direction = {
    x: null,
    y: null,
  };

  this.direction = {};
  Object.defineProperty(this.direction, "x", {
    get() {
      return _direction.x;
    },
    set: (v) => {
      if (_direction.x !== v) {
        this.deltas.x = 0;
      }
      _direction.x = v;
    },
  });

  Object.defineProperty(this.direction, "y", {
    get() {
      return _direction.y;
    },
    set: (v) => {
      if (_direction.y !== v) {
        this.deltas.y = 0;
      }
      _direction.y = v;
    },
  });

  this.sections = Array.from(
    document.querySelectorAll(".wp-site-blocks section.wp-block-group"),
  ).concat(document.querySelector("#contact"));

  this.sections.forEach((section) => {
    const id = section.id;
    section.removeAttribute("id");
    section.setAttribute("data-id", id);
    const scrollAnchor = document.createElement("span");
    scrollAnchor.classList.add("top");
    scrollAnchor.id = id;
    section.insertBefore(scrollAnchor, section.children[0]);
  });

  window.addEventListener(
    "scroll",
    throttle(
      (ev) => {
        this.track();
        for (const listener of this.listeners) {
          listener(new ScrollEvent(ev, this));
        }
      },
      this.throttleDelta,
      { trailing: true, leading: false },
    ),
  );

  window.addEventListener("popstate", () => this.setCurrentLink());

  this.setCurrentLink();
  this.updateHash();

  this.onScroll(this.updateHash.bind(this));
  this.track();
}

let _lastTrack;
let _debounce;
ScrollController.prototype.track = function () {
  const now = Date.now();
  this.deltas.y = window.scrollY - this.position.y;
  this.deltas.x = window.scrollX - this.position.x;
  this.direction.y =
    this.deltas.y > 0 ? DIRS.D : this.deltas.y < 0 ? DIRS.U : null;
  this.direction.x =
    this.deltas.x > 0 ? DIRS.R : this.deltas.x < 0 ? DIRS.L : null;
  this.position.y = window.scrollY;
  this.position.x = window.scrollX;
  if (_lastTrack) {
    this.speed.y = (this.deltas.y / (now - _lastTrack)) * 1000;
    this.speed.x = (this.deltas.x / (now - _lastTrack)) * 1000;
  }
  _lastTrack = now;
  clearTimeout(_debounce);
  _debounce = setTimeout(() => {
    this.deltas.y = this.deltas.x = 0;
    this.speed.x = this.speed.y = 0;
  }, this.throttleDelta * 2);
};

ScrollController.prototype.setCurrentLink = function () {
  const currentSection = window.location.hash.replace(/^\/?#/, "");
  for (const navItem of document.querySelectorAll(
    ".wp-block-navigation-item",
  )) {
    if (navItem.classList.contains("has-child")) {
      navItem.classList.remove("current");
    }
    const anchor = navItem.querySelector("a");
    const anchorHash = anchor.href.replace(/^.+\/?#/, "");
    const isSubItem = navItem.parentElement.classList.contains(
      "wp-block-navigation-submenu",
    );
    if (currentSection === anchorHash) {
      navItem.classList.add("current");
      if (isSubItem) {
        navItem.parentElement.parentElement.classList.add("current");
      }
    } else {
      navItem.classList.remove("current");
    }
  }
};

ScrollController.prototype.offScroll = function (listener) {
  if (this.listeners.has(listener)) return;
  this.listeners.delete(listener);
};

ScrollController.prototype.onScroll = function (listener) {
  if (this.listeners.has(listener)) return;
  this.listeners.add(listener);
};

ScrollController.prototype.updateHash = function () {
  for (const section of this.sections) {
    const { top } = section.getBoundingClientRect();
    if (Math.abs(top) < 100) {
      const hash = `#${section.dataset.id}`;
      if (hash !== window.location.hash) {
        window.history.replaceState(
          { from: window.location.hash, to: hash },
          null,
          `${window.location.pathname}${window.location.search}${hash}`,
        );
        window.dispatchEvent(new Event("popstate"));
      }
      break;
    }
  }
};

ScrollController.init = function () {
  return new ScrollController();
};

function ScrollEvent(
  ev,
  { throttleDelta, deltas, position, speed, direction },
) {
  this.sourceEvent = ev;
  this.deltas = deltas;
  this.position = position;
  this.speed = speed;
  this.interval = throttleDelta;
  this.direction = direction;
}

export default ScrollController;
