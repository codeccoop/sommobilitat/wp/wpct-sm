<?php

// Functions
require_once 'includes/functions/utils.php';
require_once 'includes/functions/checks.php';
require_once 'includes/functions/format.php';
require_once 'includes/functions/coupon.php';
require_once 'includes/functions/member.php';
require_once 'includes/functions/reward.php';
require_once 'includes/functions/email.php';

// Models
require_once 'includes/models/coupon.php';

// Forms bridges
require_once 'includes/forms/bridges/bulk-coupons-generation.php';
require_once 'includes/forms/bridges/maintenance-registration.php';
require_once 'includes/forms/bridges/member-registration.php';
require_once 'includes/forms/bridges/newsletter.php';
require_once 'includes/forms/bridges/onboarding-balenya.php';
require_once 'includes/forms/bridges/promo-codes.php';

// Posts bridges
require_once 'includes/remote/faqs.php';

// Form validations
require_once 'includes/forms/validations/member-registration.php';
require_once 'includes/forms/validations/onboarding-balenya.php';
require_once 'includes/forms/validations/promo-codes.php';

// ACF
require_once 'includes/acf/coupon.php';

// Custom Blocks
require_once 'custom-blocks/teaser/teaser.php';
require_once 'custom-blocks/stepper/stepper.php';
require_once 'custom-blocks/page-header/page-header.php';
require_once 'custom-blocks/external-link/external-link.php';

add_filter('enqueue_block_editor_assets', function () {
    $theme = wp_get_theme();
    wp_enqueue_script(
        'block-variations',
        $theme->get_stylesheet_directory_uri() .
            '/assets/js/block-variations.js',
        ['wp-blocks', 'wp-i18n'],
        $theme->get('Version')
    );
});

add_filter('wpct_gfonts', 'wpct_gfonts');
function wpct_gfonts($fonts)
{
    // Filter coop theme's default google fonts
    return $fonts;
}

add_filter('wpct_block_styles', function ($blog_styles) {
    $blog_styles['core/image']['border-radius'] = __('Suavitzat', 'wpct-sm');

    $blog_styles['core/button'] = [
        'arrow' => __('Fletxa', 'wpct-sm'),
        'brand' => __('Groc CTA', 'wpct-sm'),
    ];

    $blog_styles['core/list']['numeric'] = __('Numéric', 'wpct-sm');

    $blog_styles['core/list']['checklist'] = __('Checklist', 'wpct-sm');
    $blog_styles['core/list-item']['checklist-check'] = __('Check', 'wpct-sm');
    $blog_styles['core/list-item']['checklist-uncheck'] = __(
        'Uncheck',
        'wpct-sm'
    );

    $blog_styles['core/columns']['reverse-pile'] = __(
        'Apilament invers',
        'wpct-sm'
    );

    $blog_styles['core/table']['datatable'] = __('Taula dinàmica', 'wpct-sm');
    $blog_styles['core/table']['horizontal'] = __(
        'Scroll horitzontal',
        'wpct-sm'
    );

    return $blog_styles;
});

// add_filter('get_block_type_variations', function ($variations, $block_type) {
//     if ($block_type->name === 'core/table') {
//         $variations[] = [
//             'name' => 'dynamic-table',
//             'title' => __('Taula dinàmica', 'wpct-sm'),
//             'description' => __('Taula amb filtre i ordenació', 'wpct-sm'),
//             'scope' => ['inserter', 'transform'],
//             'isDefault' => false,
//             'attributes' => [
//                 'namespace' => 'is-variation-datatable',
//             ],
//             'isActive' => ['namespace'],
//         ];
//     }
//     return $variations;
// }, 10, 2);

add_filter('wpct_pattern_categories', function ($pattern_categories) {
    // Filter coop theme's custom pattern categories
    $pattern_categories['sm-pattern'] = [
        'label' => 'Som Mobilitat',
    ];

    return $pattern_categories;
});

add_filter(
    'wp_theme_json_data_user',
    function ($theme) {
        // setcookie('wpct-wp-theme', 'dark', time() + 86400, COOKIEPATH, COOKIE_DOMAIN);

        $palette = isset($_COOKIE['wpct-wp-theme'])
            ? $_COOKIE['wpct-wp-theme']
            : 'light';
        if ($palette === 'dark') {
            $json = file_get_contents(dirname(__FILE__) . '/theme-dark.json');
            $dark_config = json_decode($json, true);
            $theme->update_with($dark_config);
        }

        return $theme;
    },
    99
);

// GForms
add_filter('gform_required_legend', '__return_empty_string');

add_filter('gform_form_args', function ($args) {
    $args['submission_method'] = GFFormDisplay::SUBMISSION_METHOD_AJAX;
    return $args;
});

function sm_state_input_autofill($form)
{
    global $sm_address_states;
    $form_id = $form['id'];
    $addr_fields = array_values(
        array_filter($form['fields'], function ($field) {
            return $field->type === 'address';
        })
    );
    foreach ($addr_fields as $field) {

        $input_id = "input_{$form_id}_{$field->id}_4";
        $list_id = "sm-odoo-address-states-{$input_id}";
        $options = json_encode(array_keys($sm_address_states));
        ?>
        <script id="<?= $list_id ?>-script">
            (function () {
                const dataList = document.createElement("datalist");
                const options = <?= $options ?>;
                options.forEach((opt) => {
                    const el = document.createElement("option");
                    el.value = opt;
                    dataList.appendChild(el);
                });
                dataList.id = "<?= $list_id ?>";
                document.getElementById("<?= $input_id ?>").setAttribute("list", "<?= $list_id ?>");
                document.body.appendChild(dataList);
            })();
        </script>
        <?php
    }
}

add_filter('forms_bridge_prune_empties', '__return_true');

add_filter(
    'excerpt_length',
    function () {
        return 40;
    },
    99
);

/* DataTable */
add_action('wp_enqueue_scripts', function () {
    wp_register_style(
        'datatable',
        'https://cdn.datatables.net/2.2.2/css/dataTables.dataTables.min.css',
        [],
        '2.2.2'
    );

    wp_register_script(
        'datatable',
        'https://cdn.datatables.net/2.2.2/js/dataTables.min.js',
        ['jquery'],
        '2.2.2'
    );
});

add_filter(
    'render_block',
    function ($content, $block) {
        if (
            !empty($block['blockName']) &&
            $block['blockName'] === 'core/table' &&
            preg_match(
                '/is-variation-dynamic/',
                $block['attrs']['className'] ?? ''
            )
        ) {
            wp_enqueue_script('datatable');
            wp_enqueue_style('datatable');
        }

        return $content;
    },
    10,
    2
);
