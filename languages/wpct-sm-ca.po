#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Wpct Som Mobilitat\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-08 20:16+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.6.8; wp-6.5.3\n"
"X-Domain: wpct-sm"

#: theme.json:1 theme-dark.json:1
msgctxt "Color name"
msgid "Base"
msgstr ""

#: theme.json:1 theme-dark.json:1
msgctxt "Color name"
msgid "Brand"
msgstr ""

#: theme.json:1 theme-dark.json:1
msgctxt "Color name"
msgid "Primary"
msgstr ""

#: theme.json:1 theme-dark.json:1
msgctxt "Color name"
msgid "Quarterly"
msgstr ""

#: theme.json:1 theme-dark.json:1
msgctxt "Color name"
msgid "Secondary"
msgstr ""

#: theme.json:1 theme-dark.json:1
msgctxt "Color name"
msgid "Tertiary"
msgstr ""

#: theme.json:1 theme-dark.json:1
msgctxt "Color name"
msgid "Typography"
msgstr ""

#: includes/models/coupon.php:8 includes/models/coupon.php:9
msgid "Cupons"
msgstr ""

#: includes/models/member-coupon.php:8 includes/models/member-coupon.php:9
msgid "Cupons de soci"
msgstr ""

#: includes/acf/coupon.php:6
msgid "Cupó"
msgstr ""

#: includes/acf/member-coupon.php:6
msgid "Cupó de soci"
msgstr ""

#. Author of the theme
msgid "Còdec Cooperativa"
msgstr ""

#: includes/erp-forms/coupon-registration.php:78
msgid ""
"El codi ja ha estat fet servir. Per qualsevol dubte o incidència possat en "
"contacte amb nosaltres a <a href=\"mailto:info@sommobilitat.coop\">"
"info@sommobilitat.coop</a>"
msgstr ""

#: includes/erp-forms/coupon-registration.php:75
msgid ""
"El codi no coincideix amb els del nostre registre. Et recordem que per fer-"
"te soci sense un codi promocional pots visitar el següent <a href=\"https:"
"//www.sommobilitat.coop/formulari-fes-te-soci/\">enllaç</a>. Per qualsevol "
"dubte o incidència possat en contacte amb nosaltres a <a href=\"mailto:"
"info@sommobilitat.coop\">info@sommobilitat.coop</a>"
msgstr ""

#: includes/erp-forms/coupon-registration.php:80
msgid ""
"El cupó fet servir correspon a un cupó de registre d'organització. El pots "
"fer servir al <a href=\"https://www.sommobilitat.coop/registre/\">formulari "
"de registre</a>"
msgstr ""

#: theme.json:1
msgctxt "Font family name"
msgid "Bebase Neueu"
msgstr ""

#: theme.json:1
msgctxt "Font family name"
msgid "Monospace"
msgstr ""

#: theme.json:1
msgctxt "Font family name"
msgid "Rethink Sans"
msgstr ""

#: theme.json:1
msgctxt "Font size name"
msgid "Large"
msgstr ""

#: theme.json:1
msgctxt "Font size name"
msgid "Medium"
msgstr ""

#: theme.json:1
msgctxt "Font size name"
msgid "Small"
msgstr ""

#: theme.json:1
msgctxt "Font size name"
msgid "xLarge"
msgstr ""

#: theme.json:1
msgctxt "Font size name"
msgid "xSmall"
msgstr ""

#: theme.json:1
msgctxt "Font size name"
msgid "xxLarge"
msgstr ""

#: theme.json:1
msgctxt "Font size name"
msgid "xxxLarge"
msgstr ""

#. URI of the theme
msgid "https://git.coopdevs.org/codeccoop/wp/wpct-child-theme"
msgstr ""

#. Author URI of the theme
msgid "https://www.codeccoop.org"
msgstr ""

#: includes/erp-forms/newsletter.php:22
msgid "Introduiu un codi postal vàlid"
msgstr ""

#: includes/erp-forms/coupon-registration.php:26
#: includes/erp-forms/member-registration.php:28
#: includes/erp-forms/onboarding-balenya.php:26
msgid "L'adreça no és vàlida"
msgstr ""

#: includes/erp-forms/member-registration.php:72
msgid ""
"La solicitud ha fallat. Intenta-ho de nou i si el problema persisteix "
"contacta amb nosaltres a <a href=\"mailto:socis@sommobilitat.coop\">"
"socis@sommobilitat.coop</a>."
msgstr ""

#: includes/erp-forms/coupon-registration.php:146
msgid ""
"La solucitud ha fallat. Intenta-ho de nou i si el problema persisteix "
"contacta amb nosaltres a <a href=\"mailto:socis@sommobilitat.coop\">"
"socis@sommobilitat.coop</a>"
msgstr ""

#: patterns/main-navigation.php:3
msgctxt "Pattern title"
msgid "Main Navigation"
msgstr ""

#: patterns/home.php:3
msgctxt "Pattern title"
msgid "Site Home"
msgstr ""

#: patterns/footer.php:3
msgctxt "Pattern title"
msgid "SM Footer."
msgstr ""

#: patterns/header.php:3
msgctxt "Pattern title"
msgid "SM Header"
msgstr ""

#: includes/erp-forms/member-registration.php:47
msgid "Per fer-te soci de Som Mobilitat has de ser major d'edat"
msgstr ""

#: includes/erp-forms/coupon-registration.php:45
#: includes/erp-forms/onboarding-balenya.php:45
msgid "Per registrar-te al servei de carsharing has de ser major de 22 anys."
msgstr ""

#: includes/erp-forms/coupon-registration.php:53
#: includes/erp-forms/onboarding-balenya.php:53
msgid ""
"Per registrar-te al servei de carsharing has de tenir un carnet de conduir "
"amb data vàlida i en cas de tenir menys de 25 anys, ha de tenir un mínim de "
"2 anys d'antiguitat."
msgstr ""

#: includes/models/member.php:9
msgid "Soci/a"
msgstr ""

#: includes/models/member.php:8
msgid "Socis"
msgstr ""

#: theme.json:1
msgctxt "Space size name"
msgid "1"
msgstr ""

#: theme.json:1
msgctxt "Space size name"
msgid "2"
msgstr ""

#: theme.json:1
msgctxt "Space size name"
msgid "3"
msgstr ""

#: theme.json:1
msgctxt "Space size name"
msgid "4"
msgstr ""

#: theme.json:1
msgctxt "Space size name"
msgid "5"
msgstr ""

#: theme.json:1
msgctxt "Space size name"
msgid "6"
msgstr ""

#: theme.json:1
msgctxt "Template part name"
msgid "Footer"
msgstr ""

#: theme.json:1
msgctxt "Template part name"
msgid "Header"
msgstr ""

#. Description of the theme
msgid "Wpct child theme per la web de sommobiliat.coop"
msgstr "Wpct child theme per la web de sommobiliat.coop"

#. Name of the theme
msgid "Wpct Som Mobilitat"
msgstr "Wpct Som Mobilitat"

msgid "wpct-erp-forms_general__share_product--label"
msgstr "Odoo share product"
